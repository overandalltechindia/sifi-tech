﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/BookingSlotLine")]
    public class BookingSlotLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingSlotLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/BookingSlotLine
        [HttpGet]
        [Authorize]
        public IActionResult GetBookingSlotLine(string masterid)
        {
            var list = _context.BookingSlotLine.Include(x => x.containerLaden).Where(x => x.bookingSlotId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/BookingSlotLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostBookingSlotLine([FromBody] BookingSlotLine bookingSlotLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BookingSlot bookingSlot = await _context.BookingSlot.Where(x => x.bookingSlotId.Equals(bookingSlotLine.bookingSlotId)).FirstOrDefaultAsync();

            //if (bookingSlot.bookingSlotStatus == BookingSlotStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //bookingSlotLine.totalAmount = (decimal)bookingSlotLine.qty * bookingSlotLine.price;

            if (bookingSlotLine.bookingSlotLineId == string.Empty)
            {
                bookingSlotLine.bookingSlotLineId = Guid.NewGuid().ToString();
                _context.BookingSlotLine.Add(bookingSlotLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(bookingSlotLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/BookingSlotLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteBookingSlotLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookingSlotLine = await _context.BookingSlotLine
                .Include(x => x.bookingSlot)
                .SingleOrDefaultAsync(m => m.bookingSlotLineId == id);

            if (bookingSlotLine == null)
            {
                return NotFound();
            }

            //if (bookingSlotLine.bookingSlot.bookingSlotStatus == BookingSlotStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.BookingSlotLine.Remove(bookingSlotLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool BookingSlotLineExists(string id)
        {
            return _context.BookingSlotLine.Any(e => e.bookingSlotLineId == id);
        }


    }

}
