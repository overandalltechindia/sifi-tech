﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/OceanPriceLine")]
    public class OceanPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OceanPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/OceanPriceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetOceanPriceLine(string masterid)
        {
            var list = _context.OceanPriceLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Include(x => x.containerEmpty).Where(x => x.oceanPriceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/OceanPriceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostOceanPriceLine([FromBody] OceanPriceLine oceanPriceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            OceanPrice oceanPrice = await _context.OceanPrice.Where(x => x.oceanPriceId.Equals(oceanPriceLine.oceanPriceId)).FirstOrDefaultAsync();

            //if (oceanPrice.oceanPriceStatus == OceanPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //oceanPriceLine.totalAmount = (decimal)oceanPriceLine.qty * oceanPriceLine.price;

            if (oceanPriceLine.oceanPriceLineId == string.Empty)
            {
                oceanPriceLine.oceanPriceLineId = Guid.NewGuid().ToString();
                _context.OceanPriceLine.Add(oceanPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(oceanPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/OceanPriceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteOceanPriceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var oceanPriceLine = await _context.OceanPriceLine
                .Include(x => x.oceanPrice)
                .SingleOrDefaultAsync(m => m.oceanPriceLineId == id);

            if (oceanPriceLine == null)
            {
                return NotFound();
            }

            //if (oceanPriceLine.oceanPrice.oceanPriceStatus == OceanPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.OceanPriceLine.Remove(oceanPriceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool OceanPriceLineExists(string id)
        {
            return _context.OceanPriceLine.Any(e => e.oceanPriceLineId == id);
        }


    }

}
