﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/PurchaseInvoiceLine")]
    public class PurchaseInvoiceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PurchaseInvoiceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/PurchaseInvoiceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetPurchaseInvoiceLine(string masterid)
        {
            var list = _context.PurchaseInvoiceLine.Include(x => x.chargeHead).Include(x => x.currency).Where(x => x.purchaseInvoiceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/PurchaseInvoiceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostPurchaseInvoiceLine([FromBody] PurchaseInvoiceLine purchaseInvoiceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PurchaseInvoice purchaseInvoice = await _context.PurchaseInvoice.Where(x => x.purchaseInvoiceId.Equals(purchaseInvoiceLine.purchaseInvoiceId)).FirstOrDefaultAsync();

            //if (purchaseInvoice.purchaseInvoiceStatus == PurchaseInvoiceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //purchaseInvoiceLine.totalAmount = (decimal)purchaseInvoiceLine.qty * purchaseInvoiceLine.price;

            if (purchaseInvoiceLine.purchaseInvoiceLineId == string.Empty)
            {
                purchaseInvoiceLine.purchaseInvoiceLineId = Guid.NewGuid().ToString();
                _context.PurchaseInvoiceLine.Add(purchaseInvoiceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(purchaseInvoiceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/PurchaseInvoiceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeletePurchaseInvoiceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var purchaseInvoiceLine = await _context.PurchaseInvoiceLine
                .Include(x => x.purchaseInvoice)
                .SingleOrDefaultAsync(m => m.purchaseInvoiceLineId == id);

            if (purchaseInvoiceLine == null)
            {
                return NotFound();
            }

            //if (purchaseInvoiceLine.purchaseInvoice.purchaseInvoiceStatus == PurchaseInvoiceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.PurchaseInvoiceLine.Remove(purchaseInvoiceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool PurchaseInvoiceLineExists(string id)
        {
            return _context.PurchaseInvoiceLine.Any(e => e.purchaseInvoiceLineId == id);
        }


    }

}
