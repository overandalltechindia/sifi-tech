﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/PortStoragePriceLine")]
    public class PortStoragePriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PortStoragePriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/PortStoragePriceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetPortStoragePriceLine(string masterid)
        {
            var list = _context.PortStoragePriceLine.Include(x => x.chargeHead).Where(x => x.portPriceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/PortStoragePriceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostPortStoragePriceLine([FromBody] PortStoragePriceLine portStoragePriceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (portStoragePriceLine.portStoragePriceLineId == string.Empty)
            {
                portStoragePriceLine.portStoragePriceLineId = Guid.NewGuid().ToString();
                _context.PortStoragePriceLine.Add(portStoragePriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Point Of Contact Information" });
            }
            else
            {
                _context.Update(portStoragePriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Point Of Contact Information" });
            }

        }

        // DELETE: api/PortStoragePriceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeletePortStoragePriceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var portStoragePriceLine = await _context.PortStoragePriceLine.SingleOrDefaultAsync(m => m.portStoragePriceLineId == id);
            if (portStoragePriceLine == null)
            {
                return NotFound();
            }

            _context.PortStoragePriceLine.Remove(portStoragePriceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Point Of Contact Information Deletion Successful" });
        }


        private bool PortStoragePriceLineExists(string id)
        {
            return _context.PortStoragePriceLine.Any(e => e.portStoragePriceLineId == id);
        }


    }

}
