﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/SalesInquiryLine")]
    public class SalesInquiryLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SalesInquiryLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/SalesInquiryLine
        [HttpGet]
        [Authorize]
        public IActionResult GetSalesInquiryLine(string masterid)
        {
            var list = _context.SalesInquiryLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Where(x => x.salesInquiryId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/SalesInquiryLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostSalesInquiryLine([FromBody] SalesInquiryLine salesInquiryLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SalesInquiry salesInquiry = await _context.SalesInquiry.Where(x => x.salesInquiryId.Equals(salesInquiryLine.salesInquiryId)).FirstOrDefaultAsync();

            //if (salesInquiry.salesInquiryStatus == SalesInquiryStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //salesInquiryLine.totalAmount = (decimal)salesInquiryLine.qty * salesInquiryLine.price;

            if (salesInquiryLine.salesInquiryLineId == string.Empty)
            {
                salesInquiryLine.salesInquiryLineId = Guid.NewGuid().ToString();
                _context.SalesInquiryLine.Add(salesInquiryLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(salesInquiryLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/SalesInquiryLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteSalesInquiryLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var salesInquiryLine = await _context.SalesInquiryLine
                .Include(x => x.salesInquiry)
                .SingleOrDefaultAsync(m => m.salesInquiryLineId == id);

            if (salesInquiryLine == null)
            {
                return NotFound();
            }

            //if (salesInquiryLine.salesInquiry.salesInquiryStatus == SalesInquiryStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.SalesInquiryLine.Remove(salesInquiryLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool SalesInquiryLineExists(string id)
        {
            return _context.SalesInquiryLine.Any(e => e.salesInquiryLineId == id);
        }


    }

}
