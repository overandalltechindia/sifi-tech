﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/InlandPriceLine")]
    public class InlandPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InlandPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/InlandPriceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetInlandPriceLine(string masterid)
        {
            var list = _context.InlandPriceLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Include(x => x.containerEmpty).Where(x => x.inlandPriceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/InlandPriceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostInlandPriceLine([FromBody] InlandPriceLine inlandPriceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            InlandPrice inlandPrice = await _context.InlandPrice.Where(x => x.inlandPriceId.Equals(inlandPriceLine.inlandPriceId)).FirstOrDefaultAsync();

            //if (inlandPrice.inlandPriceStatus == InlandPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //inlandPriceLine.totalAmount = (decimal)inlandPriceLine.qty * inlandPriceLine.price;

            if (inlandPriceLine.inlandPriceLineId == string.Empty)
            {
                inlandPriceLine.inlandPriceLineId = Guid.NewGuid().ToString();
                _context.InlandPriceLine.Add(inlandPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(inlandPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/InlandPriceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteInlandPriceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inlandPriceLine = await _context.InlandPriceLine
                .Include(x => x.inlandPrice)
                .SingleOrDefaultAsync(m => m.inlandPriceLineId == id);

            if (inlandPriceLine == null)
            {
                return NotFound();
            }

            //if (inlandPriceLine.inlandPrice.inlandPriceStatus == InlandPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.InlandPriceLine.Remove(inlandPriceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool InlandPriceLineExists(string id)
        {
            return _context.InlandPriceLine.Any(e => e.inlandPriceLineId == id);
        }


    }

}
