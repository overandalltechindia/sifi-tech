﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/InvoicesLine")]
    public class InvoicesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InvoicesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/InvoicesLine
        [HttpGet]
        [Authorize]
        public IActionResult GetInvoicesLine(string masterid)
        {
      
            var list = _context.InvoicesLine.Include(x => x.cost).Include(x => x.income).Include(p => p.currency).Where(x => x.invoicesId.Equals(masterid)).OrderBy(x => x.createdAt).ToList();
            return Json(new { data = list });
        }

        // POST: api/InvoicesLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostInvoicesLine([FromBody] InvoicesLine invoicesLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Invoices invoices = await _context.Invoices.Where(x => x.invoicesId.Equals(invoicesLine.invoicesId)).FirstOrDefaultAsync();

            //if (invoices.invoicesStatus == InvoicesStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //invoicesLine.totalAmount = (decimal)invoicesLine.qty * invoicesLine.price;

            if (invoicesLine.invoicesLineId == string.Empty)
            {
                invoicesLine.invoicesLineId = Guid.NewGuid().ToString();
                _context.InvoicesLine.Add(invoicesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(invoicesLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/InvoicesLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteInvoicesLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var invoicesLine = await _context.InvoicesLine
                .Include(x => x.invoices)
                .SingleOrDefaultAsync(m => m.invoicesLineId == id);

            if (invoicesLine == null)
            {
                return NotFound();
            }

            //if (invoicesLine.invoices.invoicesStatus == InvoicesStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.InvoicesLine.Remove(invoicesLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool InvoicesLineExists(string id)
        {
            return _context.InvoicesLine.Any(e => e.invoicesLineId == id);
        }


    }

}
