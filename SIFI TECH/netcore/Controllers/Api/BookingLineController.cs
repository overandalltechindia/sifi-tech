﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/BookingLine")]
    public class BookingLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/BookingLine
        [HttpGet]
        [Authorize]
        public IActionResult GetBookingLine(string masterid)
        {
            var list = _context.BookingLine.Include(x => x.emptyYardLocation).Include(x => x.containerLaden).Where(x => x.bookingId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/BookingLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostBookingLine([FromBody] BookingLine bookingLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Booking booking = await _context.Booking.Where(x => x.bookingId.Equals(bookingLine.bookingId)).FirstOrDefaultAsync();

            //if (booking.bookingStatus == BookingStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //bookingLine.totalAmount = (decimal)bookingLine.qty * bookingLine.price;

            if (bookingLine.bookingLineId == string.Empty)
            {
                bookingLine.bookingLineId = Guid.NewGuid().ToString();
                _context.BookingLine.Add(bookingLine);

                var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == bookingLine.ladenContainerId);
                containerDetails.containerStatus = ContainerStatus.loaded;
                _context.ContainerDetails.Update(containerDetails);

                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(bookingLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/BookingLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteBookingLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookingLine = await _context.BookingLine
                .Include(x => x.booking)
                .SingleOrDefaultAsync(m => m.bookingLineId == id);

            if (bookingLine == null)
            {
                return NotFound();
            }

            //if (bookingLine.booking.bookingStatus == BookingStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.BookingLine.Remove(bookingLine);
            var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == bookingLine.ladenContainerId);
            containerDetails.containerStatus = ContainerStatus.empty;
            _context.ContainerDetails.Update(containerDetails);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool BookingLineExists(string id)
        {
            return _context.BookingLine.Any(e => e.bookingLineId == id);
        }


    }

}
