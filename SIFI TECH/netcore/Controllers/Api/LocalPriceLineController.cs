﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/LocalPriceLine")]
    public class LocalPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LocalPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/LocalPriceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetLocalPriceLine(string masterid)
        {
            var list = _context.LocalPriceLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Include(x => x.containerEmpty).Where(x => x.localPriceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/LocalPriceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostLocalPriceLine([FromBody] LocalPriceLine localPriceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LocalPrice localPrice = await _context.LocalPrice.Where(x => x.localPriceId.Equals(localPriceLine.localPriceId)).FirstOrDefaultAsync();

            //if (localPrice.localPriceStatus == LocalPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //localPriceLine.totalAmount = (decimal)localPriceLine.qty * localPriceLine.price;

            if (localPriceLine.localPriceLineId == string.Empty)
            {
                localPriceLine.localPriceLineId = Guid.NewGuid().ToString();
                _context.LocalPriceLine.Add(localPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(localPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/LocalPriceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteLocalPriceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var localPriceLine = await _context.LocalPriceLine
                .Include(x => x.localPrice)
                .SingleOrDefaultAsync(m => m.localPriceLineId == id);

            if (localPriceLine == null)
            {
                return NotFound();
            }

            //if (localPriceLine.localPrice.localPriceStatus == LocalPriceStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.LocalPriceLine.Remove(localPriceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool LocalPriceLineExists(string id)
        {
            return _context.LocalPriceLine.Any(e => e.localPriceLineId == id);
        }


    }

}
