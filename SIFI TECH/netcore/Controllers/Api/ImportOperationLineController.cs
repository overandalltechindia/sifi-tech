﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/ImportOperationLine")]
    public class ImportOperationLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ImportOperationLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ImportOperationLine
        [HttpGet]
        [Authorize]
        public IActionResult GetImportOperationLine(string masterid)
        {
            var list = _context.ImportOperationLine.Include(x => x.containerNumber).Include(x => x.emptyYard).Where(x => x.importOperationId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/ImportOperationLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostImportOperationLine([FromBody] ImportOperationLine importOperationLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ImportOperation importOperation = await _context.ImportOperation.Include(x => x.importOperationLine).Include(x => x.booking).Where(x => x.importOperationId.Equals(importOperationLine.importOperationId)).FirstOrDefaultAsync();

            //if (importOperation.importOperationStatus == ImportOperationStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //importOperationLine.totalAmount = (decimal)importOperationLine.qty * importOperationLine.price;

            if (importOperationLine.importOperationLineId == string.Empty)
            {
                importOperationLine.importOperationLineId = Guid.NewGuid().ToString();
                _context.ImportOperationLine.Add(importOperationLine);
                var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == importOperationLine.containerNumberId);
                containerDetails.containerStatus = ContainerStatus.empty;
                containerDetails.availableOn = DateTime.Now;
                containerDetails.locationId = importOperation.booking.tolocationId;
                containerDetails.emptyyardLocationId = importOperation.importOperationLine.SingleOrDefault(x => x.containerNumberId == importOperationLine.containerNumberId).emptyYardId;
                _context.ContainerDetails.Update(containerDetails);

                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(importOperationLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/ImportOperationLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteImportOperationLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var importOperationLine = await _context.ImportOperationLine
                .Include(x => x.importOperation)
                .SingleOrDefaultAsync(m => m.importOperationLineId == id);

            if (importOperationLine == null)
            {
                return NotFound();
            }

            ImportOperation importOperation = await _context.ImportOperation.Include(x => x.booking).ThenInclude(x => x.bookingLine).Where(x => x.importOperationId.Equals(importOperationLine.importOperationId)).FirstOrDefaultAsync();

            //if (importOperationLine.importOperation.importOperationStatus == ImportOperationStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.ImportOperationLine.Remove(importOperationLine);
            var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == importOperationLine.containerNumberId);
            containerDetails.containerStatus = ContainerStatus.onwater;
            containerDetails.locationId = importOperation.booking.tolocationId;
            containerDetails.emptyyardLocationId = null;
            _context.ContainerDetails.Update(containerDetails);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool ImportOperationLineExists(string id)
        {
            return _context.ImportOperationLine.Any(e => e.importOperationLineId == id);
        }


    }

}
