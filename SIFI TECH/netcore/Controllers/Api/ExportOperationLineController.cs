﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/ExportOperationLine")]
    public class ExportOperationLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExportOperationLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ExportOperationLine
        [HttpGet]
        [Authorize]
        public IActionResult GetExportOperationLine(string masterid)
        {
            var list = _context.ExportOperationLine.Include(x => x.containerNumber).Where(x => x.exportOperationId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/ExportOperationLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostExportOperationLine([FromBody] ExportOperationLine exportOperationLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ExportOperation exportOperation = await _context.ExportOperation.Where(x => x.exportOperationId.Equals(exportOperationLine.exportOperationId)).FirstOrDefaultAsync();

            //if (exportOperation.exportOperationStatus == ExportOperationStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //exportOperationLine.totalAmount = (decimal)exportOperationLine.qty * exportOperationLine.price;

            if (exportOperationLine.exportOperationLineId == string.Empty)
            {
                exportOperationLine.exportOperationLineId = Guid.NewGuid().ToString();
                _context.ExportOperationLine.Add(exportOperationLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(exportOperationLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/ExportOperationLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteExportOperationLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exportOperationLine = await _context.ExportOperationLine
                .Include(x => x.exportOperation)
                .SingleOrDefaultAsync(m => m.exportOperationLineId == id);

            if (exportOperationLine == null)
            {
                return NotFound();
            }

            //if (exportOperationLine.exportOperation.exportOperationStatus == ExportOperationStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.ExportOperationLine.Remove(exportOperationLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool ExportOperationLineExists(string id)
        {
            return _context.ExportOperationLine.Any(e => e.exportOperationLineId == id);
        }


    }

}
