﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/BillOfLoadingLine")]
    public class BillOfLoadingLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BillOfLoadingLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/BillOfLoadingLine
        [HttpGet]
        [Authorize]
        public IActionResult GetBillOfLoadingLine(string masterid)
        {
            var list = _context.BillOfLoadingLine.Include(x => x.containerNumber).Where(x => x.billOfLoadingId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/BillOfLoadingLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostBillOfLoadingLine([FromBody] BillOfLoadingLine billOfLoadingLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BillOfLoading billOfLoading = await _context.BillOfLoading.Include(x => x.booking).Where(x => x.billOfLoadingId.Equals(billOfLoadingLine.billOfLoadingId)).FirstOrDefaultAsync();

            //if (billOfLoading.billOfLoadingStatus == BillOfLoadingStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //billOfLoadingLine.totalAmount = (decimal)billOfLoadingLine.qty * billOfLoadingLine.price;

            if (billOfLoadingLine.billOfLoadingLineId == string.Empty)
            {
                billOfLoadingLine.billOfLoadingLineId = Guid.NewGuid().ToString();
                _context.BillOfLoadingLine.Add(billOfLoadingLine);
                var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == billOfLoadingLine.containerNumberId);
                containerDetails.containerStatus = ContainerStatus.onwater;
                containerDetails.locationId = billOfLoading.booking.tolocationId;
                containerDetails.emptyyardLocationId = null;

                _context.ContainerDetails.Update(containerDetails);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(billOfLoadingLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/BillOfLoadingLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteBillOfLoadingLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var billOfLoadingLine = await _context.BillOfLoadingLine
                .Include(x => x.billOfLoading)
                .SingleOrDefaultAsync(m => m.billOfLoadingLineId == id);

            if (billOfLoadingLine == null)
            {
                return NotFound();
            }

            //if (billOfLoadingLine.billOfLoading.billOfLoadingStatus == BillOfLoadingStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            BillOfLoading billOfLoading = await _context.BillOfLoading.Include(x => x.booking).ThenInclude(x => x.bookingLine).Where(x => x.billOfLoadingId.Equals(billOfLoadingLine.billOfLoadingId)).FirstOrDefaultAsync();

            _context.BillOfLoadingLine.Remove(billOfLoadingLine);
            var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == billOfLoadingLine.containerNumberId);
            containerDetails.containerStatus = ContainerStatus.loaded;
            containerDetails.locationId = billOfLoading.booking.fromlocationId;
            containerDetails.emptyyardLocationId = billOfLoading.booking.bookingLine.SingleOrDefault(x => x.ladenContainerId == billOfLoadingLine.containerNumberId).emptyYardLocationId;
            _context.ContainerDetails.Update(containerDetails);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool BillOfLoadingLineExists(string id)
        {
            return _context.BillOfLoadingLine.Any(e => e.billOfLoadingLineId == id);
        }


    }

}
