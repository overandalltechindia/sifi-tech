﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/MultipalRateQuoteLine")]
    public class MultipalRateQuoteLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MultipalRateQuoteLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MultipalRateQuoteLine
        [HttpGet]
        [Authorize]
        public IActionResult GetMultipalRateQuoteLine(string masterid)
        {
            var list = _context.MultipalRateQuoteLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.fromLocation).Include(x => x.trpPort).Include(x => x.toLocation).Include(x => x.currency).Where(x => x.multipalRateQuoteId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/MultipalRateQuoteLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostMultipalRateQuoteLine([FromBody] MultipalRateQuoteLine multipalRateQuoteLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MultipalRateQuote multipalRateQuote = await _context.MultipalRateQuote.Where(x => x.multipalRateQuoteId.Equals(multipalRateQuoteLine.multipalRateQuoteId)).FirstOrDefaultAsync();

            //if (multipalRateQuote.multipalRateQuoteStatus == MultipalRateQuoteStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not edit [Completed] order" });
            //}

            //multipalRateQuoteLine.totalAmount = (decimal)multipalRateQuoteLine.qty * multipalRateQuoteLine.price;

            if (multipalRateQuoteLine.multipalRateQuoteLineId == string.Empty)
            {
                multipalRateQuoteLine.multipalRateQuoteLineId = Guid.NewGuid().ToString();
                _context.MultipalRateQuoteLine.Add(multipalRateQuoteLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Add new data success." });
            }
            else
            {
                _context.Update(multipalRateQuoteLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edit data success." });
            }

        }

        // DELETE: api/MultipalRateQuoteLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult>  DeleteMultipalRateQuoteLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var multipalRateQuoteLine = await _context.MultipalRateQuoteLine
                .Include(x => x.multipalRateQuote)
                .SingleOrDefaultAsync(m => m.multipalRateQuoteLineId == id);

            if (multipalRateQuoteLine == null)
            {
                return NotFound();
            }

            //if (multipalRateQuoteLine.multipalRateQuote.multipalRateQuoteStatus == MultipalRateQuoteStatus.Completed)
            //{
            //    return Json(new { success = false, message = "Error. Can not delete [Completed] order" });
            //}

            _context.MultipalRateQuoteLine.Remove(multipalRateQuoteLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Delete success." });
        }


        private bool MultipalRateQuoteLineExists(string id)
        {
            return _context.MultipalRateQuoteLine.Any(e => e.multipalRateQuoteLineId == id);
        }


    }

}
