﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Api
{

    [Produces("application/json")]
    [Route("api/PortPriceLine")]
    public class PortPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PortPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/PortPriceLine
        [HttpGet]
        [Authorize]
        public IActionResult GetPortPriceLine(string masterid)
        {
            var list = _context.PortPriceLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Include(x => x.containerEmpty).Where(x => x.portPriceId.Equals(masterid)).ToList();
            return Json(new { data = list });
        }

        // POST: api/PortPriceLine
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PostPortPriceLine([FromBody] PortPriceLine portPriceLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (portPriceLine.portPriceLineId == string.Empty)
            {
                portPriceLine.portPriceLineId = Guid.NewGuid().ToString();
                _context.PortPriceLine.Add(portPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Added New Service Information" });
            }
            else
            {
                _context.Update(portPriceLine);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Edited Service Information" });
            }

        }

        // DELETE: api/PortPriceLine/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeletePortPriceLine([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var portPriceLine = await _context.PortPriceLine.SingleOrDefaultAsync(m => m.portPriceLineId == id);
            if (portPriceLine == null)
            {
                return NotFound();
            }

            _context.PortPriceLine.Remove(portPriceLine);
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "Service Information Deletion Successful" });
        }


        private bool PortPriceLineExists(string id)
        {
            return _context.PortPriceLine.Any(e => e.portPriceLineId == id);
        }


    }

}
