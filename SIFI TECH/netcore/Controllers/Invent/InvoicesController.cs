﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Services;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "Invoices")]
    public class InvoicesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public InvoicesController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }


        public async Task<IActionResult> ShowInvoices(string id)
        {
            Invoices obj = await _context.Invoices
                .Include(x => x.booking)
                .Include(x => x.invoicesLine).ThenInclude(x => x.invoices)
                .Include(x => x.branch)


                .SingleOrDefaultAsync(x => x.invoicesId.Equals(id));
            _context.Update(obj);

            return View(obj);
        }
        public async Task<IActionResult> PrintInvoice(string id)
        {
            Invoices obj = await _context.Invoices
                .Include(x => x.booking)
                .Include(x => x.invoicesLine).ThenInclude(x => x.currency)
                .Include(x => x.booking.bookingSlot)
                .Include(x => x.booking.salesInquiry.customer)
                .Include(x => x.booking.salesInquiry)
                .ThenInclude(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.booking.fromLocation)
                .Include(x => x.booking.toLocation)
                .Include(x => x.booking.bookingSlot.vendor)
                .Include(x => x.invoicesLine).ThenInclude(x => x.cost)
                .Include(x => x.invoicesLine).ThenInclude(x => x.invoices)
                .Include(x => x.booking.bookingLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.booking.bookingLine).ThenInclude(x => x.containerLaden.container)
                .Include(x => x.branch)


                .SingleOrDefaultAsync(x => x.invoicesId.Equals(id));
            _context.Update(obj);

            return View(obj);
        }


        // GET: Invoices
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<Invoices> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {

                applicationDbContext = _context.Invoices.OrderByDescending(x => x.createdAt)
                .Include(x => x.invoicesLine).ThenInclude(x => x.currency)
                .Include(x => x.invoicesLine).ThenInclude(x => x.income)
                .Include(x => x.invoicesLine).ThenInclude(x => x.cost)
                .Include(x => x.invoicesLine)
                    .Include(p => p.booking)
                    .Include(p => p.branch)
                    .Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));

            }
            else //If Super Admin
            {
                applicationDbContext = _context.Invoices.OrderByDescending(x => x.createdAt)
                    .Include(x => x.invoicesLine).ThenInclude(x => x.currency)
                    .Include(x => x.invoicesLine).ThenInclude(x => x.income)
                    .Include(x => x.invoicesLine).ThenInclude(x => x.cost)
                    .Include(x => x.invoicesLine)
                    .Include(p => p.booking)
                    .Include(p => p.branch);

            }
            return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> LineIndex()
        {
            var applicationDbContext = _context.InvoicesLine.Include(p => p.invoices).Include(p => p.cost).Include(p => p.income).Include(p => p.currency).Include(p => p.invoices.booking);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Invoices/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoices = await _context.Invoices
        .Include(x => x.invoicesLine)
        .Include(p => p.booking)

            .SingleOrDefaultAsync(m => m.invoicesId == id);
            if (invoices == null)
            {
                return NotFound();
            }
            _context.Update(invoices);
            await _context.SaveChangesAsync();

            return View(invoices);


        }


        public async Task<IActionResult> Finish(string id)
        {
            var invoice = await _context.Invoices
                    .Include(x => x.booking)
                    .Include(p => p.branch)
                        .SingleOrDefaultAsync(x => x.invoicesId.Equals(id));
            invoice.status = InvoiceStatus.COMPLETED;
            invoice.totalInvoiceAmount = _context.InvoicesLine.Where(x => x.invoicesId == id).Sum(x => x.incamount).ToString();
            invoice.netBalance = Convert.ToString(Convert.ToDecimal(invoice.totalInvoiceAmount) - (Convert.ToDecimal(invoice.receiptAmount) - Convert.ToDecimal(invoice.tds)));
            _context.Update(invoice);
            await _context.SaveChangesAsync();
            //return View("Details", invoice);
            return Json(new { success = true, message = "Invoice Finished and saved successfully!" });
        }


        // GET: Invoices/Create
        public IActionResult Create()
        {
            Invoices invoice = new Invoices();

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            //var branchId = _service.GetUserBranchId(User.Identity.Name);

            //if (invoice.branchId != null && invoice.bookingId != null)
            //{
            //    invoice.invNumber = GenerateInvoiceNumber(invoice.branchId, invoice.bookingId);
            //}
            return View(invoice);
        }


        [HttpGet]
        public string GenerateInvoiceNumber(string inputBranchId, string inputBookingId)
        {
            /*Version 1*/
            //var inputBranch = _context.Branch.SingleOrDefault(x => x.branchId == inputBranchId);
            //var inputPOL = _context.Booking.Include(x => x.fromLocation).SingleOrDefault(x => x.bookingId == inputBookingId).fromLocation.locationCode;
            //var inputPOD = _context.Booking.Include(x => x.toLocation).SingleOrDefault(x => x.bookingId == inputBookingId).toLocation.locationCode;

            //var start = 600;
            //var invoiceId = start + _context.Invoices.Where(x => x.branchId == inputBranch.branchId).Count() + 1;

            //var newInvoiceId = invoiceId.ToString().PadLeft((invoiceId.ToString().Length + (5 - invoiceId.ToString().Length)), '0');


            //var invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/" + newInvoiceId;


            ////Check if invoice/ sequence number is same in table
            //var duplicateInvoiceNumber = _context.Invoices.Any(x => x.invNumber == invoiceNumber);

            //if (duplicateInvoiceNumber)
            //{
            //    var newInvoiceNumber = invoiceId + 1;

            //    var newSkipInvoiceId = newInvoiceNumber.ToString().PadLeft((newInvoiceNumber.ToString().Length + (5 - newInvoiceNumber.ToString().Length)), '0');

            //    invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/" + newSkipInvoiceId;
            //}



            /*Version 2*/
            var inputBranch = _context.Branch.SingleOrDefault(x => x.branchId == inputBranchId);
            var finYear = GetFiscalYear(DateTime.Today);

            var hasCurrentYearInvoice = _context.Invoices.Any(x => x.invNumber.Contains(finYear));


            var start = 100;
            var invoiceId = 0;

            if (!hasCurrentYearInvoice)
            {
                invoiceId = start + 1;
            }
            else  //This will check the current year series and start from that position
            {
                invoiceId = start + _context.Invoices.Where(x => x.branchId == inputBranch.branchId).Where(x => x.invNumber.Contains(finYear)).Count() + 1;
            }


            var newInvoiceId = invoiceId.ToString().PadLeft((invoiceId.ToString().Length + (5 - invoiceId.ToString().Length)), '0');


            var invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "\\" + finYear + "\\" + newInvoiceId;


            //Check if invoice/ sequence number is same in table
            var duplicateInvoiceNumber = _context.Invoices.Any(x => x.invNumber == invoiceNumber);

            if (duplicateInvoiceNumber)
            {
                var newInvoiceNumber = invoiceId + 1;

                var newSkipInvoiceId = newInvoiceNumber.ToString().PadLeft((newInvoiceNumber.ToString().Length + (5 - newInvoiceNumber.ToString().Length)), '0');

                invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "\\" + finYear + "\\" + newSkipInvoiceId;
            }


            return invoiceNumber;

        }

        public string GetFiscalYear(DateTime inDate)
        {
            string finyear = "";
            DateTime dt = inDate;
            int m = dt.Month;
            int y = dt.Year;
            if (m > 3)
            {
                finyear = y.ToString().Substring(2, 2) + "-" + Convert.ToString((y + 1)).Substring(2, 2);
                //get last  two digits (eg: 10 from 2010);
            }
            else
            {
                finyear = Convert.ToString((y - 1)).Substring(2, 2) + "-" + y.ToString().Substring(2, 2);
            }
            return finyear;
        }



        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("invoicesId,bookingId,branchId,nblNumber,invNumber,invDate,dueDate,utrNumber,totalInvoiceAmount,supplyTo,receiptAmount,tds,receiptDate,netBalance,status,preparedBy,creditDays, remark")] Invoices invoices)
        {
            if (ModelState.IsValid)
            {
                var duplicateInvoiceNumberExists = _context.Invoices.Any(x => x.invNumber.Contains(invoices.invNumber));

                if (duplicateInvoiceNumberExists)
                {
                    var newInvoiceNumber = GenerateInvoiceNumber(invoices.branchId, invoices.bookingId);
                    invoices.invNumber = newInvoiceNumber;
                }


                _context.Add(invoices);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = invoices.invoicesId });
            }
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            
            return View(invoices);
        }

        // GET: Invoices/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoices = await _context.Invoices.Include(x => x.invoicesLine).SingleOrDefaultAsync(m => m.invoicesId == id);

            if (invoices == null)
            {
                return NotFound();
            }
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", invoices.bookingId);
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            return View(invoices);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("invoicesId,bookingId,branchId,nblNumber,invNumber,invDate,dueDate,utrNumber,totalInvoiceAmount,supplyTo,receiptAmount,tds,receiptDate,netBalance,status,preparedBy,creditDays, remark")] Invoices invoices)
        {
            if (id != invoices.invoicesId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(invoices);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoicesExists(invoices.invoicesId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", invoices.bookingId);
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            return View(invoices);
        }

        // GET: Invoices/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var invoices = await _context.Invoices
        .Include(x => x.invoicesLine)
        .Include(p => p.booking)

        .SingleOrDefaultAsync(m => m.invoicesId == id);

            if (invoices == null)
            {
                return NotFound();
            }

            return View(invoices);
        }



        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {

            var invoices = await _context.Invoices
                    .Include(x => x.invoicesLine)
                    .Include(p => p.branch)
                    .Include(p => p.booking)
                    .SingleOrDefaultAsync(m => m.invoicesId == id);
            try
            {

                _context.InvoicesLine.RemoveRange(invoices.invoicesLine);
                _context.Invoices.Remove(invoices);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(invoices);
            }
            
        }

        private bool InvoicesExists(string id)
        {
            return _context.Invoices.Any(e => e.invoicesId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Invoices
        {
            public const string Controller = "Invoices";
            public const string Action = "Index";
            public const string Role = "Invoices";
            public const string Url = "/Invoices/Index";
            public const string Name = "Invoices";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Invoices")]
        public bool InvoicesRole { get; set; } = false;
    }
}



