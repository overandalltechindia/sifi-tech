﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "PortStoragePriceLine")]
    public class PortStoragePriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PortStoragePriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PortStoragePriceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PortStoragePriceLine.Include(p => p.portPrice).Include(p => p.chargeHead);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PortStoragePriceLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portStoragePriceLine = await _context.PortStoragePriceLine
                    .Include(v => v.portPrice)
                        .SingleOrDefaultAsync(m => m.portStoragePriceLineId == id);
            if (portStoragePriceLine == null)
            {
                return NotFound();
            }

            return View(portStoragePriceLine);
        }


        // GET: PortStoragePriceLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.PortStoragePriceLine.SingleOrDefault(m => m.portStoragePriceLineId == id);
            var selected = _context.PortPrice.SingleOrDefault(m => m.portPriceId == masterid);
          ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");
            if (check == null)
            {
                PortStoragePriceLine objline = new PortStoragePriceLine();
                objline.portPrice = selected;
                objline.portPriceId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: PortStoragePriceLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("portStoragePriceLineId,portPriceId,chargeHeadId,twentyPrice,fortyPrice,fromDay,toDay,createdAt")] PortStoragePriceLine portStoragePriceLine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(portStoragePriceLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");


            return View(portStoragePriceLine);
        }

        // GET: PortStoragePriceLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portStoragePriceLine = await _context.PortStoragePriceLine.SingleOrDefaultAsync(m => m.portStoragePriceLineId == id);
            if (portStoragePriceLine == null)
            {
                return NotFound();
            }

            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");

            return View(portStoragePriceLine);
        }

        // POST: PortStoragePriceLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("portStoragePriceLineId,portPriceId,chargeHeadId,twentyPrice,fortyPrice,fromDay,toDay,createdAt")] PortStoragePriceLine portStoragePriceLine)
        {
            if (id != portStoragePriceLine.portStoragePriceLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(portStoragePriceLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortStoragePriceLineExists(portStoragePriceLine.portStoragePriceLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");

            return View(portStoragePriceLine);
        }

        // GET: PortStoragePriceLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portStoragePriceLine = await _context.PortStoragePriceLine
                    .Include(v => v.portPrice)
                    .SingleOrDefaultAsync(m => m.portStoragePriceLineId == id);
            if (portStoragePriceLine == null)
            {
                return NotFound();
            }

            return View(portStoragePriceLine);
        }




        // POST: PortStoragePriceLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var portStoragePriceLine = await _context.PortStoragePriceLine.SingleOrDefaultAsync(m => m.portStoragePriceLineId == id);
            _context.PortStoragePriceLine.Remove(portStoragePriceLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PortStoragePriceLineExists(string id)
        {
            return _context.PortStoragePriceLine.Any(e => e.portStoragePriceLineId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class PortStoragePriceLine
        {
            public const string Controller = "PortStoragePriceLine";
            public const string Action = "Index";
            public const string Role = "PortStoragePriceLine";
            public const string Url = "/PortStoragePriceLine/Index";
            public const string Name = "PortStoragePriceLine";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "PortStoragePriceLine")]
        public bool PortStoragePriceLineRole { get; set; } = false;
    }
}



