﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "SalesInquiryLine")]
    public class SalesInquiryLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SalesInquiryLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SalesInquiryLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.SalesInquiryLine.Include(p => p.salesInquiry).Include(p => p.chargeHead).Include(p => p.containerLaden);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SalesInquiryLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var salesInquiryLine = await _context.SalesInquiryLine
                .Include(p => p.salesInquiry)
                    .SingleOrDefaultAsync(m => m.salesInquiryId == id);
        if (salesInquiryLine == null)
        {
            return NotFound();
        }

        return View(salesInquiryLine);
    }


    //    // GET: SalesInquiryLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string salesInquiryId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (salesInquiryId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.SalesInquiry.SingleOrDefault(x => x.salesInquiryId == salesInquiryId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: SalesInquiryLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.SalesInquiryLine.SingleOrDefault(m => m.salesInquiryLineId == id);
        var selected = _context.SalesInquiry.SingleOrDefault(m => m.salesInquiryId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName");
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            //ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            if (check == null)
        {
            SalesInquiryLine objline = new SalesInquiryLine();
            objline.salesInquiry = selected;
            objline.salesInquiryId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: SalesInquiryLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("salesInquiryLineId,salesInquiryId,chargeHeadId,saleCategory,ladenContainerId,ladenPrice,count,totalPrice,part,currencyId,createdAt")] SalesInquiryLine salesInquiryLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(salesInquiryLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", salesInquiryLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryId", salesInquiryLine.salesInquiryId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", salesInquiryLine.currencyId);
               
                
            return View(salesInquiryLine);
    }

    // GET: SalesInquiryLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var salesInquiryLine = await _context.SalesInquiryLine.SingleOrDefaultAsync(m => m.salesInquiryLineId == id);
        if (salesInquiryLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", salesInquiryLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryId", salesInquiryLine.salesInquiryId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", salesInquiryLine.currencyId);

            return View(salesInquiryLine);
    }

    // POST: SalesInquiryLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("salesInquiryLineId,salesInquiryId,chargeHeadId,saleCategory,ladenContainerId,ladenPrice,count,totalPrice,part,currencyId,createdAt")] SalesInquiryLine salesInquiryLine)
    {
        if (id != salesInquiryLine.salesInquiryLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(salesInquiryLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesInquiryLineExists(salesInquiryLine.salesInquiryLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", salesInquiryLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", salesInquiryLine.ladenContainerId);
            //ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", salesInquiryLine.emptyContainerId);
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryId", salesInquiryLine.salesInquiryId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", salesInquiryLine.currencyId);

            return View(salesInquiryLine);
    }

    // GET: SalesInquiryLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var salesInquiryLine = await _context.SalesInquiryLine
                .Include(p => p.salesInquiry)
                .Include(p => p.salesInquiry)
                .SingleOrDefaultAsync(m => m.salesInquiryLineId == id);
        if (salesInquiryLine == null)
        {
            return NotFound();
        }

        return View(salesInquiryLine);
    }




    // POST: SalesInquiryLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var salesInquiryLine = await _context.SalesInquiryLine.SingleOrDefaultAsync(m => m.salesInquiryLineId == id);
            _context.SalesInquiryLine.Remove(salesInquiryLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool SalesInquiryLineExists(string id)
    {
        return _context.SalesInquiryLine.Any(e => e.salesInquiryLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class SalesInquiryLine
        {
          public const string Controller = "SalesInquiryLine";
          public const string Action = "Index";
          public const string Role = "SalesInquiryLine";
          public const string Url = "/SalesInquiryLine/Index";
          public const string Name = "SalesInquiryLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "SalesInquiryLine")]
      public bool SalesInquiryLineRole { get; set; } = false;
  }
}



