﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Purchase Invoice")]
    //[Authorize(Roles = "PurchaseInvoice")]
    public class PurchaseInvoiceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public PurchaseInvoiceController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowPurchaseInvoice(string id)
        {
            PurchaseInvoice obj = await _context.PurchaseInvoice
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.purchaseInvoice)
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.vendor)
                .Include(x => x.billOfLading)
                .Include(x => x.booking)

                .SingleOrDefaultAsync(x => x.purchaseInvoiceId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}
        //[HttpGet]
        //public string GeneratePurchaseSequenceNumber(string inputBranchId)
        //{
        //    var year = DateTime.Now.Year;
        //    var month = DateTime.Now.Month;

        //    var start = 600;
        //    var sequenceId = start + _context.PurchaseInvoice.Where(x => x.branchId == inputBranchId).Count() + 1;

        //    var newSequenceId = sequenceId.ToString().PadLeft((sequenceId.ToString().Length + (4 - sequenceId.ToString().Length)), '0');

        //    var invoiceNumber = year + month + newSequenceId;

        //    return invoiceNumber;

        //}

        public async Task<IActionResult> LineIndex()
        {
            var applicationDbContext = _context.PurchaseInvoiceLine.Include(p => p.purchaseInvoice).Include(p => p.chargeHead).Include(p => p.containerEmpty).Include(p => p.containerLaden).Include(p => p.currency).Include(p => p.purchaseInvoice.booking);

            return View(await applicationDbContext.ToListAsync());
        }


        public JsonResult GetPOLCountryFromBooking(string id)
        {
            var booking = _context.Booking
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .SingleOrDefault(x => x.bookingId.Equals(id));

            var bookingLocationCountryName = booking.fromLocation.locationName;

            return Json(new { polCountryName = bookingLocationCountryName });

        }

        public JsonResult GetPODCountryToBooking(string id)
        {

            var booking = _context.Booking
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .SingleOrDefault(x => x.bookingId.Equals(id));

            var bookingLocationCountryName = booking.toLocation.locationName;

            return Json(new { podCountryName = bookingLocationCountryName });

        }

        public JsonResult GetNumberOfContainers(string id)
        {
            var billOfLading = _context.BillOfLoading
                .SingleOrDefault(x => x.billOfLoadingId.Equals(id));

            return Json(new { totalNumberContainers = billOfLading.totalNumberContainers });

        }


        public async Task<IActionResult> Finish(string id)
        {
            var invoice = await _context.PurchaseInvoice
                    .Include(x => x.booking)
                    .Include(p => p.branch)
                        .SingleOrDefaultAsync(x => x.purchaseInvoiceId.Equals(id));
            invoice.status = InvoiceStatus.COMPLETED;
            invoice.totalInvoiceAmount = _context.PurchaseInvoice.Where(x => x.purchaseInvoiceId == id).Sum(x => x.totalAmount);
            invoice.netBalanceAmount = invoice.totalInvoiceAmount - (invoice.paymentAmount) - invoice.tdsAmount;
            _context.Update(invoice);
            await _context.SaveChangesAsync();
            //return View("Details", invoice);
            return Json(new { success = true, message = "Invoice Finished and saved successfully!" });
        }



        public async Task<IActionResult> PrintPurchaseInvoice(string id)
        {
            PurchaseInvoice obj = await _context.PurchaseInvoice
           //.Include(x => x.vendor)
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.purchaseInvoice)
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.booking)
                .Include(x => x.booking.fromLocation)
                .Include(x => x.booking.toLocation)
                .Include(x => x.booking.bookingSlot)
                .Include(x => x.booking.bookingSlot.vendor)
                .Include(x => x.billOfLading)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.vendor)
                .SingleOrDefaultAsync(x => x.purchaseInvoiceId.Equals(id));
            return View(obj);
        }

        // GET: PurchaseInvoice
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<PurchaseInvoice> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
                applicationDbContext = _context.PurchaseInvoice.OrderByDescending(x => x.createdAt)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.booking)
                    .Include(p => p.billOfLading)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.vendor).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.PurchaseInvoice.OrderByDescending(x => x.createdAt)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.booking)
                    .Include(p => p.billOfLading)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.vendor);
            }




                return View(await applicationDbContext.ToListAsync());
        }

        // GET: PurchaseInvoice/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var purchaseInvoice = await _context.PurchaseInvoice
                    .Include(x => x.purchaseInvoiceLine)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.purchaseInvoiceLine).ThenInclude(x => x.containerEmpty)
                    .Include(x => x.branch)
                    .Include(x => x.booking)
                    .Include(x => x.billOfLading)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(x => x.vendor)
                        .SingleOrDefaultAsync(m => m.purchaseInvoiceId == id);
            if (purchaseInvoice == null)
            {
                return NotFound();
            }

            //purchaseInvoice.totalOrderAmount = purchaseInvoice.purchaseInvoice.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(purchaseInvoice);
            await _context.SaveChangesAsync();

            return View(purchaseInvoice);
        }


        // GET: PurchaseInvoice/Create
        public IActionResult Create()
        {
            PurchaseInvoice purchaseInvoice = new PurchaseInvoice();
            ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["bookingId"] = new SelectList(_context.Booking.Where(x => x.status == Status.BOOKED), "bookingId", "bookingNumber");
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");

            //var year = DateTime.Now.Year;
            //var month = DateTime.Now.ToString("MM");
            //var branchId = _service.GetUserBranchId(User.Identity.Name);

            //var start = 000;
            //var sequenceId = start + _context.PurchaseInvoice.Where(x => x.branchId == branchId).Count() + 1;

            //var newSequenceId = sequenceId.ToString().PadLeft((sequenceId.ToString().Length + (3 - sequenceId.ToString().Length)), '0');

            //var invoiceNumber = year + "" + month + "" + newSequenceId;

            ////Check if invoice/ sequence number is same in table
            //var duplicateInvoiceNumber = _context.PurchaseInvoice.Any(x => x.purchaseSequenceNumber == invoiceNumber);

            //if (duplicateInvoiceNumber)
            //{
            //    var newInvoiceNumber = sequenceId + 1;

            //    var newSkipInvoiceId = newInvoiceNumber.ToString().PadLeft((newInvoiceNumber.ToString().Length + (3 - newInvoiceNumber.ToString().Length)), '0');

            //    invoiceNumber = year + "" + month + "" + newSkipInvoiceId;
            //}

            //ViewData["purchaseSequenceNumber"] = invoiceNumber;


            return View(purchaseInvoice);
        }




        // POST: PurchaseInvoice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("purchaseInvoiceId,purchaseSequenceNumber,vendorInvoiceNumber,purchaseInvoiceDate,branchId,vendorType,vendorId,fromlocationId,tolocationId,noOfContainers,bookingDate,bookingId,blNumber,utrNumber,totalAmount,totalGSTAmount,totalInvoiceAmount,netBalanceAmount,tdsAmount,paymentAmount,paymentDate,remark,status,preparedBy, bankRemark,createdAt")] PurchaseInvoice purchaseInvoice)
        {
            if (ModelState.IsValid)
            {
                var duplicateSequenceNumberExists = _context.PurchaseInvoice.Any(x => x.purchaseSequenceNumber.Contains(purchaseInvoice.purchaseSequenceNumber));

                if (duplicateSequenceNumberExists)
                {
                    var newSequenceNumber = GenerateSequenceNumber(purchaseInvoice.branchId);
                    purchaseInvoice.purchaseSequenceNumber = newSequenceNumber;
                }

                _context.Add(purchaseInvoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = purchaseInvoice.purchaseInvoiceId });
            }
            ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["bookingId"] = new SelectList(_context.Booking.Where(x => x.status == Status.BOOKED), "bookingId", "bookingNumber");
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //var year = DateTime.Now.Year;
            //var month = DateTime.Now.ToString("MM");
            //var branchId = _service.GetUserBranchId(User.Identity.Name);

            //var start = 000;
            //var sequenceId = start + _context.PurchaseInvoice.Where(x => x.branchId == branchId).Count() + 1;

            //var newSequenceId = sequenceId.ToString().PadLeft((sequenceId.ToString().Length + (3 - sequenceId.ToString().Length)), '0');

            //var invoiceNumber = year + "" + month + "" + newSequenceId;
            //ViewData["purchaseSequenceNumber"] = invoiceNumber;
            


            return View(purchaseInvoice);
        }


        [HttpGet]
        public string GenerateSequenceNumber(string inputBranchId)
        {
            var year = DateTime.Now.Year;
            var month = DateTime.Now.ToString("MM");
            var inputBranch = _context.Branch.SingleOrDefault(x => x.branchId == inputBranchId);

            var start = 000;
            var sequenceId = start + _context.PurchaseInvoice.Where(x => x.branchId == inputBranch.branchId).Count() + 1;

            var newSequenceId = sequenceId.ToString().PadLeft((sequenceId.ToString().Length + (3 - sequenceId.ToString().Length)), '0');


            var sequenceNumber = year + "" + month + "" + newSequenceId;


        //Check if invoice/ sequence number is same in table

            var duplicateSequenceNumber = _context.PurchaseInvoice.Any(x => x.purchaseSequenceNumber == sequenceNumber);
            //var currentCheckedSequenceId = sequenceId + 1;

            if (duplicateSequenceNumber)
            {
                var newInvoiceNumber = sequenceId + 1;

                var newSkipSequenceId = newInvoiceNumber.ToString().PadLeft((sequenceId.ToString().Length + (3 - sequenceId.ToString().Length)), '0');

                sequenceNumber = year + "" + month + "" + newSkipSequenceId;

            }


            return sequenceNumber;

        }



        // GET: PurchaseInvoice/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var purchaseInvoice = await _context.PurchaseInvoice.Include(x => x.purchaseInvoiceLine).SingleOrDefaultAsync(m => m.purchaseInvoiceId == id);
            if (purchaseInvoice == null)
            {
                return NotFound();
            }

            //purchaseInvoice.totalOrderAmount = purchaseInvoice.purchaseInvoiceLine.Sum(x => x.totalAmount);
            //purchaseInvoice.totalDiscountAmount = purchaseInvoice.purchaseInvoiceLine.Sum(x => x.discountAmount);
            _context.Update(purchaseInvoice);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", purchaseInvoice.vendorId);
            ViewData["bookingId"] = new SelectList(_context.Booking.Where(x => x.status == Status.BOOKED), "bookingId", "bookingNumber");
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", purchaseInvoice.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", purchaseInvoice.tolocationId);
            //ViewData["customerId"] = new SelectList(_context.Customer
            //                  .Where(u => u.isActive == true).ToList()
            //                  .Select(u => new {
            //                      customerId = u.customerId,
            //                      CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
            //                  }),
            //                      "vendorId", "vendorName", purchaseInvoice.vendorId);

            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(purchaseInvoice);
        }

        // POST: PurchaseInvoice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("purchaseInvoiceId,purchaseSequenceNumber,vendorInvoiceNumber,purchaseInvoiceDate,branchId,vendorType,vendorId,fromlocationId,tolocationId,noOfContainers,bookingDate,bookingId,blNumber,utrNumber,totalAmount,totalGSTAmount,totalInvoiceAmount,netBalanceAmount,tdsAmount,paymentAmount,paymentDate,remark,status,preparedBy, bankRemark,createdAt")] PurchaseInvoice purchaseInvoice)
        {
            if (id != purchaseInvoice.purchaseInvoiceId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(purchaseInvoice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PurchaseInvoiceExists(purchaseInvoice.purchaseInvoiceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", purchaseInvoice.vendorId);
            ViewData["bookingId"] = new SelectList(_context.Booking.Where(x => x.status == Status.BOOKED), "bookingId", "bookingNumber");
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", purchaseInvoice.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", purchaseInvoice.tolocationId);
            //ViewData["customerId"] = new SelectList(_context.Customer
            //                  .Where(u => u.isActive == true).ToList()
            //                  .Select(u => new {
            //                      customerId = u.customerId,
            //                      CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
            //                  }),
            //                      "customerId", "CustomerNameLoc", purchaseInvoice.customerId);
            return View(purchaseInvoice);
        }

        // GET: PurchaseInvoice/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var purchaseInvoice = await _context.PurchaseInvoice
                    .Include(x => x.purchaseInvoiceLine)
                    .Include(p => p.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(x => x.vendor)
                    .SingleOrDefaultAsync(m => m.purchaseInvoiceId == id);
            if (purchaseInvoice == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(purchaseInvoice);
            await _context.SaveChangesAsync();

            return View(purchaseInvoice);
        }




        // POST: PurchaseInvoice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var purchaseInvoice = await _context.PurchaseInvoice
                .Include(x => x.purchaseInvoiceLine)
                .SingleOrDefaultAsync(m => m.purchaseInvoiceId == id);
            try
            {
                _context.PurchaseInvoiceLine.RemoveRange(purchaseInvoice.purchaseInvoiceLine);
                _context.PurchaseInvoice.Remove(purchaseInvoice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(purchaseInvoice);
            }
            
        }

        private bool PurchaseInvoiceExists(string id)
        {
            return _context.PurchaseInvoice.Any(e => e.purchaseInvoiceId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class PurchaseInvoice
        {
            public const string Controller = "PurchaseInvoice";
            public const string Action = "Index";
            public const string Role = "PurchaseInvoice";
            public const string Url = "/PurchaseInvoice/Index";
            public const string Name = "PurchaseInvoice";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "PurchaseInvoice")]
        public bool PurchaseInvoiceRole { get; set; } = false;
    }
}



