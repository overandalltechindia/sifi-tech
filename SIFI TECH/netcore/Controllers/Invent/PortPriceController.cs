﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Services;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "PortPrice")]
    public class PortPriceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public PortPriceController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        // GET: PortPrice
        public async Task<IActionResult> Index()
        {
                var userBranchId = _service.GetUserBranchId(User.Identity.Name);

                IQueryable<PortPrice> applicationDbContext;

                if (userBranchId != null)  //If Non Super Admin
                {

                   applicationDbContext = _context.PortPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.portStoragePriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.vendor)
                    .Include(p => p.branch)
                    .Include(p => p.location).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));

                }
                else //If Super Admin
                {
                    applicationDbContext = _context.PortPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.portPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.portStoragePriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.vendor)
                    .Include(p => p.branch)
                    .Include(p => p.location);
                }

                return View(await applicationDbContext.ToListAsync());
        }

        // GET: PortPrice/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

         

            var portPrice = await _context.PortPrice
        .Include(x => x.portPriceLine)
        .Include(p => p.vendor)
        .Include(x => x.branch)
        .Include(p => p.location)
            .SingleOrDefaultAsync(m => m.portPriceId == id);
            if (portPrice == null)
            {
                return NotFound();
            }

            //oceanPrice.totalOrderAmount = oceanPrice.oceanPrice.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(portPrice);
            await _context.SaveChangesAsync();

            return View(portPrice);





        }


        // GET: PortPrice/Create
        public IActionResult Create()
        {
            //var model = new PortPrice();
            //var portPriceNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#PP";
            //model.portPriceNumber = portPriceNumber;
            //return View(model);

            PortPrice portPrice = new PortPrice();
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                           .Where(u => u.isActive == true).ToList()
                                           .Select(u => new {
                                               vendorId = u.vendorId,
                                               VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                           }),
                                               "vendorId", "VendorNameLoc"); ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName");
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            return View(portPrice);
        }




        // POST: PortPrice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("portPriceId,portPriceNumber,branchId,locationId,vendorId,portPriceDate,fromDate,toDate,remark,preparedBy,createdAt")] PortPrice portPrice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(portPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = portPrice.portPriceId });
            }
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            return View(portPrice);
        }

        // GET: PortPrice/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portPrice = await _context.PortPrice.SingleOrDefaultAsync(m => m.portPriceId == id);
            if (portPrice == null)
            {
                return NotFound();
            }
            _context.Update(portPrice);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                 .Where(u => u.isActive == true).ToList()
                                 .Select(u => new {
                                     vendorId = u.vendorId,
                                     VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                 }),
                                     "vendorId", "VendorNameLoc", portPrice.vendorId);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName", portPrice.locationId);

            return View(portPrice);
        }

        // POST: Vendor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("portPriceId,portPriceNumber,branchId,locationId,vendorId,portPriceDate,fromDate,toDate,remark,preparedBy,createdAt")] PortPrice portPrice)
        {
            if (id != portPrice.portPriceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(portPrice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortPriceExists(portPrice.portPriceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                 .Where(u => u.isActive == true).ToList()
                                 .Select(u => new {
                                     vendorId = u.vendorId,
                                     VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                 }),
                                     "vendorId", "VendorNameLoc", portPrice.vendorId);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName", portPrice.locationId);
       
            return View(portPrice);
        }

        // GET: PortPrice/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portPrice = await _context.PortPrice
                    .Include(x => x.portPriceLine)
                    .Include(x => x.portStoragePriceLine)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.location)
             
                    .SingleOrDefaultAsync(m => m.portPriceId == id);

            if (portPrice == null)
            {
                return NotFound();
            }

            return View(portPrice);
        }




        // POST: PortPrice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var portPrice = await _context.PortPrice.Include(x => x.portPriceLine).Include(x => x.portStoragePriceLine).SingleOrDefaultAsync(m => m.portPriceId == id);
            try
            {
                _context.PortPriceLine.RemoveRange(portPrice.portPriceLine);
                _context.PortStoragePriceLine.RemoveRange(portPrice.portStoragePriceLine);
                _context.PortPrice.Remove(portPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(portPrice);
            }
            
        }

        private bool PortPriceExists(string id)
        {
            return _context.PortPrice.Any(e => e.portPriceId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class PortPrice
        {
            public const string Controller = "PortPrice";
            public const string Action = "Index";
            public const string Role = "PortPrice";
            public const string Url = "/PortPrice/Index";
            public const string Name = "PortPrice";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "PortPrice")]
        public bool PortPriceRole { get; set; } = false;
    }
}



