﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "BookingSlotLine")]
    public class BookingSlotLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingSlotLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BookingSlotLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BookingSlotLine.Include(p => p.bookingSlot).Include(p => p.containerLaden);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: BookingSlotLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlotLine = await _context.BookingSlotLine
                    .Include(p => p.bookingSlot)
                    .Include(p => p.containerLaden)
                        .SingleOrDefaultAsync(m => m.bookingSlotId == id);
            if (bookingSlotLine == null)
            {
                return NotFound();
            }

            return View(bookingSlotLine);
        }





        // GET: BookingSlotLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.BookingSlotLine.SingleOrDefault(m => m.bookingSlotLineId == id);
            var selected = _context.BookingSlot.SingleOrDefault(m => m.bookingSlotId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotId");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            if (check == null)
            {
                BookingSlotLine objline = new BookingSlotLine();
                objline.bookingSlot = selected;
                objline.bookingSlotId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: BookingSlotLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("bookingSlotLineId,bookingSlotLineNumber,bookingSlotId,qty,ladenContainerId,ladenPrice,containerCategory,createdAt")] BookingSlotLine bookingSlotLine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookingSlotLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotId", bookingSlotLine.bookingSlotId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", bookingSlotLine.ladenContainerId);



            return View(bookingSlotLine);
        }

        // GET: BookingSlotLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlotLine = await _context.BookingSlotLine.SingleOrDefaultAsync(m => m.bookingSlotLineId == id);
            if (bookingSlotLine == null)
            {
                return NotFound();
            }


            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotId", bookingSlotLine.bookingSlotId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", bookingSlotLine.ladenContainerId);


            return View(bookingSlotLine);
        }

        // POST: BookingSlotLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("bookingSlotLineId,bookingSlotLineNumber,bookingSlotId,qty,ladenContainerId,ladenPrice,containerCategory,createdAt")] BookingSlotLine bookingSlotLine)
        {
            if (id != bookingSlotLine.bookingSlotLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookingSlotLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingSlotLineExists(bookingSlotLine.bookingSlotLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotId", bookingSlotLine.bookingSlotId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", bookingSlotLine.ladenContainerId);


            return View(bookingSlotLine);
        }

        // GET: BookingSlotLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlotLine = await _context.BookingSlotLine
                    .Include(p => p.bookingSlot)
                    .Include(p => p.containerLaden)
                    .SingleOrDefaultAsync(m => m.bookingSlotLineId == id);
            if (bookingSlotLine == null)
            {
                return NotFound();
            }

            return View(bookingSlotLine);
        }




        // POST: BookingSlotLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var bookingSlotLine = await _context.BookingSlotLine.SingleOrDefaultAsync(m => m.bookingSlotLineId == id);
            _context.BookingSlotLine.Remove(bookingSlotLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingSlotLineExists(string id)
        {
            return _context.BookingSlotLine.Any(e => e.bookingSlotLineId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class BookingSlotLine
        {
            public const string Controller = "BookingSlotLine";
            public const string Action = "Index";
            public const string Role = "BookingSlotLine";
            public const string Url = "/BookingSlotLine/Index";
            public const string Name = "BookingSlotLine";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "BookingSlotLine")]
        public bool BookingSlotLineRole { get; set; } = false;
    }
}



