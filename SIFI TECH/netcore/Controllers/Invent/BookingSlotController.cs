﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Booking Slot")]
    [Authorize(Roles = "BookingSlot")]
    public class BookingSlotController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public BookingSlotController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowBookingSlot(string id)
        {
            BookingSlot obj = await _context.BookingSlot
                .Include(x => x.bookingSlotLine).ThenInclude(x => x.bookingSlot)
                .Include(x => x.bookingSlotLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.branch)
                .Include(x => x.vendor)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)

                .SingleOrDefaultAsync(x => x.bookingSlotId.Equals(id));

            _context.Update(obj);

            return View(obj);
        }


        public async Task<IActionResult> PrintBookingSlot(string id)
        {
            BookingSlot obj = await _context.BookingSlot
          .Include(x => x.bookingSlotLine).ThenInclude(x => x.bookingSlot)
               .Include(x => x.bookingSlotLine).ThenInclude(x => x.bookingSlot)
                .Include(x => x.bookingSlotLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.vendor)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.branch)

                .SingleOrDefaultAsync(x => x.bookingSlotId.Equals(id));
            return View(obj);
        }

        // GET: BookingSlot
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<BookingSlot> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {

               applicationDbContext = _context.BookingSlot.OrderByDescending(x => x.createdAt)
                    .Include(x => x.bookingSlotLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.BookingSlot.OrderByDescending(x => x.createdAt)
                    .Include(x => x.bookingSlotLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation);
            }
                return View(await applicationDbContext.ToListAsync());
        }

        // GET: BookingSlot/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlot = await _context.BookingSlot
                    .Include(x => x.bookingSlotLine)
                    .Include(x => x.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)

                        .SingleOrDefaultAsync(m => m.bookingSlotId == id);
            if (bookingSlot == null)
            {
                return NotFound();
            }


            _context.Update(bookingSlot);
            await _context.SaveChangesAsync();

            return View(bookingSlot);
        }


        // GET: BookingSlot/Create
        public IActionResult Create()
        {
            BookingSlot bookingSlot = new BookingSlot();

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                            .Where(u => u.isActive == true).ToList()
                                            .Select(u => new {
                                                vendorId = u.vendorId,
                                                VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                            }),
                                                "vendorId", "VendorNameLoc");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");

            return View(bookingSlot);
        }




        // POST: BookingSlot/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("bookingSlotId,bookingSlotNumber,branchId,vendorId,fromDate,toDate,bookingSlotDate,remark,preparedBy,fromlocationId,tolocationId,etaDate,etdDate,vesselName,service,documentCuttofftDate,siCuttofftDate,documentCuttofftDateIcd,hazoggCuttofftDate,gateOpenCuttofftDate,gateCloseCuttofftDate,vgmCuttofftDate,formCuttofftDate,originFreeDays,originPortStorageDays,destinationFreeDays,destinationPortStorageDays,status,createdAt")] BookingSlot bookingSlot)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookingSlot);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = bookingSlot.bookingSlotId });
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            return View(bookingSlot);
        }

        // GET: BookingSlot/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlot = await _context.BookingSlot.Include(x => x.bookingSlotLine).SingleOrDefaultAsync(m => m.bookingSlotId == id);
            if (bookingSlot == null)
            {
                return NotFound();
            }


            _context.Update(bookingSlot);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                     .Where(u => u.isActive == true).ToList()
                                     .Select(u => new {
                                         vendorId = u.vendorId,
                                         VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                     }),
                                         "vendorId", "VendorNameLoc", bookingSlot.vendorId);

            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingSlot.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingSlot.tolocationId);

            return View(bookingSlot);
        }

        // POST: BookingSlot/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("bookingSlotId,bookingSlotNumber,branchId,vendorId,fromDate,toDate,bookingSlotDate,remark,preparedBy,fromlocationId,tolocationId,etaDate,etdDate,vesselName,service,documentCuttofftDate,siCuttofftDate,documentCuttofftDateIcd,hazoggCuttofftDate,gateOpenCuttofftDate,gateCloseCuttofftDate,vgmCuttofftDate,formCuttofftDate,originFreeDays,originPortStorageDays,destinationFreeDays,destinationPortStorageDays,status,createdAt")] BookingSlot bookingSlot)
        {
            if (id != bookingSlot.bookingSlotId)
            {
                return NotFound();
            }


            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookingSlot);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingSlotExists(bookingSlot.bookingSlotId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                     .Where(u => u.isActive == true).ToList()
                                     .Select(u => new {
                                         vendorId = u.vendorId,
                                         VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                     }),
                                         "vendorId", "VendorNameLoc", bookingSlot.vendorId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingSlot.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingSlot.tolocationId);

            return View(bookingSlot);
        }

        // GET: BookingSlot/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingSlot = await _context.BookingSlot
                    .Include(x => x.bookingSlotLine)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .SingleOrDefaultAsync(m => m.bookingSlotId == id);
            if (bookingSlot == null)
            {
                return NotFound();
            }


            _context.Update(bookingSlot);
            await _context.SaveChangesAsync();

            return View(bookingSlot);
        }




        // POST: BookingSlot/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var bookingSlot = await _context.BookingSlot
                .Include(x => x.bookingSlotLine)
                .SingleOrDefaultAsync(m => m.bookingSlotId == id);
            try
            {
                _context.BookingSlotLine.RemoveRange(bookingSlot.bookingSlotLine);
                _context.BookingSlot.Remove(bookingSlot);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(bookingSlot);
            }

        }

        private bool BookingSlotExists(string id)
        {
            return _context.BookingSlot.Any(e => e.bookingSlotId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class BookingSlot
        {
            public const string Controller = "BookingSlot";
            public const string Action = "Index";
            public const string Role = "BookingSlot";
            public const string Url = "/BookingSlot/Index";
            public const string Name = "BookingSlot";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "BookingSlot")]
        public bool BookingSlotRole { get; set; } = false;
    }
}



