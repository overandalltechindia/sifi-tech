﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    

    [Authorize(Roles = "Container")]
    public class ContainerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ContainerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Container
        public async Task<IActionResult> Index()
        {
            return View(await _context.Container.OrderByDescending(x => x.createdAt).ToListAsync());
        }

        // GET: Container/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var container = await _context.Container
                        .SingleOrDefaultAsync(m => m.containerId == id);
            if (container == null)
            {
                return NotFound();
            }

            return View(container);
        }


        // GET: Container/Create
        public IActionResult Create()
        {
            return View();
        }




        // POST: Container/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("containerId,containerType,containerName")] Container container)
        {
            if (ModelState.IsValid)
            {   
                _context.Add(container);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(container);
        }

        // GET: Container/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var container = await _context.Container.SingleOrDefaultAsync(m => m.containerId == id);
            if (container == null)
            {
                return NotFound();
            }
            return View(container);
        }

        // POST: Container/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("containerId,containerType,containerName")] Container container)
        {
            if (id != container.containerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(container);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContainerExists(container.containerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(container);
        }

        // GET: Container/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var container = await _context.Container
                    .SingleOrDefaultAsync(m => m.containerId == id);
            if (container == null)
            {
                return NotFound();
            }

            return View(container);
        }




        // POST: Container/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var container = await _context.Container.SingleOrDefaultAsync(m => m.containerId == id);
            try
            {
                _context.Container.Remove(container);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(container);
            }
            
        }

        private bool ContainerExists(string id)
        {
            return _context.Container.Any(e => e.containerId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Container
        {
            public const string Controller = "Container";
            public const string Action = "Index";
            public const string Role = "Container";
            public const string Url = "/Container/Index";
            public const string Name = "Container";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Container")]
        public bool ContainerRole { get; set; } = false;
    }
}



