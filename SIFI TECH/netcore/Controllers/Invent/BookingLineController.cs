﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "BookingLine")]
    public class BookingLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BookingLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BookingLine.Include(p => p.booking).Include(p => p.containerLaden).Include(p => p.emptyYardLocation);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: BookingLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingLine = await _context.BookingLine
                    .Include(p => p.booking)
                        .SingleOrDefaultAsync(m => m.bookingId == id);
            if (bookingLine == null)
            {
                return NotFound();
            }

            return View(bookingLine);
        }





        // GET: BookingLine/Create
        public IActionResult Create(string masterid, string id)
        {
            var check = _context.BookingLine.SingleOrDefault(m => m.bookingLineId == id);
            var selected = _context.Booking.SingleOrDefault(m => m.bookingId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingId");
            ViewData["emptyYardLocationId"] = new SelectList(_context.Location, "locationId", "locationName");

            if (masterid != null)
            {
                var bookingLocationId = _context.Booking.SingleOrDefault(x => x.bookingId == masterid).fromlocationId;
                ViewData["ladenContainerId"] = new SelectList(_context.ContainerDetails.Where(x => x.containerStatus == ContainerStatus.empty && x.locationId == bookingLocationId && x.isActive == true), "containerDetailsId", "containerCode");

            }
            if (check == null)
            {
                BookingLine objline = new BookingLine();
                objline.booking = selected;
                objline.bookingId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: BookingLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("bookingLineId,bookingId,ladenContainerId,containerCategory,temperature,length,width,height,weight,emptyYardLocationId,createdAt")] BookingLine bookingLine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bookingLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingId", bookingLine.bookingId);
            ViewData["emptyYardLocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingLine.emptyYardLocationId);


            var bookingLocationId = _context.Booking.SingleOrDefault(x => x.bookingId == bookingLine.bookingId).fromlocationId;
                ViewData["ladenContainerId"] = new SelectList(_context.ContainerDetails.Where(x => x.containerStatus == ContainerStatus.empty && x.locationId == bookingLocationId && x.isActive == true ), "containerDetailsId", "containerCode");



            return View(bookingLine);
        }

        // GET: BookingLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingLine = await _context.BookingLine.SingleOrDefaultAsync(m => m.bookingLineId == id);
            if (bookingLine == null)
            {
                return NotFound();
            }


            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingId", bookingLine.bookingId);
            ViewData["emptyYardLocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingLine.emptyYardLocationId);


            var bookingLocationId = _context.Booking.SingleOrDefault(x => x.bookingId == bookingLine.bookingId).fromlocationId;
                ViewData["ladenContainerId"] = new SelectList(_context.ContainerDetails.Where(x => x.containerStatus == ContainerStatus.empty && x.locationId == bookingLocationId && x.isActive == true), "containerDetailsId", "containerCode");


            return View(bookingLine);
        }

        // POST: BookingLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("bookingLineId,bookingId,ladenContainerId,containerCategory,temperature,length,width,height,weight,emptyYardLocationId,createdAt")] BookingLine bookingLine)
        {
            if (id != bookingLine.bookingLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookingLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingLineExists(bookingLine.bookingLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingId", bookingLine.bookingId);
            ViewData["emptyYardLocationId"] = new SelectList(_context.Location, "locationId", "locationName", bookingLine.emptyYardLocationId);


            var bookingLocationId = _context.Booking.SingleOrDefault(x => x.bookingId == bookingLine.bookingId).fromlocationId;
                ViewData["ladenContainerId"] = new SelectList(_context.ContainerDetails.Where(x => x.containerStatus == ContainerStatus.empty && x.locationId == bookingLocationId && x.isActive == true), "containerDetailsId", "containerCode");

            

            return View(bookingLine);
        }

        // GET: BookingLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookingLine = await _context.BookingLine
                    .Include(p => p.booking)
                    .Include(p => p.booking)
                    .SingleOrDefaultAsync(m => m.bookingLineId == id);
            if (bookingLine == null)
            {
                return NotFound();
            }

            return View(bookingLine);
        }




        // POST: BookingLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var bookingLine = await _context.BookingLine.SingleOrDefaultAsync(m => m.bookingLineId == id);
            _context.BookingLine.Remove(bookingLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingLineExists(string id)
        {
            return _context.BookingLine.Any(e => e.bookingLineId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class BookingLine
        {
            public const string Controller = "BookingLine";
            public const string Action = "Index";
            public const string Role = "BookingLine";
            public const string Url = "/BookingLine/Index";
            public const string Name = "BookingLine";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "BookingLine")]
        public bool BookingLineRole { get; set; } = false;
    }
}



