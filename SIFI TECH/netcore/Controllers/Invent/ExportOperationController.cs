﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Export Operation")]
    [Authorize(Roles = "ExportOperation")]
    public class ExportOperationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public ExportOperationController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowExportOperation(string id)
        {
            ExportOperation obj = await _context.ExportOperation
                .Include(x => x.exportOperationLine).ThenInclude(x => x.exportOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)

                .SingleOrDefaultAsync(x => x.exportOperationId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintExportOperation(string id)
        {
            ExportOperation obj = await _context.ExportOperation
         .Include(x => x.exportOperationLine).ThenInclude(x => x.exportOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)

                .SingleOrDefaultAsync(x => x.exportOperationId.Equals(id));
            return View(obj);
        }

        // GET: ExportOperation
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<ExportOperation> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
                applicationDbContext = _context.ExportOperation.OrderByDescending(x => x.createdAt)
                    .Include(p => p.exportOperationLine).ThenInclude(x => x.containerNumber)
                    .Include(p => p.branch)
                    .Include(p => p.customer)
                    .Include(p => p.booking).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.ExportOperation.OrderByDescending(x => x.createdAt)
                    .Include(p => p.exportOperationLine).ThenInclude(x => x.containerNumber)
                    .Include(p => p.branch)
                    .Include(p => p.customer)
                    .Include(p => p.booking);
            }


                return View(await applicationDbContext.ToListAsync());
        }


        public async Task<IActionResult> LineIndex()
        {
            var applicationDbContext = _context.ExportOperationLine.Include(p => p.exportOperation).Include(p => p.containerNumber).Include(p => p.exportOperation.booking);

            return View(await applicationDbContext.ToListAsync());
        }



        // GET: ExportOperation/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exportOperation = await _context.ExportOperation
           .Include(x => x.exportOperationLine).ThenInclude(x => x.exportOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking).ThenInclude(x => x.salesInquiry)
                .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                .Include(x => x.booking).ThenInclude(x => x.toLocation)

                        .SingleOrDefaultAsync(m => m.exportOperationId == id);
            if (exportOperation == null)
            {
                return NotFound();
            }

            //exportOperation.totalOrderAmount = exportOperation.exportOperation.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(exportOperation);
            await _context.SaveChangesAsync();

            return View(exportOperation);
        }


        // GET: ExportOperation/Create
        public IActionResult Create()
        {
            ExportOperation exportOperation = new ExportOperation();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                                    .Where(u => u.isActive == true).ToList()
                                    .Select(u => new {
                                        customerId = u.customerId,
                                        CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                                    }),
                                        "customerId", "CustomerNameLoc");

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");

            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(exportOperation);
        }




        // POST: ExportOperation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("exportOperationId,exportOperationNumber,exportOperationeDate,branchId,shipper,customerId,bookingId,bookingDate,originFreeDays,originPortStorage,destinationFreeDays,destinationPortStorage,commodity,uan,classes,stuffingType,egmNumber,egmDate,freight,pol,pod,vessel,terminal,lineCode,slotOperator,noOfContainer,type,docHandoverdate,sbNumber,shippingBillNo,surveyor,leoDate,remark,hsCode,containerCategory,status,etaDate,etdDate,preparedby,createdAt")] ExportOperation exportOperation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(exportOperation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = exportOperation.exportOperationId });
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                                  .Where(u => u.isActive == true).ToList()
                                  .Select(u => new {
                                      customerId = u.customerId,
                                      CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                                  }),
                                      "customerId", "CustomerNameLoc");

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            return View(exportOperation);
        }

        // GET: ExportOperation/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exportOperation = await _context.ExportOperation.Include(x => x.exportOperationLine).SingleOrDefaultAsync(m => m.exportOperationId == id);
            if (exportOperation == null)
            {
                return NotFound();
            }

            //exportOperation.totalOrderAmount = exportOperation.exportOperationLine.Sum(x => x.totalAmount);
            //exportOperation.totalDiscountAmount = exportOperation.exportOperationLine.Sum(x => x.discountAmount);
            _context.Update(exportOperation);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc", exportOperation.customerId);
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", exportOperation.bookingId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(exportOperation);
        }

        // POST: ExportOperation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("exportOperationId,exportOperationNumber,exportOperationeDate,branchId,shipper,customerId,bookingId,bookingDate,originFreeDays,originPortStorage,destinationFreeDays,destinationPortStorage,commodity,uan,classes,stuffingType,egmNumber,egmDate,freight,pol,pod,vessel,terminal,lineCode,slotOperator,noOfContainer,type,docHandoverdate,sbNumber,shippingBillNo,surveyor,leoDate,remark,hsCode,containerCategory,status,etaDate,etdDate,preparedby,createdAt")] ExportOperation exportOperation)
        {
            if (id != exportOperation.exportOperationId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(exportOperation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExportOperationExists(exportOperation.exportOperationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc", exportOperation.customerId);
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", exportOperation.bookingId);
            return View(exportOperation);
        }

        // GET: ExportOperation/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exportOperation = await _context.ExportOperation
                    .Include(x => x.exportOperationLine).ThenInclude(x => x.exportOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)
                    .SingleOrDefaultAsync(m => m.exportOperationId == id);
            if (exportOperation == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(exportOperation);
            await _context.SaveChangesAsync();

            return View(exportOperation);
        }




        // POST: ExportOperation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var exportOperation = await _context.ExportOperation
                .Include(x => x.exportOperationLine)
                .SingleOrDefaultAsync(m => m.exportOperationId == id);
            try
            {
                _context.ExportOperationLine.RemoveRange(exportOperation.exportOperationLine);
                _context.ExportOperation.Remove(exportOperation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(exportOperation);
            }
            
        }

        private bool ExportOperationExists(string id)
        {
            return _context.ExportOperation.Any(e => e.exportOperationId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class ExportOperation
        {
            public const string Controller = "ExportOperation";
            public const string Action = "Index";
            public const string Role = "ExportOperation";
            public const string Url = "/ExportOperation/Index";
            public const string Name = "ExportOperation";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "ExportOperation")]
        public bool ExportOperationRole { get; set; } = false;
    }
}



