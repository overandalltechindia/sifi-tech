﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "ImportOperationLine")]
    public class ImportOperationLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ImportOperationLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ImportOperationLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ImportOperationLine.Include(p => p.importOperation).Include(p => p.containerNumber).Include(p => p.emptyYard);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ImportOperationLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var importOperationLine = await _context.ImportOperationLine
                .Include(p => p.importOperation).Include(p => p.containerNumber).Include(p => p.emptyYard)
                    .SingleOrDefaultAsync(m => m.importOperationId == id);
        if (importOperationLine == null)
        {
            return NotFound();
        }

        return View(importOperationLine);
    }


    //    // GET: ImportOperationLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string importOperationId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (importOperationId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.ImportOperation.SingleOrDefault(x => x.importOperationId == importOperationId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: ImportOperationLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.ImportOperationLine.SingleOrDefault(m => m.importOperationLineId == id);
        var selected = _context.ImportOperation.SingleOrDefault(m => m.importOperationId == masterid);
            var list = _context.ChargeHead.ToList();
     
            ViewData["importOperationId"] = new SelectList(_context.ImportOperation, "importOperationId", "importOperationId");
            var bookingLine = _context.BookingLine.Where(x => x.bookingId == selected.bookingId);
            ViewData["containerNumberId"] = new SelectList(bookingLine.Select(x => x.containerLaden), "containerDetailsId", "containerCode");
            ViewData["emptyYardId"] = new SelectList(_context.Location, "locationId", "locationName");


            if (check == null)
        {
            ImportOperationLine objline = new ImportOperationLine();
            objline.importOperation = selected;
            objline.importOperationId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: ImportOperationLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("importOperationLineId,importOperationId,icdCode,partyNomination,emptyValidity,doValidityDate,lineNomination,containerNumberId,ial,ialDate,trpMode,freeDayExpiry,npOfDays,emptyReturnDate,portOutDate,contWeight,sealNo,icdLoadIn,icdLoadOut,icdEmptyIn,icdEmptyOut,emptyYardId,createdAt")] ImportOperationLine importOperationLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(importOperationLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }

            ViewData["importOperationId"] = new SelectList(_context.ImportOperation, "importOperationId", "importOperationId", importOperationLine.importOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", importOperationLine.containerNumberId);
            ViewData["emptyYardId"] = new SelectList(_context.Location, "locationId", "locationName", importOperationLine.emptyYardId);




            return View(importOperationLine);
    }

    // GET: ImportOperationLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var importOperationLine = await _context.ImportOperationLine.SingleOrDefaultAsync(m => m.importOperationLineId == id);
        if (importOperationLine == null)
        {
            return NotFound();
        }

            ViewData["importOperationId"] = new SelectList(_context.ImportOperation, "importOperationId", "importOperationId", importOperationLine.importOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", importOperationLine.containerNumberId);
            ViewData["emptyYardId"] = new SelectList(_context.Location, "locationId", "locationName", importOperationLine.emptyYardId);



            return View(importOperationLine);
    }

    // POST: ImportOperationLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("importOperationLineId,importOperationId,icdCode,partyNomination,emptyValidity,doValidityDate,lineNomination,containerNumberId,ial,ialDate,trpMode,freeDayExpiry,npOfDays,emptyReturnDate,portOutDate,contWeight,sealNo,icdLoadIn,icdLoadOut,icdEmptyIn,icdEmptyOut,emptyYardId,createdAt")] ImportOperationLine importOperationLine)
    {
        if (id != importOperationLine.importOperationLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(importOperationLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportOperationLineExists(importOperationLine.importOperationLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }

            ViewData["importOperationId"] = new SelectList(_context.ImportOperation, "importOperationId", "importOperationId", importOperationLine.importOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", importOperationLine.containerNumberId);
            ViewData["emptyYardId"] = new SelectList(_context.Location, "locationId", "locationName", importOperationLine.emptyYardId);



            return View(importOperationLine);
    }

    // GET: ImportOperationLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var importOperationLine = await _context.ImportOperationLine
                .Include(p => p.importOperation)
                .SingleOrDefaultAsync(m => m.importOperationLineId == id);
        if (importOperationLine == null)
        {
            return NotFound();
        }

        return View(importOperationLine);
    }




    // POST: ImportOperationLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var importOperationLine = await _context.ImportOperationLine.SingleOrDefaultAsync(m => m.importOperationLineId == id);
            _context.ImportOperationLine.Remove(importOperationLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool ImportOperationLineExists(string id)
    {
        return _context.ImportOperationLine.Any(e => e.importOperationLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class ImportOperationLine
        {
          public const string Controller = "ImportOperationLine";
          public const string Action = "Index";
          public const string Role = "ImportOperationLine";
          public const string Url = "/ImportOperationLine/Index";
          public const string Name = "ImportOperationLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "ImportOperationLine")]
      public bool ImportOperationLineRole { get; set; } = false;
  }
}



