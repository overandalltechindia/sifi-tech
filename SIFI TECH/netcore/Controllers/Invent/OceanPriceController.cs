﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using Newtonsoft.Json;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Ocean Price")]
    [Authorize(Roles = "OceanPrice")]
    public class OceanPriceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;

        public OceanPriceController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;
        }

        public async Task<IActionResult> ShowOceanPrice(string id)
        {
            OceanPrice obj = await _context.OceanPrice
                .Include(x => x.vendor)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.oceanPrice)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.placeofReceipt)
                .Include(x => x.placeofDelivery)
                .Include(x => x.trpPort)

                .SingleOrDefaultAsync(x => x.oceanPriceId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}

        public JsonResult GetPOLCountry(string id)
        {

            var selectedLocationCountryId = (int)_context.Location.FirstOrDefault(x => x.locationId == id).countries;

            return Json(new { polCountryId = selectedLocationCountryId });

        }

        public JsonResult GetPODCountry(string id)
        {

            var selectedLocationCountryId = (int)_context.Location.FirstOrDefault(x => x.locationId == id).countries;

            return Json(new { podCountryId = selectedLocationCountryId });

        }



        public async Task<IActionResult> PrintOceanPrice(string id)
        {
            OceanPrice obj = await _context.OceanPrice
           .Include(x => x.vendor)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.oceanPrice)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.oceanPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.placeofReceipt)
                .Include(x => x.placeofDelivery)
                .Include(x => x.trpPort)

                .SingleOrDefaultAsync(x => x.oceanPriceId.Equals(id));
            return View(obj);
        }

        // GET: OceanPrice
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<OceanPrice> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
                applicationDbContext = _context.OceanPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.vendor)
                    .Include(p => p.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.placeofReceipt)
                    .Include(p => p.placeofDelivery)
                    .Include(p => p.trpPort).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.OceanPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.oceanPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.vendor)
                    .Include(p => p.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.placeofReceipt)
                    .Include(p => p.placeofDelivery)
                    .Include(p => p.trpPort);
            }
            

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: OceanPrice/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oceanPrice = await _context.OceanPrice
                    .Include(x => x.oceanPriceLine)
                    .Include(p => p.vendor)
                    .Include(x => x.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(x => x.placeofReceipt)
                    .Include(x => x.placeofDelivery)
                    .Include(p => p.trpPort)
                        .SingleOrDefaultAsync(m => m.oceanPriceId == id);
            if (oceanPrice == null)
            {
                return NotFound();
            }

            //oceanPrice.totalOrderAmount = oceanPrice.oceanPrice.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(oceanPrice);
            await _context.SaveChangesAsync();

            return View(oceanPrice);
        }


        // GET: OceanPrice/Create
        public IActionResult Create()
        {
            OceanPrice oceanPrice = new OceanPrice();
            var vendorsList = _context.Vendor.ToList();

            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")}),
                                              "vendorId", "VendorNameLoc");

            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName");

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName");
            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);

            var enumData = from Countries e in Enum.GetValues(typeof(Countries))
                           select new
                           {
                               Value = (int)e,
                               Text = e.GetDisplayName().ToString()
                           };

            //var selectedPolLocation = _context.Location.FirstOrDefault(x => x.locationId == id);
            ViewBag.EnumList = new SelectList(enumData, "Value", "Text");



            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(oceanPrice);
        }




        // POST: OceanPrice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("oceanPriceId,oceanPriceNumber,oceanPriceDate,branchId,vendorId,fromDate,toDate,fromlocationId,tolocationId,placeofReceiptId,placeofDeliveryId,polCountry,podCountry,precarriage,onCarriage,via,trpPortId,mode,polTerm,podTerm,freightTerm,status,remark,preparedBy,createdAt")] OceanPrice oceanPrice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(oceanPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = oceanPrice.oceanPriceId });
            }
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName");
            return View(oceanPrice);
        }

        // GET: OceanPrice/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oceanPrice = await _context.OceanPrice.Include(x => x.oceanPriceLine).SingleOrDefaultAsync(m => m.oceanPriceId == id);
            if (oceanPrice == null)
            {
                return NotFound();
            }

            //oceanPrice.totalOrderAmount = oceanPrice.oceanPriceLine.Sum(x => x.totalAmount);
            //oceanPrice.totalDiscountAmount = oceanPrice.oceanPriceLine.Sum(x => x.discountAmount);
            _context.Update(oceanPrice);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc", oceanPrice.vendorId);
            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", oceanPrice.vendorId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.placeofDeliveryId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.trpPortId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(oceanPrice);
        }

        // POST: OceanPrice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("oceanPriceId,oceanPriceNumber,oceanPriceDate,branchId,vendorId,fromDate,toDate,fromlocationId,tolocationId,placeofReceiptId,placeofDeliveryId,polCountry,podCountry,precarriage,onCarriage,via,trpPortId,mode,polTerm,podTerm,freightTerm,status,remark,preparedBy,createdAt")] OceanPrice oceanPrice)
        {
            if (id != oceanPrice.oceanPriceId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(oceanPrice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OceanPriceExists(oceanPrice.oceanPriceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", oceanPrice.branchId);

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            ViewData["vendorId"] = new SelectList(_context.Vendor
                                        .Where(u => u.isActive == true).ToList()
                                        .Select(u => new {
                                            vendorId = u.vendorId,
                                            VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                        }),
                                            "vendorId", "VendorNameLoc", oceanPrice.vendorId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.placeofDeliveryId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", oceanPrice.trpPortId);
            ViewData["POLCountry"] = new SelectList(_context.Location, "POLCountry", "POLCountry", oceanPrice.polCountry);
            ViewData["PODCountry"] = new SelectList(_context.Location, "PODountry", "PODountry", oceanPrice.polCountry);
            return View(oceanPrice);
        }

        // GET: OceanPrice/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oceanPrice = await _context.OceanPrice
                    .Include(x => x.oceanPriceLine)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.placeofReceipt)
                    .Include(p => p.placeofDelivery)
                    .Include(p => p.trpPort)
                    .SingleOrDefaultAsync(m => m.oceanPriceId == id);
            if (oceanPrice == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(oceanPrice);
            await _context.SaveChangesAsync();

            return View(oceanPrice);
        }




        // POST: OceanPrice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var oceanPrice = await _context.OceanPrice
                .Include(x => x.oceanPriceLine)
                .SingleOrDefaultAsync(m => m.oceanPriceId == id);
            try
            {
                _context.OceanPriceLine.RemoveRange(oceanPrice.oceanPriceLine);
                _context.OceanPrice.Remove(oceanPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(oceanPrice);
            }
            
        }

        private bool OceanPriceExists(string id)
        {
            return _context.OceanPrice.Any(e => e.oceanPriceId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class OceanPrice
        {
            public const string Controller = "OceanPrice";
            public const string Action = "Index";
            public const string Role = "OceanPrice";
            public const string Url = "/OceanPrice/Index";
            public const string Name = "OceanPrice";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "OceanPrice")]
        public bool OceanPriceRole { get; set; } = false;
    }
}



