﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;
using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    //[Authorize(Roles = "PurchaseInvoiceLine")]
    public class PurchaseInvoiceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PurchaseInvoiceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PurchaseInvoiceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PurchaseInvoiceLine.Include(p => p.purchaseInvoice).Include(p => p.chargeHead);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: PurchaseInvoiceLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var purchaseInvoiceLine = await _context.PurchaseInvoiceLine
                    .Include(p => p.purchaseInvoice)
                    .Include(p => p.purchaseInvoice.booking)
                        .SingleOrDefaultAsync(m => m.purchaseInvoiceId == id);
            if (purchaseInvoiceLine == null)
            {
                return NotFound();
            }

            return View(purchaseInvoiceLine);
        }


    //    // GET: PurchaseInvoiceLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string purchaseInvoiceId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (purchaseInvoiceId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.PurchaseInvoice.SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    //Laden
    public decimal? GetOceanPriceLadenAmount(string purchaseInvoiceId, string chargeHeadId, string ladenContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (ladenContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.oceanPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

        var oceanPrice = purchaseInvoice.booking.oceanPrice;

        var oceanPriceLine = _context.OceanPriceLine.Include(x => x.oceanPrice).Where(x => x.oceanPriceId == oceanPrice.oceanPriceId).ToList();

        var ladenChargeHeadAmount = oceanPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.ladenContainerId == ladenContainerId).ladenPrice;            

        //if (ladenChargeHeadAmount == '')
        //{
        //    ladenChargeHeadAmount = 0;
        //}

        return ladenChargeHeadAmount;
    }


    public decimal? GetInlandPriceLadenAmount(string purchaseInvoiceId, string chargeHeadId, string ladenContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (ladenContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.inlandPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

        var inlandPrice = purchaseInvoice.booking.inlandPrice;

        var inlandPriceLine = _context.InlandPriceLine.Include(x => x.inlandPrice).Where(x => x.inlandPriceId == inlandPrice.inlandPriceId).ToList();

        var ladenChargeHeadAmount = inlandPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.ladenContainerId == ladenContainerId).ladenPrice;

        return ladenChargeHeadAmount;
    }


    public decimal? GetPortPriceLadenAmount(string purchaseInvoiceId, string chargeHeadId, string ladenContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (ladenContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.portPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

        var portPrice = purchaseInvoice.booking.portPrice;

        var portPriceLine = _context.PortPriceLine.Include(x => x.portPrice).Where(x => x.portPriceId == portPrice.portPriceId).ToList();

        var ladenChargeHeadAmount = portPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.ladenContainerId ==ladenContainerId).ladenPrice;

        return ladenChargeHeadAmount;
    }

    public decimal? GetLocalPriceLadenAmount(string purchaseInvoiceId, string chargeHeadId, string ladenContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (ladenContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.localPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

        var localPrice = purchaseInvoice.booking.localPrice;

        var inlandPriceLine = _context.LocalPriceLine.Include(x => x.localPrice).Where(x => x.localPriceId == localPrice.localPriceId).ToList();

        var ladenChargeHeadAmount = inlandPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.ladenContainerId ==ladenContainerId).ladenPrice;

        return ladenChargeHeadAmount;
    }




        
    //Empty
    public decimal? GetOceanPriceEmptyAmount(string purchaseInvoiceId, string chargeHeadId, string emptyContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (emptyContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.oceanPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

        //Ocean

        var oceanPrice = purchaseInvoice.booking.oceanPrice;

        var oceanPriceLine = _context.OceanPriceLine.Include(x => x.oceanPrice).Where(x => x.oceanPriceId == oceanPrice.oceanPriceId).ToList();

        var emptyChargeHeadAmount = oceanPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.emptyContainerId ==emptyContainerId).emptyPrice;
        
        if (emptyChargeHeadAmount == null)
        {
            emptyChargeHeadAmount = 0;
        }

        return emptyChargeHeadAmount;

        
    }

    public decimal? GetInlandPriceEmptyAmount(string purchaseInvoiceId, string chargeHeadId, string emptyContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (emptyContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.inlandPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

 
        var inlandPrice = purchaseInvoice.booking.inlandPrice;

        var inlandPriceLine = _context.InlandPriceLine.Include(x => x.inlandPrice).Where(x => x.inlandPriceId==inlandPrice.inlandPriceId).ToList();

        var emptyChargeHeadAmount = inlandPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.emptyContainerId==emptyContainerId).emptyPrice;

        return emptyChargeHeadAmount;


    }

    public decimal? GetPortPriceEmptyAmount(string purchaseInvoiceId, string chargeHeadId, string emptyContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (emptyContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.portPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);

 
        var portPrice = purchaseInvoice.booking.portPrice;

        var portPriceLine = _context.PortPriceLine.Include(x => x.portPrice).Where(x => x.portPriceId == portPrice.portPriceId).ToList();

        var emptyChargeHeadAmount = portPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.emptyContainerId == emptyContainerId).emptyPrice;

        return emptyChargeHeadAmount;


    }

    public decimal? GetLocalPriceEmptyAmount(string purchaseInvoiceId, string chargeHeadId, string emptyContainerId)
    {
        if (purchaseInvoiceId == null)
        {
            return null;
        }
        if (chargeHeadId == null)
        {
            return null;
        }
        if (emptyContainerId == null)
        {
            return null;
        }

        var purchaseInvoice = _context.PurchaseInvoice
             .Include(x => x.booking)
             .Include(x => x.booking.localPrice)
             .SingleOrDefault(x => x.purchaseInvoiceId == purchaseInvoiceId);


        var localPrice = purchaseInvoice.booking.localPrice;

        var localPriceLine = _context.LocalPriceLine.Include(x => x.localPrice).Where(x => x.localPriceId == localPrice.localPriceId).ToList();

        var emptyChargeHeadAmount = localPriceLine.SingleOrDefault(x => x.chargeHeadId == chargeHeadId && x.emptyContainerId==emptyContainerId).emptyPrice;

        return emptyChargeHeadAmount;

    }


    




        
    // GET: PurchaseInvoiceLine/Create
    public async Task<IActionResult> Create(string masterid, string id)
    {
        var check = _context.PurchaseInvoiceLine.SingleOrDefault(m => m.purchaseInvoiceLineId == id);
        var selected = _context.PurchaseInvoice.Include(x => x.booking).SingleOrDefault(m => m.purchaseInvoiceId == masterid);

        //Ocean
        var oceanPriceChargeHeads = await _context.OceanPriceLine.Include(x => x.chargeHead).Where(x => x.oceanPrice.oceanPriceId == selected.booking.oceanPriceId).Select(x => x.chargeHead).ToListAsync();

        var oceanPriceLadenContainers = await _context.OceanPriceLine.Include(x => x.containerLaden).Where(x => x.oceanPrice.oceanPriceId == selected.booking.oceanPriceId).Select(x => x.containerLaden).ToListAsync();

        var oceanPriceEmptyContainers = await _context.OceanPriceLine.Include(x => x.containerEmpty).Where(x => x.oceanPrice.oceanPriceId == selected.booking.oceanPriceId).Select(x => x.containerEmpty).ToListAsync();

        ViewData["chargeHeadId"] = new SelectList(oceanPriceChargeHeads, "chargeHeadId", "chargeHeadName");
        ViewData["ladenContainerId"] = new SelectList(oceanPriceLadenContainers, "containerId", "containerName");
        ViewData["emptyContainerId"] = new SelectList(oceanPriceEmptyContainers, "containerId", "containerName");


        //Inland
        var inlandPriceChargeHeads = await _context.InlandPriceLine.Include(x => x.chargeHead).Where(x => x.inlandPrice.inlandPriceId == selected.booking.inlandPriceId).Select(x => x.chargeHead).ToListAsync();

        var inlandPriceLadenContainers = await _context.InlandPriceLine.Include(x => x.containerLaden).Where(x => x.inlandPrice.inlandPriceId == selected.booking.inlandPriceId).Select(x => x.containerLaden).ToListAsync();

        var inlandPriceEmptyContainers = await _context.InlandPriceLine.Include(x => x.containerEmpty).Where(x => x.inlandPrice.inlandPriceId == selected.booking.inlandPriceId).Select(x => x.containerEmpty).ToListAsync();

        ViewData["inLandChargeHeadId"] = new SelectList(inlandPriceChargeHeads, "chargeHeadId", "chargeHeadName");
        ViewData["inlandLadenContainerId"] = new SelectList(inlandPriceLadenContainers, "containerId", "containerName");
        ViewData["inlandEmptyContainerId"] = new SelectList(inlandPriceEmptyContainers, "containerId", "containerName");

        
        //Port
        var portPriceChargeHeads = await _context.PortPriceLine.Include(x => x.chargeHead).Where(x => x.portPrice.portPriceId == selected.booking.portPriceId).Select(x => x.chargeHead).ToListAsync();

        var portPriceLadenContainers = await _context.PortPriceLine.Include(x => x.containerLaden).Where(x => x.portPrice.portPriceId == selected.booking.portPriceId).Select(x => x.containerLaden).ToListAsync();

        var portPriceEmptyContainers = await _context.PortPriceLine.Include(x => x.containerEmpty).Where(x => x.portPrice.portPriceId == selected.booking.portPriceId).Select(x => x.containerEmpty).ToListAsync();

        ViewData["portChargeHeadId"] = new SelectList(portPriceChargeHeads, "chargeHeadId", "chargeHeadName");
        ViewData["portLadenContainerId"] = new SelectList(portPriceLadenContainers, "containerId", "containerName");
        ViewData["portEmptyContainerId"] = new SelectList(portPriceEmptyContainers, "containerId", "containerName");

        
        //Local
        var localPriceChargeHeads = await _context.LocalPriceLine.Include(x => x.chargeHead).Where(x => x.localPrice.localPriceId == selected.booking.localPriceId).Select(x => x.chargeHead).ToListAsync();

        var localPriceLadenContainers = await _context.LocalPriceLine.Include(x => x.containerLaden).Where(x => x.localPrice.localPriceId == selected.booking.localPriceId).Select(x => x.containerLaden).ToListAsync();

        var localPriceEmptyContainers = await _context.LocalPriceLine.Include(x => x.containerEmpty).Where(x => x.localPrice.localPriceId == selected.booking.localPriceId).Select(x => x.containerEmpty).ToListAsync();

        ViewData["localChargeHeadId"] = new SelectList(localPriceChargeHeads, "chargeHeadId", "chargeHeadName");
        ViewData["localLadenContainerId"] = new SelectList(localPriceLadenContainers, "containerId", "containerName");
        ViewData["localEmptyContainerId"] = new SelectList(localPriceEmptyContainers, "containerId", "containerName");




        ViewData["purchaseInvoiceId"] = new SelectList(_context.PurchaseInvoice, "purchaseInvoiceId", "purchaseInvoiceId");
        ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
        if (check == null)
        {
            PurchaseInvoiceLine objline = new PurchaseInvoiceLine();
            objline.purchaseInvoice = selected;
            objline.purchaseInvoiceId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: PurchaseInvoiceLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("purchaseInvoiceLineId,purchaseInvoiceId,chargeHeadId,containerEmptyLaden,ladenContainerId,emptyContainerId,currencyId,quantity,totalAmountWithoutGST,amount,rate,sac,cgst,sgst,igst,totalGSTAmount,totalAmountWithGST,createdAt")] PurchaseInvoiceLine purchaseInvoiceLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(purchaseInvoiceLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", purchaseInvoiceLine.chargeHeadId);
            ViewData["purchaseInvoiceId"] = new SelectList(_context.PurchaseInvoice, "purchaseInvoiceId", "purchaseInvoiceId", purchaseInvoiceLine.purchaseInvoiceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", purchaseInvoiceLine.currencyId);
               
                
            return View(purchaseInvoiceLine);
    }

    // GET: PurchaseInvoiceLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var purchaseInvoiceLine = await _context.PurchaseInvoiceLine.SingleOrDefaultAsync(m => m.purchaseInvoiceLineId == id);
        if (purchaseInvoiceLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", purchaseInvoiceLine.chargeHeadId);
            ViewData["purchaseInvoiceId"] = new SelectList(_context.PurchaseInvoice, "purchaseInvoiceId", "purchaseInvoiceId", purchaseInvoiceLine.purchaseInvoiceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", purchaseInvoiceLine.currencyId);

            return View(purchaseInvoiceLine);
    }

    // POST: PurchaseInvoiceLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("purchaseInvoiceLineId,purchaseInvoiceId,chargeHeadId,containerEmptyLaden,ladenContainerId,emptyContainerId,currencyId,quantity,totalAmountWithoutGST,amount,rate,sac,cgst,sgst,igst,totalGSTAmount,totalAmountWithGST,createdAt")] PurchaseInvoiceLine purchaseInvoiceLine)
    {
        if (id != purchaseInvoiceLine.purchaseInvoiceLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(purchaseInvoiceLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PurchaseInvoiceLineExists(purchaseInvoiceLine.purchaseInvoiceLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", purchaseInvoiceLine.chargeHeadId);
            ViewData["purchaseInvoiceId"] = new SelectList(_context.PurchaseInvoice, "purchaseInvoiceId", "purchaseInvoiceId", purchaseInvoiceLine.purchaseInvoiceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", purchaseInvoiceLine.currencyId);

            return View(purchaseInvoiceLine);
    }

    // GET: PurchaseInvoiceLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var purchaseInvoiceLine = await _context.PurchaseInvoiceLine
                .Include(p => p.purchaseInvoice)
                .SingleOrDefaultAsync(m => m.purchaseInvoiceLineId == id);
        if (purchaseInvoiceLine == null)
        {
            return NotFound();
        }

        return View(purchaseInvoiceLine);
    }



    // POST: PurchaseInvoiceLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var purchaseInvoiceLine = await _context.PurchaseInvoiceLine.SingleOrDefaultAsync(m => m.purchaseInvoiceLineId == id);
            _context.PurchaseInvoiceLine.Remove(purchaseInvoiceLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool PurchaseInvoiceLineExists(string id)
    {
        return _context.PurchaseInvoiceLine.Any(e => e.purchaseInvoiceLineId == id);
    }

  }
}





    namespace netcore.MVC
    {
      public static partial class Pages
      {
          public static class PurchaseInvoiceLine
            {
              public const string Controller = "PurchaseInvoiceLine";
              public const string Action = "Index";
              public const string Role = "PurchaseInvoiceLine";
              public const string Url = "/PurchaseInvoiceLine/Index";
              public const string Name = "PurchaseInvoiceLine";
          }
      }
    }
    namespace netcore.Models
    {
      public partial class ApplicationUser
      {
          [Display(Name = "PurchaseInvoiceLine")]
          public bool PurchaseInvoiceLineRole { get; set; } = false;
      }
    }



