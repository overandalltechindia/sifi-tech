﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Sales Inquiry")]
    [Authorize(Roles = "SalesInquiry")]
    public class SalesInquiryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public SalesInquiryController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowSalesInquiry(string id)
        {
            SalesInquiry obj = await _context.SalesInquiry
                //.Include(x => x.vendor)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.salesInquiry)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.chargeHead)
                //.Include(x => x.salesInquiryLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.customer)
                .Include(x => x.placeofReceipt)
                .Include(x => x.placeofDelivery)
                .Include(x => x.trpPort)

                .SingleOrDefaultAsync(x => x.salesInquiryId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}

        public JsonResult GetPOLCountry(string id)
        {

            var selectedLocationCountryName = _context.Location.FirstOrDefault(x => x.locationId == id).countries.GetDisplayName();

            return Json(new { polCountryName = selectedLocationCountryName });

        }

        public JsonResult GetPODCountry(string id)
        {

            var selectedLocationCountryName = _context.Location.FirstOrDefault(x => x.locationId == id).countries.GetDisplayName();

            return Json(new { podCountryName = selectedLocationCountryName });

        }



        public async Task<IActionResult> PrintSalesInquiry(string id)
        {
            SalesInquiry obj = await _context.SalesInquiry
           //.Include(x => x.vendor)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.salesInquiry)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.chargeHead)
                //.Include(x => x.salesInquiryLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.salesInquiryLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.customer)
                .Include(x => x.placeofReceipt)
                .Include(x => x.placeofDelivery)
                .Include(x => x.trpPort)
                .SingleOrDefaultAsync(x => x.salesInquiryId.Equals(id));
            return View(obj);
        }

        // GET: SalesInquiry
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<SalesInquiry> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
                applicationDbContext = _context.SalesInquiry.OrderByDescending(x => x.createdAt)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.customer)
                    .Include(p => p.placeofReceipt)
                    .Include(p => p.placeofDelivery)
                    .Include(p => p.trpPort).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.SalesInquiry.OrderByDescending(x => x.createdAt)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.salesInquiryLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.customer)
                    .Include(p => p.placeofReceipt)
                    .Include(p => p.placeofDelivery)
                    .Include(p => p.trpPort);
            }




                return View(await applicationDbContext.ToListAsync());
        }

        // GET: SalesInquiry/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salesInquiry = await _context.SalesInquiry
                    .Include(x => x.salesInquiryLine)
                    //.Include(p => p.vendor)
                    .Include(x => x.branch)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(x => x.customer)
                    .Include(x => x.placeofReceipt)
                    .Include(x => x.placeofDelivery)
                    .Include(x => x.trpPort)
                        .SingleOrDefaultAsync(m => m.salesInquiryId == id);
            if (salesInquiry == null)
            {
                return NotFound();
            }

            //salesInquiry.totalOrderAmount = salesInquiry.salesInquiry.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(salesInquiry);
            await _context.SaveChangesAsync();

            return View(salesInquiry);
        }


        // GET: SalesInquiry/Create
        public IActionResult Create()
        {
            SalesInquiry salesInquiry = new SalesInquiry();
            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["customerId"] = new SelectList(_context.Customer
                                    .Where(u => u.isActive == true).ToList()
                                    .Select(u => new {
                                        customerId = u.customerId,
                                        CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                                    }),
                                        "customerId", "CustomerNameLoc");      
            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(salesInquiry);
        }




        // POST: SalesInquiry/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("salesInquiryId,salesInquiryNumber,salesInquiryDate,branchId,customerId,shipper,salePerson,fromlocationId,tolocationId,polCountry,podCountry,precarriage,onCarriage,via,mode,polTerm,podTerm,freightTerm,fromDate,toDate,remark,customerId,placeofReceiptId,placeofDeliveryId,trpPortId,status,preparedBy,createdAt")] SalesInquiry salesInquiry)
        {
            if (ModelState.IsValid)
            {
                _context.Add(salesInquiry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = salesInquiry.salesInquiryId });
            }
            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["customerId"] = new SelectList(_context.Customer
                                    .Where(u => u.isActive == true).ToList()
                                    .Select(u => new {
                                        customerId = u.customerId,
                                        CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                                    }),
                                        "customerId", "CustomerNameLoc");
            return View(salesInquiry);
        }

        // GET: SalesInquiry/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salesInquiry = await _context.SalesInquiry.Include(x => x.salesInquiryLine).SingleOrDefaultAsync(m => m.salesInquiryId == id);
            if (salesInquiry == null)
            {
                return NotFound();
            }

            //salesInquiry.totalOrderAmount = salesInquiry.salesInquiryLine.Sum(x => x.totalAmount);
            //salesInquiry.totalDiscountAmount = salesInquiry.salesInquiryLine.Sum(x => x.discountAmount);
            _context.Update(salesInquiry);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", salesInquiry.vendorId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.placeofDeliveryId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.trpPortId);
            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc", salesInquiry.customerId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(salesInquiry);
        }

        // POST: SalesInquiry/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("salesInquiryId,salesInquiryNumber,salesInquiryDate,branchId,customerId,shipper,salePerson,fromlocationId,tolocationId,polCountry,podCountry,precarriage,onCarriage,via,mode,polTerm,podTerm,freightTerm,fromDate,toDate,remark,customerId,placeofReceiptId,placeofDeliveryId,trpPortId,status,preparedBy,createdAt")] SalesInquiry salesInquiry)
        {
            if (id != salesInquiry.salesInquiryId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(salesInquiry);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SalesInquiryExists(salesInquiry.salesInquiryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            //ViewData["vendorId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", salesInquiry.vendorId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.placeofDeliveryId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", salesInquiry.trpPortId);
            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc", salesInquiry.customerId);
            return View(salesInquiry);
        }

        // GET: SalesInquiry/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var salesInquiry = await _context.SalesInquiry
                    .Include(x => x.salesInquiryLine)
                    .Include(p => p.branch)
                    //.Include(p => p.vendor)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(x => x.customer)
                    .Include(x => x.placeofReceipt)
                    .Include(x => x.placeofDelivery)
                    .Include(x => x.trpPort)
                    .SingleOrDefaultAsync(m => m.salesInquiryId == id);
            if (salesInquiry == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(salesInquiry);
            await _context.SaveChangesAsync();

            return View(salesInquiry);
        }




        // POST: SalesInquiry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var salesInquiry = await _context.SalesInquiry
                .Include(x => x.salesInquiryLine)
                .SingleOrDefaultAsync(m => m.salesInquiryId == id);
            try
            {
                _context.SalesInquiryLine.RemoveRange(salesInquiry.salesInquiryLine);
                _context.SalesInquiry.Remove(salesInquiry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(salesInquiry);
            }
            
        }

        private bool SalesInquiryExists(string id)
        {
            return _context.SalesInquiry.Any(e => e.salesInquiryId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class SalesInquiry
        {
            public const string Controller = "SalesInquiry";
            public const string Action = "Index";
            public const string Role = "SalesInquiry";
            public const string Url = "/SalesInquiry/Index";
            public const string Name = "SalesInquiry";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "SalesInquiry")]
        public bool SalesInquiryRole { get; set; } = false;
    }
}



