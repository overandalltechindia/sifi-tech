﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "OceanPriceLine")]
    public class OceanPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OceanPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: OceanPriceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.OceanPriceLine.Include(p => p.oceanPrice).Include(p => p.chargeHead).Include(p => p.containerLaden).Include(p => p.containerEmpty);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: OceanPriceLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var oceanPriceLine = await _context.OceanPriceLine
                    .Include(p => p.oceanPrice)
                        .SingleOrDefaultAsync(m => m.oceanPriceId == id);
            if (oceanPriceLine == null)
            {
                return NotFound();
            }

            return View(oceanPriceLine);
        }


    //    // GET: OceanPriceLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string oceanPriceId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (oceanPriceId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.OceanPrice.SingleOrDefault(x => x.oceanPriceId == oceanPriceId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}

    public JsonResult GetChargeHeadsByPart(int partId)
    {
        var chargeHeadsList = _context.ChargeHead.Where(x => (int)x.part == partId && x.category == ChargeHeadCategory.Ocean && x.categoryType == CategoryType.IMP).ToList();

        return Json(chargeHeadsList);
    }


    // GET: OceanPriceLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.OceanPriceLine.SingleOrDefault(m => m.oceanPriceLineId == id);
        var selected = _context.OceanPrice.SingleOrDefault(m => m.oceanPriceId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName");
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice, "oceanPriceId", "oceanPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            if (check == null)
        {
            OceanPriceLine objline = new OceanPriceLine();
            objline.oceanPrice = selected;
            objline.oceanPriceId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: OceanPriceLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("oceanPriceLineId,oceanPriceId,chargeHeadId,containerEmptycontainerId,containerLadencontainerId,ladenContainerId,ladenPrice,emptyContainerId,emptyPrice,part,currencyId,createdAt")] OceanPriceLine oceanPriceLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(oceanPriceLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", oceanPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", oceanPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", oceanPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice, "oceanPriceId", "oceanPriceId", oceanPriceLine.oceanPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", oceanPriceLine.currencyId);
               
                
            return View(oceanPriceLine);
    }

    // GET: OceanPriceLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var oceanPriceLine = await _context.OceanPriceLine.SingleOrDefaultAsync(m => m.oceanPriceLineId == id);
        if (oceanPriceLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", oceanPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", oceanPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", oceanPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice, "oceanPriceId", "oceanPriceId", oceanPriceLine.oceanPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", oceanPriceLine.currencyId);

            return View(oceanPriceLine);
    }

    // POST: OceanPriceLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("oceanPriceLineId,oceanPriceId,chargeHeadId,containerEmptycontainerId,containerLadencontainerId,ladenContainerId,ladenPrice,emptyContainerId,emptyPrice,part,currencyId,createdAt")] OceanPriceLine oceanPriceLine)
    {
        if (id != oceanPriceLine.oceanPriceLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(oceanPriceLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OceanPriceLineExists(oceanPriceLine.oceanPriceLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", oceanPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", oceanPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", oceanPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", oceanPriceLine.emptyContainerId);
            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice, "oceanPriceId", "oceanPriceId", oceanPriceLine.oceanPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", oceanPriceLine.currencyId);

            return View(oceanPriceLine);
    }

    // GET: OceanPriceLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var oceanPriceLine = await _context.OceanPriceLine
                .Include(p => p.oceanPrice)
                .Include(p => p.oceanPrice)
                .SingleOrDefaultAsync(m => m.oceanPriceLineId == id);
        if (oceanPriceLine == null)
        {
            return NotFound();
        }

        return View(oceanPriceLine);
    }




    // POST: OceanPriceLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var oceanPriceLine = await _context.OceanPriceLine.SingleOrDefaultAsync(m => m.oceanPriceLineId == id);
            _context.OceanPriceLine.Remove(oceanPriceLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool OceanPriceLineExists(string id)
    {
        return _context.OceanPriceLine.Any(e => e.oceanPriceLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class OceanPriceLine
      {
          public const string Controller = "OceanPriceLine";
          public const string Action = "Index";
          public const string Role = "OceanPriceLine";
          public const string Url = "/OceanPriceLine/Index";
          public const string Name = "OceanPriceLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "OceanPriceLine")]
      public bool OceanPriceLineRole { get; set; } = false;
  }
}



