﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "MultipalRateQuoteLine")]
    public class MultipalRateQuoteLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MultipalRateQuoteLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: MultipalRateQuoteLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.MultipalRateQuoteLine.Include(p => p.multipalRateQuote).Include(p => p.chargeHead).Include(p => p.containerLaden).Include(p => p.fromLocation).Include(p => p.toLocation).Include(p => p.currency);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: MultipalRateQuoteLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var multipalRateQuoteLine = await _context.MultipalRateQuoteLine
                .Include(p => p.multipalRateQuote)
                    .SingleOrDefaultAsync(m => m.multipalRateQuoteId == id);
        if (multipalRateQuoteLine == null)
        {
            return NotFound();
        }

        return View(multipalRateQuoteLine);
    }





    // GET: MultipalRateQuoteLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.MultipalRateQuoteLine.SingleOrDefault(m => m.multipalRateQuoteLineId == id);
        var selected = _context.MultipalRateQuote.SingleOrDefault(m => m.multipalRateQuoteId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["multipalRateQuoteId"] = new SelectList(_context.MultipalRateQuote, "multipalRateQuoteId", "multipalRateQuoteId");
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            if (check == null)
        {
            MultipalRateQuoteLine objline = new MultipalRateQuoteLine();
            objline.multipalRateQuote = selected;
            objline.multipalRateQuoteId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: MultipalRateQuoteLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("multipalRateQuoteLineId,multipalRateQuoteId,chargeHeadId,fromlocationId,tolocationId,currencyId,via,mode,ladenContainerId,ladenPrice,trpPortId,createdAt")] MultipalRateQuoteLine multipalRateQuoteLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(multipalRateQuoteLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }

            ViewData["multipalRateQuoteId"] = new SelectList(_context.MultipalRateQuote, "multipalRateQuoteId", "multipalRateQuoteId", multipalRateQuoteLine.multipalRateQuoteId);

            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", multipalRateQuoteLine.chargeHeadId);

            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.tolocationId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.trpPortId);

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", multipalRateQuoteLine.ladenContainerId);

            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", multipalRateQuoteLine.currencyId);
               
                
            return View(multipalRateQuoteLine);
    }

    // GET: MultipalRateQuoteLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var multipalRateQuoteLine = await _context.MultipalRateQuoteLine.SingleOrDefaultAsync(m => m.multipalRateQuoteLineId == id);
        if (multipalRateQuoteLine == null)
        {
            return NotFound();
        }

            ViewData["multipalRateQuoteId"] = new SelectList(_context.MultipalRateQuote, "multipalRateQuoteId", "multipalRateQuoteId", multipalRateQuoteLine.multipalRateQuoteId);

            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", multipalRateQuoteLine.chargeHeadId);

            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.tolocationId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.trpPortId);

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", multipalRateQuoteLine.ladenContainerId);

            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", multipalRateQuoteLine.currencyId);

            return View(multipalRateQuoteLine);
    }

    // POST: MultipalRateQuoteLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("multipalRateQuoteLineId,multipalRateQuoteId,chargeHeadId,fromlocationId,tolocationId,currencyId,via,mode,ladenContainerId,ladenPrice,trpPortId,createdAt")] MultipalRateQuoteLine multipalRateQuoteLine)
    {
        if (id != multipalRateQuoteLine.multipalRateQuoteLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(multipalRateQuoteLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MultipalRateQuoteLineExists(multipalRateQuoteLine.multipalRateQuoteLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }

            ViewData["multipalRateQuoteId"] = new SelectList(_context.MultipalRateQuote, "multipalRateQuoteId", "multipalRateQuoteId", multipalRateQuoteLine.multipalRateQuoteId);

            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", multipalRateQuoteLine.chargeHeadId);

            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.tolocationId);
            ViewData["trpPortId"] = new SelectList(_context.Location, "locationId", "locationName", multipalRateQuoteLine.trpPortId);

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", multipalRateQuoteLine.ladenContainerId);

            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", multipalRateQuoteLine.currencyId);

            return View(multipalRateQuoteLine);
    }

    // GET: MultipalRateQuoteLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var multipalRateQuoteLine = await _context.MultipalRateQuoteLine
                .Include(p => p.multipalRateQuote)
                .Include(p => p.multipalRateQuote)
                .SingleOrDefaultAsync(m => m.multipalRateQuoteLineId == id);
        if (multipalRateQuoteLine == null)
        {
            return NotFound();
        }

        return View(multipalRateQuoteLine);
    }




    // POST: MultipalRateQuoteLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var multipalRateQuoteLine = await _context.MultipalRateQuoteLine.SingleOrDefaultAsync(m => m.multipalRateQuoteLineId == id);
            _context.MultipalRateQuoteLine.Remove(multipalRateQuoteLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool MultipalRateQuoteLineExists(string id)
    {
        return _context.MultipalRateQuoteLine.Any(e => e.multipalRateQuoteLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class MultipalRateQuoteLine
        {
          public const string Controller = "MultipalRateQuoteLine";
          public const string Action = "Index";
          public const string Role = "MultipalRateQuoteLine";
          public const string Url = "/MultipalRateQuoteLine/Index";
          public const string Name = "MultipalRateQuoteLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "MultipalRateQuoteLine")]
      public bool MultipalRateQuoteLineRole { get; set; } = false;
  }
}



