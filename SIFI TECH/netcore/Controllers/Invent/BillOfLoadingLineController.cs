﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "BillOfLoadingLine")]
    public class BillOfLoadingLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BillOfLoadingLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: BillOfLoadingLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BillOfLoadingLine.Include(p => p.billOfLoading).Include(p => p.containerNumber);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: BillOfLoadingLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var billOfLoadingLine = await _context.BillOfLoadingLine
                .Include(p => p.billOfLoading).Include(p => p.containerNumber)
                    .SingleOrDefaultAsync(m => m.billOfLoadingId == id);
        if (billOfLoadingLine == null)
        {
            return NotFound();
        }

        return View(billOfLoadingLine);
    }


    //    // GET: BillOfLoadingLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string billOfLoadingId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (billOfLoadingId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.BillOfLoading.SingleOrDefault(x => x.billOfLoadingId == billOfLoadingId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: BillOfLoadingLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.BillOfLoadingLine.SingleOrDefault(m => m.billOfLoadingLineId == id);
        var selected = _context.BillOfLoading.SingleOrDefault(m => m.billOfLoadingId == masterid);
            var list = _context.ChargeHead.ToList();

            ViewData["billOfLoadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingId");

            var bookingLine = _context.BookingLine.Where(x => x.bookingId == selected.bookingId);
            ViewData["containerNumberId"] = new SelectList(bookingLine.Select(x => x.containerLaden), "containerDetailsId", "containerCode");

            if (check == null)
        {
            BillOfLoadingLine objline = new BillOfLoadingLine();
            objline.billOfLoading = selected;
            objline.billOfLoadingId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: BillOfLoadingLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("billOfLoadingLineId,billOfLoadingId,containerNumberId,cargoItemNo,marksAndNo,woodPack,sealno1,sealno2,sealno3,noOfPackages,kindOfPackages,description,grossCargoWeight,netCargoWeight,unit,measurement,hsCode,shippingBillDate,createdAt")] BillOfLoadingLine billOfLoadingLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(billOfLoadingLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }

            ViewData["billOfLoadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingId", billOfLoadingLine.billOfLoadingId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", billOfLoadingLine.containerNumberId);




            return View(billOfLoadingLine);
    }

    // GET: BillOfLoadingLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var billOfLoadingLine = await _context.BillOfLoadingLine.SingleOrDefaultAsync(m => m.billOfLoadingLineId == id);
        if (billOfLoadingLine == null)
        {
            return NotFound();
        }

            ViewData["billOfLoadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingId", billOfLoadingLine.billOfLoadingId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", billOfLoadingLine.containerNumberId);



            return View(billOfLoadingLine);
    }

    // POST: BillOfLoadingLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("billOfLoadingLineId,billOfLoadingId,containerNumberId,cargoItemNo,marksAndNo,woodPack,sealno1,sealno2,sealno3,noOfPackages,kindOfPackages,description,grossCargoWeight,netCargoWeight,unit,measurement,hsCode,shippingBillDate,createdAt")] BillOfLoadingLine billOfLoadingLine)
    {
        if (id != billOfLoadingLine.billOfLoadingLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(billOfLoadingLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BillOfLoadingLineExists(billOfLoadingLine.billOfLoadingLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }

            ViewData["billOfLoadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingId", billOfLoadingLine.billOfLoadingId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", billOfLoadingLine.containerNumberId);



            return View(billOfLoadingLine);
    }

    // GET: BillOfLoadingLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var billOfLoadingLine = await _context.BillOfLoadingLine
                .Include(p => p.billOfLoading)
                .Include(p => p.billOfLoading)
                .SingleOrDefaultAsync(m => m.billOfLoadingLineId == id);
        if (billOfLoadingLine == null)
        {
            return NotFound();
        }

        return View(billOfLoadingLine);
    }




    // POST: BillOfLoadingLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var billOfLoadingLine = await _context.BillOfLoadingLine.SingleOrDefaultAsync(m => m.billOfLoadingLineId == id);
            _context.BillOfLoadingLine.Remove(billOfLoadingLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool BillOfLoadingLineExists(string id)
    {
        return _context.BillOfLoadingLine.Any(e => e.billOfLoadingLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class BillOfLoadingLine
        {
          public const string Controller = "BillOfLoadingLine";
          public const string Action = "Index";
          public const string Role = "BillOfLoadingLine";
          public const string Url = "/BillOfLoadingLine/Index";
          public const string Name = "BillOfLoadingLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "BillOfLoadingLine")]
      public bool BillOfLoadingLineRole { get; set; } = false;
  }
}



