﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    

    [Authorize(Roles = "ChargeHead")]
    public class ChargeHeadController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ChargeHeadController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ChargeHead
        public async Task<IActionResult> Index()
        {
            return View(await _context.ChargeHead.OrderByDescending(x => x.createdAt).ToListAsync());
        }

        // GET: ChargeHead/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chargeHead = await _context.ChargeHead
                        .SingleOrDefaultAsync(m => m.chargeHeadId == id);
            if (chargeHead == null)
            {
                return NotFound();
            }

            return View(chargeHead);
        }


        // GET: ChargeHead/Create
        public IActionResult Create()
        {
            return View();
        }




        // POST: ChargeHead/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("chargeHeadId,categoryType,part,category,chargeHeadName")] ChargeHead chargeHead)
        {
            if (ModelState.IsValid)
            {
                _context.Add(chargeHead);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(chargeHead);
        }

        // GET: ChargeHead/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chargeHead = await _context.ChargeHead.SingleOrDefaultAsync(m => m.chargeHeadId == id);
            if (chargeHead == null)
            {
                return NotFound();
            }
            return View(chargeHead);
        }

        // POST: ChargeHead/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("chargeHeadId,categoryType,part,category,chargeHeadName")] ChargeHead chargeHead)
        {
            if (id != chargeHead.chargeHeadId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(chargeHead);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChargeHeadExists(chargeHead.chargeHeadId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(chargeHead);
        }

        // GET: ChargeHead/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chargeHead = await _context.ChargeHead
                    .SingleOrDefaultAsync(m => m.chargeHeadId == id);
            if (chargeHead == null)
            {
                return NotFound();
            }

            return View(chargeHead);
        }




        // POST: ChargeHead/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var chargeHead = await _context.ChargeHead.SingleOrDefaultAsync(m => m.chargeHeadId == id);
            try
            {
                _context.ChargeHead.Remove(chargeHead);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(chargeHead);
            }
            
        }

        private bool ChargeHeadExists(string id)
        {
            return _context.ChargeHead.Any(e => e.chargeHeadId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class ChargeHead
        {
            public const string Controller = "ChargeHead";
            public const string Action = "Index";
            public const string Role = "ChargeHead";
            public const string Url = "/ChargeHead/Index";
            public const string Name = "ChargeHead";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "ChargeHead")]
        public bool ChargeHeadRole { get; set; } = false;
    }
}



