﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Import Operation")]
    [Authorize(Roles = "ImportOperation")]
    public class ImportOperationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public ImportOperationController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowImportOperation(string id)
        {
            ImportOperation obj = await _context.ImportOperation
                .Include(x => x.importOperationLine).ThenInclude(x => x.importOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)
                .Include(x => x.billOfLading)


                .SingleOrDefaultAsync(x => x.importOperationId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintImportDO(string id)
        {
            ImportOperation obj = await _context.ImportOperation
         .Include(x => x.importOperationLine).ThenInclude(x => x.importOperation)
         .Include(x => x.importOperationLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.vendorTerminal)
                .Include(x => x.booking).ThenInclude(x => x.bookingLine)
                .Include(x => x.billOfLading)
     
                .SingleOrDefaultAsync(x => x.importOperationId.Equals(id));
            return View(obj);
        }

        public async Task<IActionResult> PrintImportOffOrder(string id)
        {
            ImportOperation obj = await _context.ImportOperation
         .Include(x => x.importOperationLine).ThenInclude(x => x.importOperation)
         .Include(x => x.importOperationLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.vendorTerminal)
                .Include(x => x.booking)
                .Include(x => x.billOfLading)

                .SingleOrDefaultAsync(x => x.importOperationId.Equals(id));
            return View(obj);
        }

        // GET: ImportOperation
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<ImportOperation> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
              applicationDbContext = _context.ImportOperation.OrderByDescending(x => x.createdAt)
                    .Include(x => x.importOperationLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                    .Include(x => x.importOperationLine).ThenInclude(x => x.emptyYard)
                    .Include(x => x.billOfLading)
                    .Include(x => x.branch)
                    .Include(x => x.booking)
                    .Include(x => x.customer)
                    .Include(x => x.booking).ThenInclude(x => x.salesInquiry)
                    .Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.ImportOperation.OrderByDescending(x => x.createdAt)
                    .Include(x => x.importOperationLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                    .Include(x => x.importOperationLine).ThenInclude(x => x.emptyYard)
                    .Include(x => x.billOfLading)
                    .Include(x => x.branch)
                    .Include(x => x.booking)
                    .Include(x => x.customer)
                    .Include(x => x.booking).ThenInclude(x => x.salesInquiry);
            }


                return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> LineIndex()
        {
            var applicationDbContext = _context.ImportOperationLine.Include(p => p.importOperation).Include(p => p.containerNumber).Include(p => p.emptyYard).Include(p => p.importOperation.booking);

            return View(await applicationDbContext.ToListAsync());
        }


        // GET: ImportOperation/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var importOperation = await _context.ImportOperation
           .Include(x => x.importOperationLine).ThenInclude(x => x.importOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)
                .Include(x => x.billOfLading)
                .Include(x => x.booking).ThenInclude(x => x.salesInquiry)
                .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                .Include(x => x.booking).ThenInclude(x => x.toLocation)

                        .SingleOrDefaultAsync(m => m.importOperationId == id);
            if (importOperation == null)
            {
                return NotFound();
            }

            //importOperation.totalOrderAmount = importOperation.importOperation.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(importOperation);
            await _context.SaveChangesAsync();

            return View(importOperation);
        }


        // GET: ImportOperation/Create
        public IActionResult Create()
        {
            ImportOperation importOperation = new ImportOperation();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc");


            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["vendorTerminalId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");

            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(importOperation);
        }




        // POST: ImportOperation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("importOperationId,importOperationNumber,importOperationDate,branchId,vendorTerminalId,shipper,clearingpartydetails,consignee,customerId,bookingId,bookingDate,billOfLadingId,thirdPartyBLN,billOfLadingDate,originFreeDays,originPortStorage,destinationFreeDays,destinationPortStorage,commodity,category,uan,classes,deStuff,stuffingType,igmNumber,igmDate,freight,pol,pod,vessel,voyage,terminal,lineCode,slotOperator,noOfContainer,type,surveyor,leoDate,etd,eta,totalGrossWeight,netWeight,hsCode,smtpNo,smtpDate,incoterm,remark,createdAt")] ImportOperation importOperation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(importOperation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = importOperation.importOperationId });
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                               .Where(u => u.isActive == true).ToList()
                               .Select(u => new {
                                   customerId = u.customerId,
                                   CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                               }),
                                   "customerId", "CustomerNameLoc");
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["vendorTerminalId"] = new SelectList(_context.Vendor, "vendorId", "vendorName");

            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber");

            return View(importOperation);
        }

        // GET: ImportOperation/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var importOperation = await _context.ImportOperation.Include(x => x.importOperationLine).SingleOrDefaultAsync(m => m.importOperationId == id);
            if (importOperation == null)
            {
                return NotFound();
            }

            //importOperation.totalOrderAmount = importOperation.importOperationLine.Sum(x => x.totalAmount);
            //importOperation.totalDiscountAmount = importOperation.importOperationLine.Sum(x => x.discountAmount);
            _context.Update(importOperation);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                              .Where(u => u.isActive == true).ToList()
                              .Select(u => new {
                                  customerId = u.customerId,
                                  CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                              }),
                                  "customerId", "CustomerNameLoc", importOperation.customerId);
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", importOperation.bookingId);
            ViewData["vendorTerminalId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", importOperation.vendorTerminalId);

            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber", importOperation.billOfLadingId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(importOperation);
        }

        // POST: ImportOperation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("importOperationId,importOperationNumber,importOperationDate,branchId,vendorTerminalId,shipper,clearingpartydetails,consignee,customerId,bookingId,bookingDate,billOfLadingId,thirdPartyBLN,billOfLadingDate,originFreeDays,originPortStorage,destinationFreeDays,destinationPortStorage,commodity,category,uan,classes,deStuff,stuffingType,igmNumber,igmDate,freight,pol,pod,vessel,voyage,terminal,lineCode,slotOperator,noOfContainer,type,surveyor,leoDate,etd,eta,totalGrossWeight,netWeight,hsCode,smtpNo,smtpDate,incoterm,remark,createdAt")] ImportOperation importOperation)
        {
            if (id != importOperation.importOperationId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(importOperation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImportOperationExists(importOperation.importOperationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                           .Where(u => u.isActive == true).ToList()
                           .Select(u => new {
                               customerId = u.customerId,
                               CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                           }),
                               "customerId", "CustomerNameLoc", importOperation.customerId);

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", importOperation.bookingId);
            ViewData["vendorTerminalId"] = new SelectList(_context.Vendor, "vendorId", "vendorName", importOperation.vendorTerminalId);
            ViewData["billOfLadingId"] = new SelectList(_context.BillOfLoading, "billOfLoadingId", "billOfLoadingNumber", importOperation.billOfLadingId);
            return View(importOperation);
        }

        // GET: ImportOperation/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var importOperation = await _context.ImportOperation
                    .Include(x => x.importOperationLine).ThenInclude(x => x.importOperation)
                .Include(x => x.branch)
                .Include(x => x.customer)
                .Include(x => x.booking)
                .Include(x => x.billOfLading)
                    .SingleOrDefaultAsync(m => m.importOperationId == id);
            if (importOperation == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(importOperation);
            await _context.SaveChangesAsync();

            return View(importOperation);
        }




        // POST: ImportOperation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var importOperation = await _context.ImportOperation
                .Include(x => x.importOperationLine)
                .SingleOrDefaultAsync(m => m.importOperationId == id);
            try
            {
                _context.ImportOperationLine.RemoveRange(importOperation.importOperationLine);
                _context.ImportOperation.Remove(importOperation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(importOperation);
            }
            
        }

        private bool ImportOperationExists(string id)
        {
            return _context.ImportOperation.Any(e => e.importOperationId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class ImportOperation
        {
            public const string Controller = "ImportOperation";
            public const string Action = "Index";
            public const string Role = "ImportOperation";
            public const string Url = "/ImportOperation/Index";
            public const string Name = "ImportOperation";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "ImportOperation")]
        public bool ImportOperationRole { get; set; } = false;
    }
}



