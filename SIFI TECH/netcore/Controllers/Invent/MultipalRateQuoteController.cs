﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Multipal Rate Quote")]
    [Authorize(Roles = "MultipalRateQuote")]
    public class MultipalRateQuoteController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public MultipalRateQuoteController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowMultipalRateQuote(string id)
        {
            MultipalRateQuote obj = await _context.MultipalRateQuote
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.multipalRateQuote)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.fromLocation)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.toLocation)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.customer)

                .SingleOrDefaultAsync(x => x.multipalRateQuoteId.Equals(id));
   
            _context.Update(obj);

            return View(obj);
        }


        public async Task<IActionResult> PrintMultipalRateQuote(string id)
        {
            MultipalRateQuote obj = await _context.MultipalRateQuote
          .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.multipalRateQuote)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.fromLocation)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.toLocation)
                .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.customer)

                .SingleOrDefaultAsync(x => x.multipalRateQuoteId.Equals(id));
            return View(obj);
        }

        // GET: MultipalRateQuote
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<MultipalRateQuote> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {

              applicationDbContext = _context.MultipalRateQuote.OrderByDescending(x => x.createdAt)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.fromLocation)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.toLocation)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.customer)
                    .Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.MultipalRateQuote.OrderByDescending(x => x.createdAt)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.fromLocation)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.toLocation)
                    .Include(x => x.multipalRateQuoteLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.customer);
            }

                return View(await applicationDbContext.ToListAsync());
        }

        // GET: MultipalRateQuote/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multipalRateQuote = await _context.MultipalRateQuote
                    .Include(x => x.multipalRateQuoteLine)
                    .Include(x => x.branch)
                    .Include(x => x.customer)
                        .SingleOrDefaultAsync(m => m.multipalRateQuoteId == id);
            if (multipalRateQuote == null)
            {
                return NotFound();
            }


            _context.Update(multipalRateQuote);
            await _context.SaveChangesAsync();

            return View(multipalRateQuote);
        }


        // GET: MultipalRateQuote/Create
        public IActionResult Create()
        {
            MultipalRateQuote multipalRateQuote = new MultipalRateQuote();

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                                 .Where(u => u.isActive == true).ToList()
                                 .Select(u => new {
                                     customerId = u.customerId,
                                     CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                                 }),
                                     "customerId", "CustomerNameLoc");

            return View(multipalRateQuote);
        }




        // POST: MultipalRateQuote/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("multipalRateQuoteId,multipalRateQuoteNumber,branchId,customerId,fromDate,toDate,multipalRateQuoteDate,remark,status,preparedBy,createdAt")] MultipalRateQuote multipalRateQuote)
        {
            if (ModelState.IsValid)
            {
                _context.Add(multipalRateQuote);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = multipalRateQuote.multipalRateQuoteId });
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                           .Where(u => u.isActive == true).ToList()
                           .Select(u => new {
                               customerId = u.customerId,
                               CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                           }),
                               "customerId", "CustomerNameLoc");
            return View(multipalRateQuote);
        }

        // GET: MultipalRateQuote/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multipalRateQuote = await _context.MultipalRateQuote.Include(x => x.multipalRateQuoteLine).SingleOrDefaultAsync(m => m.multipalRateQuoteId == id);
            if (multipalRateQuote == null)
            {
                return NotFound();
            }


            _context.Update(multipalRateQuote);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
          .Where(u => u.isActive == true).ToList()
          .Select(u => new {
              customerId = u.customerId,
              CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
          }),
              "customerId", "CustomerNameLoc", multipalRateQuote.customerId);
            return View(multipalRateQuote);
        }

        // POST: MultipalRateQuote/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("multipalRateQuoteId,multipalRateQuoteNumber,branchId,customerId,fromDate,toDate,multipalRateQuoteDate,remark,status,preparedBy,createdAt")] MultipalRateQuote multipalRateQuote)
        {
            if (id != multipalRateQuote.multipalRateQuoteId)
            {
                return NotFound();
            }
            

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(multipalRateQuote);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MultipalRateQuoteExists(multipalRateQuote.multipalRateQuoteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["customerId"] = new SelectList(_context.Customer
                      .Where(u => u.isActive == true).ToList()
                      .Select(u => new {
                          customerId = u.customerId,
                          CustomerNameLoc = String.Concat(u.customerName + " (" + u.city + ", " + u.country.GetDisplayName() + ")")
                      }),
                          "customerId", "CustomerNameLoc", multipalRateQuote.customerId);

            return View(multipalRateQuote);
        }

        // GET: MultipalRateQuote/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var multipalRateQuote = await _context.MultipalRateQuote
                    .Include(x => x.multipalRateQuoteLine)
                    .Include(p => p.branch)
                    .Include(p => p.customer)
                    .SingleOrDefaultAsync(m => m.multipalRateQuoteId == id);
            if (multipalRateQuote == null)
            {
                return NotFound();
            }


            _context.Update(multipalRateQuote);
            await _context.SaveChangesAsync();

            return View(multipalRateQuote);
        }




        // POST: MultipalRateQuote/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var multipalRateQuote = await _context.MultipalRateQuote
                .Include(x => x.multipalRateQuoteLine)
                .SingleOrDefaultAsync(m => m.multipalRateQuoteId == id);
            try
            {
                _context.MultipalRateQuoteLine.RemoveRange(multipalRateQuote.multipalRateQuoteLine);
                _context.MultipalRateQuote.Remove(multipalRateQuote);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(multipalRateQuote);
            }
            
        }

        private bool MultipalRateQuoteExists(string id)
        {
            return _context.MultipalRateQuote.Any(e => e.multipalRateQuoteId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class MultipalRateQuote
        {
            public const string Controller = "MultipalRateQuote";
            public const string Action = "Index";
            public const string Role = "MultipalRateQuote";
            public const string Url = "/MultipalRateQuote/Index";
            public const string Name = "MultipalRateQuote";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "MultipalRateQuote")]
        public bool MultipalRateQuoteRole { get; set; } = false;
    }
}



