﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "LocalPriceLine")]
    public class LocalPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LocalPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LocalPriceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.LocalPriceLine.Include(p => p.localPrice).Include(p => p.chargeHead).Include(p => p.containerLaden).Include(p => p.containerEmpty);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: LocalPriceLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var localPriceLine = await _context.LocalPriceLine
                .Include(p => p.localPrice)
                    .SingleOrDefaultAsync(m => m.localPriceId == id);
        if (localPriceLine == null)
        {
            return NotFound();
        }

        return View(localPriceLine);
    }


    //    // GET: LocalPriceLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string localPriceId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (localPriceId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.LocalPrice.SingleOrDefault(x => x.localPriceId == localPriceId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: LocalPriceLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.LocalPriceLine.SingleOrDefault(m => m.localPriceLineId == id);
        var selected = _context.LocalPrice.SingleOrDefault(m => m.localPriceId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName");
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            if (check == null)
        {
            LocalPriceLine objline = new LocalPriceLine();
            objline.localPrice = selected;
            objline.localPriceId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: LocalPriceLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("localPriceLineId,localPriceId,chargeHeadId,containerCategory,currencyId,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,createdAt")] LocalPriceLine localPriceLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(localPriceLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", localPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", localPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", localPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceId", localPriceLine.localPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", localPriceLine.currencyId);
               
                
            return View(localPriceLine);
    }

    // GET: LocalPriceLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var localPriceLine = await _context.LocalPriceLine.SingleOrDefaultAsync(m => m.localPriceLineId == id);
        if (localPriceLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", localPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", localPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", localPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceId", localPriceLine.localPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", localPriceLine.currencyId);

            return View(localPriceLine);
    }

    // POST: LocalPriceLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("localPriceLineId,localPriceId,chargeHeadId,containerCategory,currencyId,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,createdAt")] LocalPriceLine localPriceLine)
    {
        if (id != localPriceLine.localPriceLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(localPriceLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalPriceLineExists(localPriceLine.localPriceLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", localPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", localPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", localPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", localPriceLine.emptyContainerId);
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceId", localPriceLine.localPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", localPriceLine.currencyId);

            return View(localPriceLine);
    }

    // GET: LocalPriceLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var localPriceLine = await _context.LocalPriceLine
                .Include(p => p.localPrice)
                .Include(p => p.localPrice)
                .SingleOrDefaultAsync(m => m.localPriceLineId == id);
        if (localPriceLine == null)
        {
            return NotFound();
        }

        return View(localPriceLine);
    }




    // POST: LocalPriceLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var localPriceLine = await _context.LocalPriceLine.SingleOrDefaultAsync(m => m.localPriceLineId == id);
            _context.LocalPriceLine.Remove(localPriceLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool LocalPriceLineExists(string id)
    {
        return _context.LocalPriceLine.Any(e => e.localPriceLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class LocalPriceLine
      {
          public const string Controller = "LocalPriceLine";
          public const string Action = "Index";
          public const string Role = "LocalPriceLine";
          public const string Url = "/LocalPriceLine/Index";
          public const string Name = "LocalPriceLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "LocalPriceLine")]
      public bool LocalPriceLineRole { get; set; } = false;
  }
}



