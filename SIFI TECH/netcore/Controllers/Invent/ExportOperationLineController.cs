﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "ExportOperationLine")]
    public class ExportOperationLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExportOperationLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ExportOperationLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ExportOperationLine.Include(p => p.exportOperation).Include(p => p.containerNumber);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ExportOperationLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var exportOperationLine = await _context.ExportOperationLine
                .Include(p => p.exportOperation).Include(p => p.containerNumber)
                    .SingleOrDefaultAsync(m => m.exportOperationId == id);
        if (exportOperationLine == null)
        {
            return NotFound();
        }

        return View(exportOperationLine);
    }


    //    // GET: ExportOperationLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string exportOperationId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (exportOperationId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.ExportOperation.SingleOrDefault(x => x.exportOperationId == exportOperationId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: ExportOperationLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.ExportOperationLine.SingleOrDefault(m => m.exportOperationLineId == id);
        var selected = _context.ExportOperation.SingleOrDefault(m => m.exportOperationId == masterid);
            var list = _context.ChargeHead.ToList();
     
            ViewData["exportOperationId"] = new SelectList(_context.ExportOperation, "exportOperationId", "exportOperationId");

            var bookingLine= _context.BookingLine.Where(x => x.bookingId == selected.bookingId);
            ViewData["containerNumberId"] = new SelectList(bookingLine.Select(x => x.containerLaden), "containerDetailsId", "containerCode");
            if (check == null)
        {
            ExportOperationLine objline = new ExportOperationLine();
            objline.exportOperation = selected;
            objline.exportOperationId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: ExportOperationLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("exportOperationLineId,exportOperationId,emptyPickupDate,containerNumberId,emptyPickupYard,tareWeight,payloadWeight,actualGrossWeight,form1113,form1113Date,terminalGateInDate,gateInTerminal,onBoardDate,etaDestination,requestDate,updationDate,ssr,ssrRequestDate,ssrUpdationDate,vessel,vesselTerminal,shiftingConformation,createdAt")] ExportOperationLine exportOperationLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(exportOperationLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }

            ViewData["exportOperationId"] = new SelectList(_context.ExportOperation, "exportOperationId", "exportOperationId", exportOperationLine.exportOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", exportOperationLine.containerNumberId);


            return View(exportOperationLine);
    }

    // GET: ExportOperationLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var exportOperationLine = await _context.ExportOperationLine.SingleOrDefaultAsync(m => m.exportOperationLineId == id);
        if (exportOperationLine == null)
        {
            return NotFound();
        }

            ViewData["exportOperationId"] = new SelectList(_context.ExportOperation, "exportOperationId", "exportOperationId", exportOperationLine.exportOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", exportOperationLine.containerNumberId);


            return View(exportOperationLine);
    }

    // POST: ExportOperationLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("exportOperationLineId,exportOperationId,emptyPickupDate,containerNumberId,emptyPickupYard,tareWeight,payloadWeight,actualGrossWeight,form1113,form1113Date,terminalGateInDate,gateInTerminal,onBoardDate,etaDestination,requestDate,updationDate,ssr,ssrRequestDate,ssrUpdationDate,vessel,vesselTerminal,shiftingConformation,createdAt")] ExportOperationLine exportOperationLine)
    {
        if (id != exportOperationLine.exportOperationLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(exportOperationLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExportOperationLineExists(exportOperationLine.exportOperationLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }

            ViewData["exportOperationId"] = new SelectList(_context.ExportOperation, "exportOperationId", "exportOperationId", exportOperationLine.exportOperationId);
            ViewData["containerNumberId"] = new SelectList(_context.ContainerDetails, "containerDetailsId", "containerCode", exportOperationLine.containerNumberId);


            return View(exportOperationLine);
    }

    // GET: ExportOperationLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var exportOperationLine = await _context.ExportOperationLine
                .Include(p => p.exportOperation)
                .SingleOrDefaultAsync(m => m.exportOperationLineId == id);
        if (exportOperationLine == null)
        {
            return NotFound();
        }

        return View(exportOperationLine);
    }




    // POST: ExportOperationLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var exportOperationLine = await _context.ExportOperationLine.SingleOrDefaultAsync(m => m.exportOperationLineId == id);
            _context.ExportOperationLine.Remove(exportOperationLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool ExportOperationLineExists(string id)
    {
        return _context.ExportOperationLine.Any(e => e.exportOperationLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class ExportOperationLine
        {
          public const string Controller = "ExportOperationLine";
          public const string Action = "Index";
          public const string Role = "ExportOperationLine";
          public const string Url = "/ExportOperationLine/Index";
          public const string Name = "ExportOperationLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "ExportOperationLine")]
      public bool ExportOperationLineRole { get; set; } = false;
  }
}



