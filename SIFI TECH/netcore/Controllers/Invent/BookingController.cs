﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Booking")]
    [Authorize(Roles = "Booking")]
    public class BookingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public BookingController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowBooking(string id)
        {
            Booking obj = await _context.Booking
                .Include(x => x.bookingLine).ThenInclude(x => x.booking)
                .Include(x => x.bookingLine).ThenInclude(x => x.containerLaden)
                 .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.branch)

                .SingleOrDefaultAsync(x => x.bookingId.Equals(id));

            _context.Update(obj);

            return View(obj);
        }


        public async Task<IActionResult> PrintBooking(string id)
        {
            Booking obj = await _context.Booking
                .Include(x => x.salesInquiry).ThenInclude(x => x.salesInquiryLine)
                .Include(x => x.salesInquiry).ThenInclude(x => x.customer)
                .Include(x => x.salesInquiry).ThenInclude(x => x.customer)
                .Include(x => x.bookingSlot).ThenInclude(x => x.bookingSlotLine)
                .Include(x => x.bookingLine).ThenInclude(x => x.booking).Include(x => x.fromLocation)
                .Include(x => x.bookingLine).ThenInclude(x => x.booking).Include(x => x.toLocation)
                .Include(x => x.bookingLine).ThenInclude(x => x.containerLaden).ThenInclude(x => x.container)
                .Include(x => x.branch)

                .SingleOrDefaultAsync(x => x.bookingId.Equals(id));
            return View(obj);
        }

        public IActionResult Finish(string id)
        {
            var booking =  _context.Booking
                    .Include(x => x.bookingLine)
                    .Include(x => x.fromLocation)
                    .Include(x => x.toLocation)
                    .Include(p => p.branch)
                        .SingleOrDefault(x => x.bookingId.Equals(id));
            booking.status = Status.BOOKED;
            _context.Update(booking);
            _context.SaveChanges();
            return View("Details", booking);
        }

        // GET: Booking
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<Booking> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {

                applicationDbContext = _context.Booking.OrderByDescending(x => x.createdAt)
                    .Include(x => x.bookingLine)
                    .ThenInclude(x => x.containerLaden)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.branch)
                    .Include(p => p.salesInquiry)
                    .Include(p => p.oceanPrice)
                    .Include(p => p.inlandPrice)
                    .Include(p => p.portPrice)
                    .Include(p => p.localPrice)
                    .Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name) && x.isDeleted == false );
            }
            else //If Super Admin
            {

                applicationDbContext = _context.Booking.OrderByDescending(x => x.createdAt)
                    .Include(x => x.bookingLine)
                    .ThenInclude(x => x.containerLaden)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.branch)
                    .Include(p => p.salesInquiry)
                    .Include(p => p.oceanPrice)
                    .Include(p => p.inlandPrice)
                    .Include(p => p.portPrice)
                    .Include(p => p.localPrice);
            }

                return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> LineIndex()
        {
            var applicationDbContext = _context.BookingLine.Include(p => p.booking).Include(p => p.containerLaden).Include(p => p.emptyYardLocation);

            return View(await applicationDbContext.ToListAsync());
        }
        // GET: Booking/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                    .Include(x => x.bookingLine)
                    .Include(x => x.fromLocation)
                    .Include(x => x.toLocation)
                    .Include(p => p.branch)
                        .SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }


            _context.Update(booking);
            await _context.SaveChangesAsync();

            return View(booking);
        }


        // GET: Booking/Create
        public IActionResult Create()
        {
            Booking booking = new Booking();

            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryNumber");
            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotNumber");

            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice.Where(x => x.status == Status.BOOKED), "oceanPriceId", "oceanPriceNumber");
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceNumber");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceNumber");
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceNumber");

            if (booking.branchId != null && booking.fromlocationId != null && booking.tolocationId != null)
            {
                booking.bookingNumber = GenerateBookingNumber(booking.branchId, booking.fromlocationId, booking.tolocationId);
            }
            return View(booking);
        }

        [HttpGet]
        public string GenerateBookingNumber(string inputBranchId, string inputPOLId, string inputPODId)
        {
            var inputBranch = _context.Branch.SingleOrDefault(x => x.branchId == inputBranchId).branchName;
            var inputPOL = _context.Location.SingleOrDefault(x => x.locationId == inputPOLId).locationCode;
            var inputPOD = _context.Location.SingleOrDefault(x => x.locationId == inputPODId).locationCode;

            var bookingId = _context.Booking.Where(x => x.branchId == inputBranchId).Count() + 1;

            var newBookingId = bookingId.ToString().PadLeft((bookingId.ToString().Length + (4 - bookingId.ToString().Length)), '0');

            var bookingNumber = inputBranch.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/"+ newBookingId;


            //var lastBookingNumber = _context.Booking.Where(x => x.branchId == inputBranchId && x.status == Status.BOOKED).OrderByDescending(x => x.createdAt).Take(1)
            //                 .Select(x => x.bookingNumber)
            //                 .ToList()
            //                 .FirstOrDefault();

            //var bookingNumber = "";

            //if (lastBookingNumber != null)
            //{
            //    var bookingId = Convert.ToInt32(lastBookingNumber.Substring(lastBookingNumber.Length - 5)) + 1;

            //    //var bookingId = _context.Booking.Where(x => x.branchId == inputBranchId).Count() + 1;

            //    var newBookingId = bookingId.ToString().PadLeft((bookingId.ToString().Length + (5 - bookingId.ToString().Length)), '0');

            //    bookingNumber = inputBranch.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/" + newBookingId;

            //}


            return bookingNumber;

        }



        // POST: Booking/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("bookingId,bookingNumber,branchId,oceanPriceId,pricingType,inlandPriceId,portPriceId,localPriceId,emailId,salesPersonName,status,fromlocationId,fromServiceMode,tolocationId,toServiceMode,isEmptyContainer,city,earliestdepatureDate,remark,placeofReceiptId,placeofDeliveryId,stuffingType,commodity,hsCode,originFreeDays,originPortStorageDays,destinationFreeDays,destinationPortStorageDays,salesInquiryId,bookingSlotId,preparedBy,eta,etd,surveyorDetails,noOfContainers,bookingValidity,freightTerms,finalDestination,docCutOff,stuffingPoint,vesselGateInTerminal,createdUserEmail,modifiedUserEmail,createdAt")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                var bookingNumber = GenerateBookingNumber(booking.branchId, booking.fromlocationId, booking.tolocationId);
                booking.bookingNumber = bookingNumber;
                booking.createdUserEmail = await GetUserName();
                _context.Add(booking);
                await _context.SaveChangesAsync();
               
                return RedirectToAction(nameof(Details), new { id = booking.bookingId });
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryNumber");
            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotNumber");

            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice.Where(x => x.status == Status.BOOKED), "oceanPriceId", "oceanPriceNumber");
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceNumber");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceNumber");
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceNumber");
            return View(booking);
        }

        public Task<string> GetUserName()
        {
            var userName = User.Identity.Name;

            return Task.FromResult(userName);
        }
        public Task<string> GetUserNameEdit()
        {
            var userName = User.Identity.Name;

            return Task.FromResult(userName);
        }

        // GET: Booking/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.Include(x => x.bookingLine).SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            booking.modifiedUserEmail = await GetUserNameEdit();

            _context.Update(booking);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", booking.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", booking.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName",booking.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", booking.placeofDeliveryId);
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryNumber", booking.salesInquiryId);
            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotNumber", booking.bookingSlotId);

            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice.Where(x => x.status == Status.BOOKED), "oceanPriceId", "oceanPriceNumber");
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceNumber");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceNumber");
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceNumber");
            return View(booking);
        }

        // POST: Booking/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("bookingId,bookingNumber,branchId,pricingType,oceanPriceId,inlandPriceId,portPriceId,localPriceId,emailId,salesPersonName,status,fromlocationId,fromServiceMode,tolocationId,toServiceMode,isEmptyContainer,city,earliestdepatureDate,remark,placeofReceiptId,placeofDeliveryId,stuffingType,commodity,hsCode,originFreeDays,originPortStorageDays,destinationFreeDays,destinationPortStorageDays,salesInquiryId,bookingSlotId,preparedBy,eta,etd,surveyorDetails,noOfContainers,bookingValidity,freightTerms,finalDestination,docCutOff,stuffingPoint,vesselGateInTerminal,modifiedUserEmail,createdAt")] Booking booking)
        {
            if (id != booking.bookingId)
            {
                return NotFound();
            }


            if (ModelState.IsValid)
            {
                try
                {
                    booking.modifiedUserEmail = await GetUserNameEdit();
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.bookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", booking.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", booking.tolocationId);
            ViewData["placeofReceiptId"] = new SelectList(_context.Location, "locationId", "locationName", booking.placeofReceiptId);
            ViewData["placeofDeliveryId"] = new SelectList(_context.Location, "locationId", "locationName", booking.placeofDeliveryId);
            ViewData["salesInquiryId"] = new SelectList(_context.SalesInquiry, "salesInquiryId", "salesInquiryNumber", booking.salesInquiryId);
            ViewData["bookingSlotId"] = new SelectList(_context.BookingSlot, "bookingSlotId", "bookingSlotNumber", booking.bookingSlotId);

            ViewData["oceanPriceId"] = new SelectList(_context.OceanPrice.Where(x => x.status == Status.BOOKED), "oceanPriceId", "oceanPriceNumber");
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceNumber");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceNumber");
            ViewData["localPriceId"] = new SelectList(_context.LocalPrice, "localPriceId", "localPriceNumber");
            return View(booking);
        }

        // GET: Booking/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                    .Include(x => x.bookingLine)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .SingleOrDefaultAsync(m => m.bookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            //booking.modifiedUserEmail = await GetUserName();
            _context.Update(booking);
            await _context.SaveChangesAsync();

            return View(booking);
        }




        //// POST: Booking/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(string id)
        //{
        //    var booking = await _context.Booking
        //        .Include(x => x.bookingLine)
        //            .Include(p => p.branch)
        //        .SingleOrDefaultAsync(m => m.bookingId == id);
        //    try
        //    {
        //        _context.BookingLine.RemoveRange(booking.bookingLine);
        //        _context.Booking.Remove(booking);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch (Exception ex)
        //    {

        //        ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
        //        return View(booking);
        //    }

        //}



        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var booking = await _context.Booking
                .Include(x => x.bookingLine)
                    .Include(p => p.branch)
                .SingleOrDefaultAsync(m => m.bookingId == id);

            var bookingLine = _context.BookingLine
                .Include(x => x.booking)
                .Where(m => m.bookingId == booking.bookingId).ToList();
            try
            {
                foreach (var item in bookingLine)
                {
                    var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(x => x.containerDetailsId == item.ladenContainerId);
                    containerDetails.containerStatus = ContainerStatus.empty;
                    _context.ContainerDetails.Update(containerDetails);
                }
                

                _context.BookingLine.RemoveRange(booking.bookingLine);
                booking.isDeleted = true;
                booking.modifiedUserEmail = await GetUserName();
                _context.Booking.Update(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(booking);
            }

        }



        private bool BookingExists(string id)
        {
            return _context.Booking.Any(e => e.bookingId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class Booking
        {
            public const string Controller = "Booking";
            public const string Action = "Index";
            public const string Role = "Booking";
            public const string Url = "/Booking/Index";
            public const string Name = "Booking";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Booking")]
        public bool BookingRole { get; set; } = false;
    }
}



