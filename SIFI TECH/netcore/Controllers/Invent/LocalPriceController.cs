﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Local Price")]
    [Authorize(Roles = "LocalPrice")]
    public class LocalPriceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public LocalPriceController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowLocalPrice(string id)
        {
            LocalPrice obj = await _context.LocalPrice
                .Include(x => x.vendor)
                .Include(x => x.localPriceLine).ThenInclude(x => x.localPrice)
                .Include(x => x.localPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.localPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.localPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.localPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.location)
                //.Include(x => x.toLocation)

                .SingleOrDefaultAsync(x => x.localPriceId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintLocalPrice(string id)
        {
            LocalPrice obj = await _context.LocalPrice
           .Include(x => x.vendor)
                .Include(x => x.localPriceLine).ThenInclude(x => x.localPrice)
                .Include(x => x.localPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.localPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.localPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.localPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                .Include(x => x.location)
                //.Include(x => x.toLocation)

                .SingleOrDefaultAsync(x => x.localPriceId.Equals(id));
            return View(obj);
        }

        // GET: LocalPrice
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<LocalPrice> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {

                applicationDbContext = _context.LocalPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.vendor)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.localPrice)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.location).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {

                applicationDbContext = _context.LocalPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.vendor)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.localPrice)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(x => x.localPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.branch)
                    .Include(p => p.location);
            }

                return View(await applicationDbContext.ToListAsync());
        }

        // GET: LocalPrice/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var localPrice = await _context.LocalPrice
                    .Include(x => x.localPriceLine)
                    .Include(p => p.vendor)
                    .Include(x => x.branch)
                    .Include(p => p.location)
                        //.Include(p => p.toLocation)
                        .SingleOrDefaultAsync(m => m.localPriceId == id);
            if (localPrice == null)
            {
                return NotFound();
            }

            //localPrice.totalOrderAmount = localPrice.localPrice.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(localPrice);
            await _context.SaveChangesAsync();

            return View(localPrice);
        }


        // GET: LocalPrice/Create
        public IActionResult Create()
        {
            LocalPrice localPrice = new LocalPrice();
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(localPrice);
        }




        // POST: LocalPrice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("localPriceId,localPriceNumber,branchId,locationId,vendorId,fromDate,toDate,localPriceDate,remark,preparedBy,eximType,status,createdAt")] LocalPrice localPrice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(localPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = localPrice.localPriceId });
            }
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            return View(localPrice);
        }

        // GET: LocalPrice/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var localPrice = await _context.LocalPrice.Include(x => x.localPriceLine).SingleOrDefaultAsync(m => m.localPriceId == id);
            if (localPrice == null)
            {
                return NotFound();
            }

            //localPrice.totalOrderAmount = localPrice.localPriceLine.Sum(x => x.totalAmount);
            //localPrice.totalDiscountAmount = localPrice.localPriceLine.Sum(x => x.discountAmount);
            _context.Update(localPrice);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                      .Where(u => u.isActive == true).ToList()
                                      .Select(u => new {
                                          vendorId = u.vendorId,
                                          VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                      }),
                                          "vendorId", "VendorNameLoc", localPrice.vendorId);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName", localPrice.locationId);
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", localPrice.tolocationId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(localPrice);
        }

        // POST: LocalPrice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("localPriceId,localPriceNumber,branchId,locationId,vendorId,fromDate,toDate,localPriceDate,remark,preparedBy,eximType,status,createdAt")] LocalPrice localPrice)
        {
            if (id != localPrice.localPriceId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(localPrice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LocalPriceExists(localPrice.localPriceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                         .Where(u => u.isActive == true).ToList()
                                         .Select(u => new {
                                             vendorId = u.vendorId,
                                             VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                         }),
                                             "vendorId", "VendorNameLoc", localPrice.vendorId);
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName", localPrice.locationId);
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", localPrice.tolocationId);
            return View(localPrice);
        }

        // GET: LocalPrice/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var localPrice = await _context.LocalPrice
                    .Include(x => x.localPriceLine)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    .Include(p => p.location)
                    //.Include(p => p.toLocation)
                    .SingleOrDefaultAsync(m => m.localPriceId == id);
            if (localPrice == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(localPrice);
            await _context.SaveChangesAsync();

            return View(localPrice);
        }




        // POST: LocalPrice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var localPrice = await _context.LocalPrice
                .Include(x => x.localPriceLine)
                .SingleOrDefaultAsync(m => m.localPriceId == id);
            try
            {
                _context.LocalPriceLine.RemoveRange(localPrice.localPriceLine);
                _context.LocalPrice.Remove(localPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(localPrice);
            }
            
        }

        private bool LocalPriceExists(string id)
        {
            return _context.LocalPrice.Any(e => e.localPriceId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class LocalPrice
        {
            public const string Controller = "LocalPrice";
            public const string Action = "Index";
            public const string Role = "LocalPrice";
            public const string Url = "/LocalPrice/Index";
            public const string Name = "LocalPrice";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "LocalPrice")]
        public bool LocalPriceRole { get; set; } = false;
    }
}



