﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "InlandPriceLine")]
    public class InlandPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InlandPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: InlandPriceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.InlandPriceLine.Include(p => p.inlandPrice).Include(p => p.chargeHead).Include(p => p.containerLaden).Include(p => p.containerEmpty);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: InlandPriceLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var inlandPriceLine = await _context.InlandPriceLine
                .Include(p => p.inlandPrice)
                    .SingleOrDefaultAsync(m => m.inlandPriceId == id);
        if (inlandPriceLine == null)
        {
            return NotFound();
        }

        return View(inlandPriceLine);
    }


    //    // GET: InlandPriceLine/GetINRPrice/5
    //    [HttpGet]
    //public decimal? GetINRPrice(string productId, string inlandPriceId)
    //{
    //    if (productId == null)
    //    {
    //        return null;
    //    }
    //    if (inlandPriceId == null)
    //    {
    //        return null;
    //    }

    //    var productPriceinUSD = _context.Product.SingleOrDefault(x => x.productId == productId).priceInUSD;
    //    var usdRate = _context.InlandPrice.SingleOrDefault(x => x.inlandPriceId == inlandPriceId).usdRate;
    //    var INRTotal = productPriceinUSD * usdRate;

    //    if (INRTotal == null)
    //    {
    //        return null;
    //    }
    //    return INRTotal;
    //}


    // GET: InlandPriceLine/Create
    public IActionResult Create(string masterid, string id)
    {
        var check = _context.InlandPriceLine.SingleOrDefault(m => m.inlandPriceLineId == id);
        var selected = _context.InlandPrice.SingleOrDefault(m => m.inlandPriceId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName");
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName");
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            if (check == null)
        {
            InlandPriceLine objline = new InlandPriceLine();
            objline.inlandPrice = selected;
            objline.inlandPriceId = masterid;
            return View(objline);
        }
        else
        {
            return View(check);
        }
    }




    // POST: InlandPriceLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("inlandPriceLineId,inlandPriceId,chargeHeadId,wtFrom,wtTo,category,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,currencyId,createdAt")] InlandPriceLine inlandPriceLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(inlandPriceLine);
            await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", inlandPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", inlandPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", inlandPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceId", inlandPriceLine.inlandPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", inlandPriceLine.currencyId);
               
                
            return View(inlandPriceLine);
    }

    // GET: InlandPriceLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var inlandPriceLine = await _context.InlandPriceLine.SingleOrDefaultAsync(m => m.inlandPriceLineId == id);
        if (inlandPriceLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", inlandPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", inlandPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", inlandPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceId", inlandPriceLine.inlandPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", inlandPriceLine.currencyId);

            return View(inlandPriceLine);
    }

    // POST: InlandPriceLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("inlandPriceLineId,inlandPriceId,chargeHeadId,wtFrom,wtTo,category,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,currencyId,createdAt")] InlandPriceLine inlandPriceLine)
    {
        if (id != inlandPriceLine.inlandPriceLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(inlandPriceLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InlandPriceLineExists(inlandPriceLine.inlandPriceLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName", inlandPriceLine.chargeHeadId);
            //ViewData["containerEmptycontainerId"] = new SelectList(_context.Container, "containerEmptycontainerId", "containerName", inlandPriceLine.ladenContainerId);
            //ViewData["containerLadencontainerId"] = new SelectList(_context.Container, "containerLadencontainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "ladenContainerId", "containerName", inlandPriceLine.ladenContainerId);
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "emptyContainerId", "containerName", inlandPriceLine.emptyContainerId);
            ViewData["inlandPriceId"] = new SelectList(_context.InlandPrice, "inlandPriceId", "inlandPriceId", inlandPriceLine.inlandPriceId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode", inlandPriceLine.currencyId);

            return View(inlandPriceLine);
    }

    // GET: InlandPriceLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var inlandPriceLine = await _context.InlandPriceLine
                .Include(p => p.inlandPrice)
                .Include(p => p.inlandPrice)
                .SingleOrDefaultAsync(m => m.inlandPriceLineId == id);
        if (inlandPriceLine == null)
        {
            return NotFound();
        }

        return View(inlandPriceLine);
    }




    // POST: InlandPriceLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var inlandPriceLine = await _context.InlandPriceLine.SingleOrDefaultAsync(m => m.inlandPriceLineId == id);
            _context.InlandPriceLine.Remove(inlandPriceLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool InlandPriceLineExists(string id)
    {
        return _context.InlandPriceLine.Any(e => e.inlandPriceLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class InlandPriceLine
      {
          public const string Controller = "InlandPriceLine";
          public const string Action = "Index";
          public const string Role = "InlandPriceLine";
          public const string Url = "/InlandPriceLine/Index";
          public const string Name = "InlandPriceLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "InlandPriceLine")]
      public bool InlandPriceLineRole { get; set; } = false;
  }
}



