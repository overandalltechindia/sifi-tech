﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Bill Of Loading")]
    [Authorize(Roles = "BillOfLoading")]
    public class BillOfLoadingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public BillOfLoadingController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        public async Task<IActionResult> ShowBillOfLoading(string id)
        {
            BillOfLoading obj = await _context.BillOfLoading
                .Include(x => x.booking)
                .Include(x => x.billOfLoadingLine).ThenInclude(x => x.billOfLoading)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.branch)

                .SingleOrDefaultAsync(x => x.billOfLoadingId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintBillOfLading(string id)
        {
            BillOfLoading obj = await _context.BillOfLoading
              .Include(x => x.booking)
                .Include(x => x.billOfLoadingLine).ThenInclude(x => x.billOfLoading)
                .Include(x => x.billOfLoadingLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                .Include(x => x.fromLocation)
                .Include(x => x.toLocation)
                .Include(x => x.branch)

                .SingleOrDefaultAsync(x => x.billOfLoadingId.Equals(id));
            return View(obj);
        }

        // GET: BillOfLoading
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<BillOfLoading> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {


                applicationDbContext = _context.BillOfLoading.OrderByDescending(x => x.createdAt)
                    .Include(x => x.billOfLoadingLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                    .Include(p => p.booking)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.branch).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
                applicationDbContext = _context.BillOfLoading.OrderByDescending(x => x.createdAt)
                    .Include(x => x.billOfLoadingLine).ThenInclude(x => x.containerNumber).ThenInclude(x => x.container)
                    .Include(p => p.booking)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.branch);
            }

                return View(await applicationDbContext.ToListAsync());
        }

        // GET: BillOfLoading/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billOfLoading = await _context.BillOfLoading
                    .Include(x => x.billOfLoadingLine)
                    .Include(p => p.booking)
                .Include(x => x.booking).ThenInclude(x => x.salesInquiry)
                .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                .Include(x => x.booking).ThenInclude(x => x.toLocation)
                    .Include(x => x.branch)

                        .SingleOrDefaultAsync(m => m.billOfLoadingId == id);
            if (billOfLoading == null)
            {
                return NotFound();
            }

            //billOfLoading.totalOrderAmount = billOfLoading.billOfLoading.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(billOfLoading);
            await _context.SaveChangesAsync();

            return View(billOfLoading);
        }


        // GET: BillOfLoading/Create
        public IActionResult Create()
        {
            BillOfLoading billOfLoading = new BillOfLoading();
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(billOfLoading);
        }




        // POST: BillOfLoading/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("billOfLoadingId,billOfLoadingNumber,branchId,brokerName,documentType,shipper,bookingId,consignee,destinationAgentReference,placeOfReceipt,placeOfDelivery,vesselName,voyageNumber,fromlocationId,tolocationId,movementType,totalNumberContainers,totalNumberCargoes,shipperTaxId,consigneeTaxId,notifyTaxId,freightPayableAt,invoiceReference,setCharges,originHaulage,originPort,seaFreaightAdditionals,destinationHaulage,destinationPort,bLType,originalBl,coppies,remark,notifyToparty,createdAt")] BillOfLoading billOfLoading)
        {
            if (ModelState.IsValid)
            {
                _context.Add(billOfLoading);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = billOfLoading.billOfLoadingId });
            }
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            return View(billOfLoading);
        }

        // GET: BillOfLoading/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billOfLoading = await _context.BillOfLoading.Include(x => x.billOfLoadingLine).SingleOrDefaultAsync(m => m.billOfLoadingId == id);
            if (billOfLoading == null)
            {
                return NotFound();
            }

            //billOfLoading.totalOrderAmount = billOfLoading.billOfLoadingLine.Sum(x => x.totalAmount);
            //billOfLoading.totalDiscountAmount = billOfLoading.billOfLoadingLine.Sum(x => x.discountAmount);
            _context.Update(billOfLoading);
            await _context.SaveChangesAsync();
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", billOfLoading.bookingId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", billOfLoading.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", billOfLoading.tolocationId);
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(billOfLoading);
        }

        // POST: BillOfLoading/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("billOfLoadingId,billOfLoadingNumber,branchId,brokerName,documentType,shipper,bookingId,consignee,destinationAgentReference,placeOfReceipt,placeOfDelivery,vesselName,voyageNumber,fromlocationId,tolocationId,movementType,totalNumberContainers,totalNumberCargoes,shipperTaxId,consigneeTaxId,notifyTaxId,freightPayableAt,invoiceReference,setCharges,originHaulage,originPort,seaFreaightAdditionals,destinationHaulage,destinationPort,bLType,originalBl,coppies,remark,notifyToparty,createdAt")] BillOfLoading billOfLoading)
        {
            if (id != billOfLoading.billOfLoadingId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(billOfLoading);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BillOfLoadingExists(billOfLoading.billOfLoadingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", billOfLoading.bookingId);
            ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", billOfLoading.fromlocationId);
            ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", billOfLoading.tolocationId);
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            return View(billOfLoading);
        }

        // GET: BillOfLoading/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billOfLoading = await _context.BillOfLoading
                    .Include(x => x.billOfLoadingLine)
                    .Include(p => p.booking)
                    .Include(p => p.fromLocation)
                    .Include(p => p.toLocation)
                    .Include(p => p.branch)

                    .SingleOrDefaultAsync(m => m.billOfLoadingId == id);
            if (billOfLoading == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(billOfLoading);
            await _context.SaveChangesAsync();

            return View(billOfLoading);
        }




        // POST: BillOfLoading/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var billOfLoading = await _context.BillOfLoading
                .Include(x => x.billOfLoadingLine)
                .SingleOrDefaultAsync(m => m.billOfLoadingId == id);
            try
            {
                _context.BillOfLoadingLine.RemoveRange(billOfLoading.billOfLoadingLine);
                _context.BillOfLoading.Remove(billOfLoading);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(billOfLoading);
            }
            
        }

        private bool BillOfLoadingExists(string id)
        {
            return _context.BillOfLoading.Any(e => e.billOfLoadingId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class BillOfLoading
        {
            public const string Controller = "BillOfLoading";
            public const string Action = "Index";
            public const string Role = "BillOfLoading";
            public const string Url = "/BillOfLoading/Index";
            public const string Name = "BillOfLoading";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "BillOfLoading")]
        public bool BillOfLoadingRole { get; set; } = false;
    }
}



