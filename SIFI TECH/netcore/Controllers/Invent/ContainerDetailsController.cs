﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "ContainerDetails")]
    public class ContainerDetailsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ContainerDetailsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ContainerDetails
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ContainerDetails.OrderByDescending(x => x.createdAt).Include(p => p.container).Include(p => p.location).Include(p => p.emptyyardLocation).Include(p => p.branch);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ContainerDetails/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var containerDetails = await _context.ContainerDetails
                .Include(x => x.container)
                .Include(x => x.branch)
                .Include(x => x.location)
                        .SingleOrDefaultAsync(m => m.containerDetailsId == id);
            if (containerDetails == null)
            {
                return NotFound();
            }

            return View(containerDetails);
        }


        // GET: ContainerDetails/Create
        public IActionResult Create()
        {

            ContainerDetails containerDetails = new ContainerDetails();

            ViewData["containerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName");
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["emptyyardLocationId"] = new SelectList(_context.Location, "locationId", "locationName");

            return View(containerDetails);
        }




        // POST: ContainerDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("containerDetailsId,containerCode,containerId,branchId,locationId,description,barcode,serialNumber,ownership,containerStatus,containerManufactureDate,dateofPurchase,purchasedLeasingParty,purchasedLeasingLocation,containerleasingDate,noofLeasedays,tareweight,payloadcapacity,emptyyardLocationId,availableOn,noOfIdleDays,isActive,createdAt")] ContainerDetails containerDetails)
        {
            if (ModelState.IsValid)
            {
                if (!ContainerCodeExists(containerDetails.containerCode))
                {
                    _context.Add(containerDetails);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewData["StatusMessage"] = "Container Code Already Exists! Please add a New Container Code!";
                }
                
            }
            return View(containerDetails);
        }

        // GET: ContainerDetails/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(m => m.containerDetailsId == id);
            if (containerDetails == null)
            {
                return NotFound();
            }

            _context.Update(containerDetails);
            await _context.SaveChangesAsync();

            ViewData["containerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName");
            ViewData["locationId"] = new SelectList(_context.Location, "locationId", "locationName");
            ViewData["emptyyardLocationId"] = new SelectList(_context.Location, "locationId", "locationName");

            return View(containerDetails);
        }

        // POST: ContainerDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("containerDetailsId,containerCode,containerId,branchId,locationId,description,barcode,serialNumber,ownership,containerStatus,containerManufactureDate,dateofPurchase,purchasedLeasingParty,purchasedLeasingLocation,containerleasingDate,noofLeasedays,tareweight,payloadcapacity,emptyyardLocationId,availableOn,noOfIdleDays,isActive,createdAt")] ContainerDetails containerDetails)
        {
            if (id != containerDetails.containerDetailsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(containerDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContainerDetailsExists(containerDetails.containerDetailsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(containerDetails);
        }

        // GET: ContainerDetails/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var containerDetails = await _context.ContainerDetails
                    .SingleOrDefaultAsync(m => m.containerDetailsId == id);
            if (containerDetails == null)
            {
                return NotFound();
            }

            return View(containerDetails);
        }




        // POST: ContainerDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var containerDetails = await _context.ContainerDetails.SingleOrDefaultAsync(m => m.containerDetailsId == id);
            try
            {
                _context.ContainerDetails.Remove(containerDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(containerDetails);
            }
            
        }

        private bool ContainerDetailsExists(string id)
        {
            return _context.ContainerDetails.Any(e => e.containerDetailsId == id);
        }

        private bool ContainerCodeExists(string containerCode)
        {
            return _context.ContainerDetails.Any(e => e.containerCode == containerCode);
        }


    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class ContainerDetails
        {
            public const string Controller = "ContainerDetails";
            public const string Action = "Index";
            public const string Role = "ContainerDetails";
            public const string Url = "/ContainerDetails/Index";
            public const string Name = "ContainerDetails";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "Container Details")]
        public bool ContainerDetailsRole { get; set; } = false;
    }
}



