﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{
    [Authorize(Roles = "PortPriceLine")]
    public class PortPriceLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PortPriceLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PortPriceLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PortPriceLine.Include(p => p.portPrice).Include(p => p.chargeHead).Include(p => p.containerLaden).Include(p => p.containerEmpty);
            return View(await applicationDbContext.ToListAsync());

        }

        // GET: PortPriceLine/Details/5
        public async Task<IActionResult> Details(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var PortPriceLine = await _context.PortPriceLine
                .Include(v => v.portPrice)
                    .SingleOrDefaultAsync(m => m.portPriceLineId == id);
        if (PortPriceLine == null)
        {
            return NotFound();
        }

        return View(PortPriceLine);
    }


    // GET: PortPriceLine/Create
    public IActionResult Create(string masterid, string id)
    {
            var check = _context.PortPriceLine.SingleOrDefault(m => m.portPriceLineId == id);
            var selected = _context.PortPrice.SingleOrDefault(m => m.portPriceId == masterid);
            var list = _context.ChargeHead.ToList();
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");
       
            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            if (check == null)
            {
                PortPriceLine objline = new PortPriceLine();
                objline.portPrice = selected;
                objline.portPriceId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




    // POST: PortPriceLine/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("portPriceLineId,portPriceId,chargeHeadId,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,containerCategory,currencyId,createdAt")] PortPriceLine portPriceLine)
    {
        if (ModelState.IsValid)
        {
            _context.Add(portPriceLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");

            return View(portPriceLine);
    }

    // GET: PortPriceLine/Edit/5
    public async Task<IActionResult> Edit(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var portPriceLine = await _context.PortPriceLine.SingleOrDefaultAsync(m => m.portPriceLineId == id);
        if (portPriceLine == null)
        {
            return NotFound();
        }
            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            return View(portPriceLine);
    }

    // POST: PortPriceLine/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(string id, [Bind("portPriceLineId,portPriceId,chargeHeadId,laden,ladenContainerId,ladenPrice,empty,emptyContainerId,emptyPrice,part,containerCategory,currencyId,createdAt")] PortPriceLine portPriceLine)
    {
        if (id != portPriceLine.portPriceLineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(portPriceLine);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PortPriceLineExists(portPriceLine.portPriceLineId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        return RedirectToAction(nameof(Index));
        }

            ViewData["chargeHeadId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");

            ViewData["ladenContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["emptyContainerId"] = new SelectList(_context.Container, "containerId", "containerName");
            ViewData["portPriceId"] = new SelectList(_context.PortPrice, "portPriceId", "portPriceId");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyCode");
            return View(portPriceLine);
    }

    // GET: PortPriceLine/Delete/5
    public async Task<IActionResult> Delete(string id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var portPriceLine = await _context.PortPriceLine
                .Include(v => v.portPrice)
                .SingleOrDefaultAsync(m => m.portPriceLineId == id);
        if (portPriceLine == null)
        {
            return NotFound();
        }

        return View(portPriceLine);
    }




    // POST: PortPriceLine/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(string id)
    {
        var portPriceLine = await _context.PortPriceLine.SingleOrDefaultAsync(m => m.portPriceLineId == id);
            _context.PortPriceLine.Remove(portPriceLine);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool PortPriceLineExists(string id)
    {
        return _context.PortPriceLine.Any(e => e.portPriceLineId == id);
    }

  }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class PortPriceLine
      {
          public const string Controller = "PortPriceLine";
          public const string Action = "Index";
          public const string Role = "PortPriceLine";
          public const string Url = "/PortPriceLine/Index";
          public const string Name = "PortPriceLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "PortPriceLine")]
      public bool PortPriceLineRole { get; set; } = false;
  }
}



