﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;

namespace netcore.Controllers.Invent
{

    [Authorize(Roles = "InvoicesLine")]
    public class InvoicesLineController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InvoicesLineController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: InvoicesLine
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.InvoicesLine.Include(p => p.invoices);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: InvoicesLine/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoicesLine = await _context.InvoicesLine
                    .Include(p => p.invoices)
                    .Include(p => p.currency)
                    .Include(p => p.invoices.booking)
                        .SingleOrDefaultAsync(m => m.invoicesId == id);
            if (invoicesLine == null)
            {
                return NotFound();
            }

            return View(invoicesLine);
        }


        // GET: InvoicesLine/GetINRPrice/5
        //[HttpGet]
        public decimal? GetAmount(string chargeHeadId, string invoiceId)
        {
            if (chargeHeadId == null)
            {
                return null;
            }
            if (invoiceId == null)
            {
                return null;
            }
            var selected = _context.Invoices.Include(x => x.booking).Include(x => x.booking.salesInquiry).Include(x => x.booking.bookingSlot).SingleOrDefault(m => m.invoicesId == invoiceId);

            var incomeChargeHeadAmount = _context.SalesInquiryLine.Include(x => x.salesInquiry).Include(x => x.chargeHead).FirstOrDefault(x => x.salesInquiry.salesInquiryId == selected.booking.salesInquiryId && x.chargeHeadId == chargeHeadId).totalPrice;

            if (incomeChargeHeadAmount == 0)
            {
                return null;
            }
            return incomeChargeHeadAmount;
        }


        // GET: InvoicesLine/Create
        public async Task<IActionResult> Create(string masterid, string id)
        {
            var check = await _context.InvoicesLine.SingleOrDefaultAsync(m => m.invoicesLineId == id);

            var selected = await _context.Invoices.Include(x => x.booking).Include(x => x.booking.salesInquiry).Include(x => x.booking.bookingSlot).SingleOrDefaultAsync(m => m.invoicesId == masterid);

            ViewData["invoicesId"] = new SelectList(_context.Invoices, "invoicesId", "invoicesId");
            ViewData["costId"] = new SelectList(_context.ChargeHead, "chargeHeadId", "chargeHeadName");

            var existingChargeHeads = await _context.InvoicesLine.Where(x => x.invoicesId == masterid).Select(y => y.income).ToListAsync();

            //Compare
            HashSet<string> diffids = new HashSet<string>(existingChargeHeads.Select(s => s.chargeHeadId));

            var incomeChargeHeads = await _context.SalesInquiryLine.Include(x => x.salesInquiry).Include(x => x.chargeHead).Where(x => x.salesInquiry.salesInquiryId == selected.booking.salesInquiryId && !diffids.Contains(x.chargeHeadId)).Select(x => x.chargeHead).ToListAsync();

            ViewData["incomeId"] = new SelectList(incomeChargeHeads, "chargeHeadId", "chargeHeadName");
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyName");
            if (check == null)
            {
                InvoicesLine objline = new InvoicesLine();
                objline.invoices = selected;
                objline.invoicesId = masterid;
                return View(objline);
            }
            else
            {
                return View(check);
            }
        }




        // POST: InvoicesLine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("invoicesLineId,invoicesId,costId,amount,incomeId,type,cgst,cgstPercent,sgst,sgstPercent,igst,igstPercent,incamount,margin,sacCode,createdAt")] InvoicesLine invoicesLine)
        {
            if (ModelState.IsValid)
            {

                _context.Add(invoicesLine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["invoicesId"] = new SelectList(_context.Invoices, "invoicesId", "invoicesId", invoicesLine.invoicesId);
            ViewData["costId"] = new SelectList(_context.ChargeHead, "costId", "chargeHeadName", invoicesLine.costId);
            ViewData["invoicesId"] = new SelectList(_context.ChargeHead, "incomeId", "chargeHeadName", invoicesLine.incomeId);
            ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyName");



            return View(invoicesLine);
        }

        // GET: InvoicesLine/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoicesLine = await _context.InvoicesLine.SingleOrDefaultAsync(m => m.invoicesLineId == id);
            if (invoicesLine == null)
            {
                return NotFound();
            }

                ViewData["invoicesId"] = new SelectList(_context.Invoices, "invoicesId", "invoicesId", invoicesLine.invoicesId);
                ViewData["costId"] = new SelectList(_context.ChargeHead, "costId", "chargeHeadName", invoicesLine.costId);
                ViewData["invoicesId"] = new SelectList(_context.ChargeHead, "incomeId", "chargeHeadName", invoicesLine.incomeId);
                ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyName", invoicesLine.currencyId);

                return View(invoicesLine);
        }

        // POST: InvoicesLine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("invoicesLineId,invoicesId,costId,amount,incomeId,type,cgst,cgstPercent,sgst,sgstPercent,igst,igstPercent,incamount,margin,sacCode,createdAt")] InvoicesLine invoicesLine)
        {
            if (id != invoicesLine.invoicesLineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(invoicesLine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InvoicesLineExists(invoicesLine.invoicesLineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            return RedirectToAction(nameof(Index));
            }

                ViewData["invoicesId"] = new SelectList(_context.Invoices, "invoicesId", "invoicesId", invoicesLine.invoicesId);
                ViewData["costId"] = new SelectList(_context.ChargeHead, "costId", "chargeHeadName", invoicesLine.costId);
                ViewData["invoicesId"] = new SelectList(_context.ChargeHead, "incomeId", "chargeHeadName", invoicesLine.incomeId);
                ViewData["currencyId"] = new SelectList(_context.Currency, "currencyId", "currencyName", invoicesLine.currencyId);

                return View(invoicesLine);
        }

        // GET: InvoicesLine/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoicesLine = await _context.InvoicesLine
                    .Include(p => p.invoices)
                    .Include(p => p.invoices)
                    .SingleOrDefaultAsync(m => m.invoicesLineId == id);
            if (invoicesLine == null)
            {
                return NotFound();
            }

            return View(invoicesLine);
        }




        // POST: InvoicesLine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var invoicesLine = await _context.InvoicesLine.SingleOrDefaultAsync(m => m.invoicesLineId == id);
                _context.InvoicesLine.Remove(invoicesLine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InvoicesLineExists(string id)
        {
            return _context.InvoicesLine.Any(e => e.incomeId == id);
        }

    }
}





namespace netcore.MVC
{
  public static partial class Pages
  {
      public static class InvoicesLine
        {
          public const string Controller = "InvoicesLine";
          public const string Action = "Index";
          public const string Role = "InvoicesLine";
          public const string Url = "/InvoicesLine/Index";
          public const string Name = "InvoicesLine";
      }
  }
}
namespace netcore.Models
{
  public partial class ApplicationUser
  {
      [Display(Name = "InvoicesLine")]
      public bool InvoicesLineRole { get; set; } = false;
  }
}



