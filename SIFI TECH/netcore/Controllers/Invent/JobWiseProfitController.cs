﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using netcore.Services;
using netcore.Models.ManageViewModels;

namespace netcore.Controllers.Invent
{


    [Authorize(Roles = "JobWiseProfit")]
    public class JobWiseProfitController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public JobWiseProfitController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;

        }

        // GET: JobWiseProfit
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            JobWiseProfitViewModel jobWiseProfitViewModel = new JobWiseProfitViewModel();

            ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

            //var branchId = _service.GetUserBranchId(User.Identity.Name);

            //if (invoice.branchId != null && invoice.bookingId != null)
            //{
            //    invoice.invNumber = GenerateInvoiceNumber(invoice.branchId, invoice.bookingId);
            //}
            return View(jobWiseProfitViewModel);

        }


        //public async Task<IActionResult> ShowJobWiseProfit(string id)
        //{
        //    JobWiseProfitViewModel obj = await _context.JobWiseProfit
        //        .Include(x => x.booking)
        //        .Include(x => x.invoicesLine).ThenInclude(x => x.invoices)
        //        .Include(x => x.branch)


        //        .SingleOrDefaultAsync(x => x.invoicesId.Equals(id));
        //    _context.Update(obj);

        //    return View(obj);
        //}
        //public async Task<IActionResult> PrintInvoice(string id)
        //{
        //    JobWiseProfitViewModel obj = await _context.JobWiseProfit
        //        .Include(x => x.booking)
        //        .Include(x => x.invoicesLine).ThenInclude(x => x.currency)
        //        .Include(x => x.booking.bookingSlot)
        //        .Include(x => x.booking.salesInquiry.customer)
        //        .Include(x => x.booking.salesInquiry)
        //        .ThenInclude(x => x.salesInquiryLine).ThenInclude(x => x.containerLaden)
        //        .Include(x => x.booking.fromLocation)
        //        .Include(x => x.booking.toLocation)
        //        .Include(x => x.booking.bookingSlot.vendor)
        //        .Include(x => x.invoicesLine).ThenInclude(x => x.cost)
        //        .Include(x => x.invoicesLine).ThenInclude(x => x.invoices)
        //        .Include(x => x.booking.bookingLine).ThenInclude(x => x.containerLaden)
        //        .Include(x => x.booking.bookingLine).ThenInclude(x => x.containerLaden.container)
        //        .Include(x => x.branch)


        //        .SingleOrDefaultAsync(x => x.invoicesId.Equals(id));
        //    _context.Update(obj);

        //    return View(obj);
        //}




        //public async Task<IActionResult> LineIndex()
        //{
        //    var applicationDbContext = _context.JobWiseProfitLineViewModel.Include(p => p.invoices).Include(p => p.cost).Include(p => p.income).Include(p => p.currency);

        //    return View(await applicationDbContext.ToListAsync());
        //}

        // GET: JobWiseProfit/Details/5
        //public async Task<IActionResult> Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var invoices = await _context.JobWiseProfit
        //.Include(x => x.invoicesLine)
        //.Include(p => p.booking)

        //    .SingleOrDefaultAsync(m => m.invoicesId == id);
        //    if (invoices == null)
        //    {
        //        return NotFound();
        //    }
        //    _context.Update(invoices);
        //    await _context.SaveChangesAsync();

        //    return View(invoices);


        //}

        //public IActionResult GeneratePNL(string id)
        //{
        //    var list = _context.InlandPriceLine.Include(x => x.chargeHead).Include(x => x.containerLaden).Include(x => x.currency).Include(x => x.containerEmpty).Where(x => x.inlandPriceId.Equals(masterid)).ToList();
        //    return Json(new { data = list });
        //}
        [HttpGet]
        public async Task<IActionResult> GeneratePNL(string id, string branchId)
        {

             var salesInvoiceList =  _context.Invoices
                    .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                    .Include(x => x.booking).ThenInclude(x => x.toLocation)
      
                    .Include(p => p.branch)
                    .Where(x => x.bookingId.Equals(id) && x.branchId.Equals(branchId) && x.status.Equals(InvoiceStatus.COMPLETED)).ToList();

            var billOfLadingList = _context.BillOfLoading
                    .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                    .Include(x => x.booking).ThenInclude(x => x.toLocation)
                    .Include(p => p.branch)
                    .SingleOrDefault(x => x.bookingId.Equals(id) && x.branchId.Equals(branchId));

            var purchaseInvoiceList = _context.PurchaseInvoice
                    .Include(x => x.booking).ThenInclude(x => x.fromLocation)
                    .Include(x => x.booking).ThenInclude(x => x.toLocation)
      
                    .Include(p => p.branch)
  
                    .Where(x => x.bookingId.Equals(id) && x.branchId.Equals(branchId) && x.status.Equals(InvoiceStatus.COMPLETED)).ToList();

            var jobWiseProfit = new JobWiseProfitViewModel();
            if (salesInvoiceList.Count > 0)
            {
                jobWiseProfit.mblNumber = salesInvoiceList.FirstOrDefault().nblNumber;
                jobWiseProfit.fromlocationName = salesInvoiceList.FirstOrDefault().booking.fromLocation.locationName;
                jobWiseProfit.tolocationName = salesInvoiceList.FirstOrDefault().booking.toLocation.locationName;
                jobWiseProfit.noOfContainers = salesInvoiceList.FirstOrDefault().booking.noOfContainers;

                var incomeLineList = _context.InvoicesLine
                    .Include(x => x.invoices)
                    .Include(x => x.income)
                    .Where(x => x.invoices.branchId.Equals(branchId) && x.invoices.bookingId.Equals(id)).Distinct().ToList();

                var costLineList = _context.PurchaseInvoiceLine
                    .Include(x => x.purchaseInvoice)
                    .Include(x => x.chargeHead)
                    .Where(x => x.purchaseInvoice.branchId.Equals(branchId) && x.purchaseInvoice.bookingId.Equals(id)).Distinct().ToList();


                //var left = from incomeList in incomeLineList
                //            join costList in costLineList 
                //            on incomeList.invoices.bookingId equals costList.purchaseInvoice.bookingId 
                //            into incomCostGroup
                //            //from cost in costLineList
                //            //select new JobWiseProfitLineViewModel()
                //            //{
                //            //    bookingId = l1.invoices.bookingId,
                //            //    chargeHeadName = l1.income.chargeHeadName,
                //            //    incomeAmount = l1.incamount
                //            //}
                //           select new { incomeList, costList };

                //var QSOuterJoin = (from incomeList in incomeLineList
                //                   join costList in costLineList
                //                   on incomeList.invoices.bookingId equals costList.purchaseInvoice.bookingId into incomCostGroup
                //                   from cost in incomCostGroup.DefaultIfEmpty()
                //                   select new JobWiseProfitLineViewModel()
                //                   {
                //                       chargeHeadName = incomeList.income.chargeHeadName,
                //                       incomeAmount = cost == null ? 0 : cost.totalAmountWithGST,
                //                       marginAmount = incomeList.margin

                //                   }).Distinct().ToList();


                //var left = (from l1 in incomeLineList
                //            join l2 in costLineList on l1.invoices.bookingId equals l2.purchaseInvoice.bookingId into j
                //            from l3 in j.DefaultIfEmpty()
                //            select new JobWiseProfitLineViewModel()
                //            {
                //                bookingId = l1.invoices.bookingId,
                //                chargeHeadName = l3.chargeHead.chargeHeadName,
                //                incomeAmount = l1.incamount,
                //                costAmount = l3.totalAmountWithGST
                //            }
                //         ).ToList();

                //var right = (from l2 in costLineList
                //             join l1 in incomeLineList on l2.purchaseInvoice.bookingId equals l1.invoices.bookingId into k
                //             from l1 in k.DefaultIfEmpty()
                //             select new JobWiseProfitLineViewModel()
                //             {
                //                 bookingId = l2.purchaseInvoice.bookingId,
                //                 chargeHeadName = l2.chargeHead.chargeHeadName,
                //                 costAmount = l2.totalAmountWithGST
                //             }
                //           ).ToList();



                //var joinedList = (from income in left
                //                  join cost in right on income.bookingId equals cost.bookingId
                //                  select new
                //                  {
                //                      income.chargeHeadName,
                //                      income.incomeAmount,
                //                      cost.costAmount,
                //                      income.marginAmount
                //                  }).Distinct();


                //var joinedList = (from income in incomeLineList
                //         join cost in costLineList on income.invoices.bookingId equals cost.purchaseInvoice.bookingId
                //         orderby cost.chargeHead.chargeHeadName
                //         select new
                //         {
                //             income.income.chargeHeadName,
                //             income.incamount,
                //             cost.totalAmountWithGST,
                //             income.margin
                //         }).ToList();



                //var resultList = left.Union(right).ToList();


                foreach (var item in incomeLineList)
                {
                    var incomeModel = new JobWiseProfitLineViewModel();
                    incomeModel.chargeHeadName = item.income.chargeHeadName;
                    incomeModel.incomeAmount = item.incamount;
                    incomeModel.marginAmount = item.margin;
                    jobWiseProfit.jobWiseProfitLineViewModel.Add(incomeModel);
                }

                foreach (var item in costLineList)
                {
                    var costModel = new JobWiseProfitCostLineViewModel();
                    costModel.chargeHeadName = item.chargeHead.chargeHeadName;
                    costModel.costAmount = item.totalAmountWithGST;
                    jobWiseProfit.jobWiseProfitCostLineViewModel.Add(costModel);
                }
            }
            else
            {
                return Json(new { success = false, message = "No data found!" });
            }

            if (billOfLadingList != null)
            {
                jobWiseProfit.vessel = billOfLadingList.vesselName;

            }
   

            return Json(new { success = true, data = jobWiseProfit, message = "Generated successfully!" });
        }


        // GET: JobWiseProfit/Create
        //public IActionResult Create()
        //{
        //    JobWiseProfit invoice = new JobWiseProfit();

        //    ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
        //    ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

        //    //var branchId = _service.GetUserBranchId(User.Identity.Name);

        //    //if (invoice.branchId != null && invoice.bookingId != null)
        //    //{
        //    //    invoice.invNumber = GenerateInvoiceNumber(invoice.branchId, invoice.bookingId);
        //    //}
        //    return View(invoice);
        //}


        //[HttpGet]
        //public string GenerateInvoiceNumber(string inputBranchId, string inputBookingId)
        //{
        //    var inputBranch = _context.Branch.SingleOrDefault(x => x.branchId == inputBranchId);
        //    var inputPOL = _context.Booking.Include(x => x.fromLocation).SingleOrDefault(x => x.bookingId == inputBookingId).fromLocation.locationCode;
        //    var inputPOD = _context.Booking.Include(x => x.toLocation).SingleOrDefault(x => x.bookingId == inputBookingId).toLocation.locationCode;

        //    var start = 600;
        //    var invoiceId = start + _context.JobWiseProfit.Where(x => x.branchId == inputBranch.branchId).Count() + 1;

        //    var newInvoiceId = invoiceId.ToString().PadLeft((invoiceId.ToString().Length + (5 - invoiceId.ToString().Length)), '0');


        //    var invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/" + newInvoiceId;


        //    //Check if invoice/ sequence number is same in table
        //    var duplicateInvoiceNumber = _context.JobWiseProfit.Any(x => x.invNumber == invoiceNumber);

        //    if (duplicateInvoiceNumber)
        //    {
        //        var newInvoiceNumber = invoiceId + 1;

        //        var newSkipInvoiceId = newInvoiceNumber.ToString().PadLeft((newInvoiceNumber.ToString().Length + (5 - newInvoiceNumber.ToString().Length)), '0');

        //        invoiceNumber = inputBranch.branchName.Substring(0, 3).ToUpper() + "/" + inputPOL + "/" + inputPOD + "/" + newSkipInvoiceId;
        //    }


        //    return invoiceNumber;

        //}



        // POST: JobWiseProfit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("invoicesId,bookingId,branchId,nblNumber,invNumber,invDate,dueDate,utrNumber,totalInvoiceAmount,supplyTo,receiptAmount,tds,receiptDate,netBalance,status,preparedBy,creditDays, remark")] JobWiseProfit invoices)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var duplicateInvoiceNumberExists = _context.JobWiseProfit.Any(x => x.invNumber.Contains(invoices.invNumber));

        //        if (duplicateInvoiceNumberExists)
        //        {
        //            var newInvoiceNumber = GenerateInvoiceNumber(invoices.branchId, invoices.bookingId);
        //            invoices.invNumber = newInvoiceNumber;
        //        }


        //        _context.Add(invoices);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Details), new { id = invoices.invoicesId });
        //    }
        //    ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber");
        //    ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);


        //    return View(invoices);
        //}

        //// GET: JobWiseProfit/Edit/5
        //public async Task<IActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var invoices = await _context.JobWiseProfit.Include(x => x.invoicesLine).SingleOrDefaultAsync(m => m.invoicesId == id);

        //    if (invoices == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["bookingId"] = new SelectList(_context.Booking, "bookingId", "bookingNumber", invoices.bookingId);
        //    ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);

        //    return View(invoices);
        //}


        // POST: JobWiseProfit/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(string id)
        //{

        //    var invoices = await _context.JobWiseProfit
        //            .Include(x => x.invoicesLine)
        //            .Include(p => p.branch)
        //            .Include(p => p.booking)
        //            .SingleOrDefaultAsync(m => m.invoicesId == id);
        //    try
        //    {

        //        _context.JobWiseProfitLine.RemoveRange(invoices.invoicesLine);
        //        _context.JobWiseProfit.Remove(invoices);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch (Exception ex)
        //    {

        //        ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
        //        return View(invoices);
        //    }

        //}

        //private bool JobWiseProfitExists(string id)
        //{
        //    return _context.JobWiseProfit.Any(e => e.invoicesId == id);
        //}

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class JobWiseProfit
        {
            public const string Controller = "JobWiseProfit";
            public const string Action = "Index";
            public const string Role = "JobWiseProfit";
            public const string Url = "/JobWiseProfit/Index";
            public const string Name = "JobWiseProfit";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "JobWiseProfit")]
        public bool JobWiseProfitRole { get; set; } = false;
    }
}



