﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

using netcore.Data;
using netcore.Models.Invent;
using System.ComponentModel;
using netcore.Services;

namespace netcore.Controllers.Invent
{

    [DisplayName("Inland Price")]
    [Authorize(Roles = "InlandPrice")]
    public class InlandPriceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INetcoreService _service;


        public InlandPriceController(ApplicationDbContext context, INetcoreService service)
        {
            _context = context;
            _service = service;
        }

        public async Task<IActionResult> ShowInlandPrice(string id)
        {
            InlandPrice obj = await _context.InlandPrice
                .Include(x => x.vendor)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.inlandPrice)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                //.Include(x => x.fromLocation)
                //.Include(x => x.toLocation)

                .SingleOrDefaultAsync(x => x.inlandPriceId.Equals(id));
            //obj.totalOrderAmount = obj.purchaseOrderLine.Sum(x => x.totalAmount);
            //obj.totalDiscountAmount = obj.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(obj);

            return View(obj);
        }


        //Get List off Details of Vendor and Purchase Order by Product Id
        //[HttpGet]
        //public Task<List<PurchaseOrder>> GetPurchaseOrderDetails(string productModel)
        //{

        //    var obj = _context.PurchaseOrderLine
        //        .Where(x => x.product.productCode == productModel)
        //        .ToList();

        //    var purchaseOrderList = new List<PurchaseOrder>();
        //    var purchaseOrder = new PurchaseOrder();

        //    foreach (var item in obj)
        //    {
        //        purchaseOrder.vendorId = item.purchaseOrder.vendorId;
        //        purchaseOrderList.Add(purchaseOrder);
        //    }

        //    //_context.Update(purchaseOrderList);

        //    return Task.FromResult(purchaseOrderList);
        //}


        public async Task<IActionResult> PrintInlandPrice(string id)
        {
            InlandPrice obj = await _context.InlandPrice
           .Include(x => x.vendor)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.inlandPrice)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.chargeHead)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.containerEmpty)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.containerLaden)
                .Include(x => x.inlandPriceLine).ThenInclude(x => x.currency)
                .Include(x => x.branch)
                //.Include(x => x.fromLocation)
                //.Include(x => x.toLocation)

                .SingleOrDefaultAsync(x => x.inlandPriceId.Equals(id));
            return View(obj);
        }

        // GET: InlandPrice
        public async Task<IActionResult> Index()
        {
            var userBranchId = _service.GetUserBranchId(User.Identity.Name);

            IQueryable<InlandPrice> applicationDbContext;

            if (userBranchId != null)  //If Non Super Admin
            {
                applicationDbContext = _context.InlandPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.vendor)
                    .Include(p => p.branch).Where(x => x.branchId == _service.GetUserBranchId(User.Identity.Name));
            }
            else //If Super Admin
            {
               applicationDbContext = _context.InlandPrice.OrderByDescending(x => x.createdAt)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.chargeHead)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.currency)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.containerEmpty)
                    .Include(p => p.inlandPriceLine).ThenInclude(x => x.containerLaden)
                    .Include(p => p.vendor)
                    .Include(p => p.branch);
            }

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: InlandPrice/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inlandPrice = await _context.InlandPrice
                    .Include(x => x.inlandPriceLine)
                    .Include(p => p.vendor)
                    .Include(x => x.branch)
                    //.Include(p => p.fromLocation)
                    //.Include(p => p.toLocation)
                        .SingleOrDefaultAsync(m => m.inlandPriceId == id);
            if (inlandPrice == null)
            {
                return NotFound();
            }

            //inlandPrice.totalOrderAmount = inlandPrice.inlandPrice.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(inlandPrice);
            await _context.SaveChangesAsync();

            return View(inlandPrice);
        }


        // GET: InlandPrice/Create
        public IActionResult Create()
        {
            InlandPrice inlandPrice = new InlandPrice();
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            //ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //Branch defaultBranch = _context.Branch.Where(x => x.isDefaultBranch.Equals(true)).FirstOrDefault();
            //ViewData["branchId"] = new SelectList(_context.Branch, "branchId", "branchName", defaultBranch != null ? defaultBranch.branchId : null);


            //Another Alternative Way
            //var list = _context.Location.ToList();
            //ViewData["fromlocationId"] = new SelectList(list, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(list, "locationId", "locationName");

            return View(inlandPrice);
        }




        // POST: InlandPrice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("inlandPriceId,inlandPriceNumber,inlandPriceDate,branchId,vendorId,fromDate,toDate,pickupCountries,pickupLocation,pickupcity,pickupPincode,dropCountries.dropLocation,dropcity,dropPincode,remark,preparedBy,createdAt")] InlandPrice inlandPrice)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inlandPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = inlandPrice.inlandPriceId });
            }
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                          .Where(u => u.isActive == true).ToList()
                                          .Select(u => new {
                                              vendorId = u.vendorId,
                                              VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                          }),
                                              "vendorId", "VendorNameLoc");
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            //ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName");
            return View(inlandPrice);
        }

        // GET: InlandPrice/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inlandPrice = await _context.InlandPrice.Include(x => x.inlandPriceLine).SingleOrDefaultAsync(m => m.inlandPriceId == id);
            if (inlandPrice == null)
            {
                return NotFound();
            }

            //inlandPrice.totalOrderAmount = inlandPrice.inlandPriceLine.Sum(x => x.totalAmount);
            //inlandPrice.totalDiscountAmount = inlandPrice.inlandPriceLine.Sum(x => x.discountAmount);
            _context.Update(inlandPrice);
            await _context.SaveChangesAsync();
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                     .Where(u => u.isActive == true).ToList()
                                     .Select(u => new {
                                         vendorId = u.vendorId,
                                         VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                     }),
                                         "vendorId", "VendorNameLoc", inlandPrice.vendorId);
            //ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", inlandPrice.fromlocationId);
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", inlandPrice.tolocationId);
            //TempData["PurchaseOrderStatus"] = purchaseOrder.purchaseOrderStatus;
            //ViewData["StatusMessage"] = TempData["StatusMessage"];
            return View(inlandPrice);
        }

        // POST: InlandPrice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("inlandPriceId,inlandPriceNumber,inlandPriceDate,branchId,vendorId,fromDate,toDate,pickupCountries,pickupLocation,pickupcity,pickupPincode,dropCountries,dropLocation,dropcity,dropPincode,remark,preparedBy,createdAt")] InlandPrice inlandPrice)
        {
            if (id != inlandPrice.inlandPriceId)
            {
                return NotFound();
            }
            

            //if ((PurchaseOrderStatus)TempData["PurchaseOrderStatus"] == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit [Completed] order.";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            //if (purchaseOrder.purchaseOrderStatus == PurchaseOrderStatus.Completed)
            //{
            //    TempData["StatusMessage"] = "Error. Can not edit status to [Completed].";
            //    return RedirectToAction(nameof(Edit), new { id = purchaseOrder.purchaseOrderId });
            //}

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inlandPrice);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InlandPriceExists(inlandPrice.inlandPriceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["branchId"] = _service.GetUserBranches(User.Identity.Name);
            ViewData["vendorId"] = new SelectList(_context.Vendor
                                      .Where(u => u.isActive == true).ToList()
                                      .Select(u => new {
                                          vendorId = u.vendorId,
                                          VendorNameLoc = String.Concat(u.vendorName + " (" + u.city + ", " + u.countries.GetDisplayName() + ")")
                                      }),
                                          "vendorId", "VendorNameLoc", inlandPrice.vendorId);
            //ViewData["fromlocationId"] = new SelectList(_context.Location, "locationId", "locationName", inlandPrice.fromlocationId);
            //ViewData["tolocationId"] = new SelectList(_context.Location, "locationId", "locationName", inlandPrice.tolocationId);
            return View(inlandPrice);
        }

        // GET: InlandPrice/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inlandPrice = await _context.InlandPrice
                    .Include(x => x.inlandPriceLine)
                    .Include(p => p.branch)
                    .Include(p => p.vendor)
                    //.Include(p => p.fromLocation)
                    //.Include(p => p.toLocation)
                    .SingleOrDefaultAsync(m => m.inlandPriceId == id);
            if (inlandPrice == null)
            {
                return NotFound();
            }

            //purchaseOrder.totalOrderAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.totalAmount);
            //purchaseOrder.totalDiscountAmount = purchaseOrder.purchaseOrderLine.Sum(x => x.discountAmount);
            _context.Update(inlandPrice);
            await _context.SaveChangesAsync();

            return View(inlandPrice);
        }




        // POST: InlandPrice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var inlandPrice = await _context.InlandPrice
                .Include(x => x.inlandPriceLine)
                .SingleOrDefaultAsync(m => m.inlandPriceId == id);
            try
            {
                _context.InlandPriceLine.RemoveRange(inlandPrice.inlandPriceLine);
                _context.InlandPrice.Remove(inlandPrice);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {

                ViewData["StatusMessage"] = "Error. Calm Down ^_^ and please contact your SysAdmin with this message: " + ex;
                return View(inlandPrice);
            }
            
        }

        private bool InlandPriceExists(string id)
        {
            return _context.InlandPrice.Any(e => e.inlandPriceId == id);
        }

    }
}





namespace netcore.MVC
{
    public static partial class Pages
    {
        public static class InlandPrice
        {
            public const string Controller = "InlandPrice";
            public const string Action = "Index";
            public const string Role = "InlandPrice";
            public const string Url = "/InlandPrice/Index";
            public const string Name = "InlandPrice";
        }
    }
}
namespace netcore.Models
{
    public partial class ApplicationUser
    {
        [Display(Name = "InlandPrice")]
        public bool InlandPriceRole { get; set; } = false;
    }
}



