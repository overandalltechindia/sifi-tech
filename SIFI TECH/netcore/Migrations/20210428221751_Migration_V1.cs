﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_V1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Branch",
                columns: table => new
                {
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    accountNumber = table.Column<string>(maxLength: 50, nullable: true),
                    bankName = table.Column<string>(maxLength: 50, nullable: true),
                    benificiary = table.Column<string>(maxLength: 100, nullable: true),
                    branchLogoUrl = table.Column<string>(nullable: true),
                    branchName = table.Column<string>(maxLength: 50, nullable: false),
                    city = table.Column<string>(maxLength: 30, nullable: true),
                    country = table.Column<string>(maxLength: 30, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currency = table.Column<string>(maxLength: 50, nullable: true),
                    description = table.Column<string>(maxLength: 50, nullable: true),
                    gst = table.Column<string>(maxLength: 50, nullable: true),
                    iban = table.Column<string>(maxLength: 50, nullable: true),
                    ifsc = table.Column<string>(maxLength: 50, nullable: true),
                    isDefaultBranch = table.Column<bool>(nullable: false),
                    pan = table.Column<string>(maxLength: 50, nullable: true),
                    province = table.Column<string>(maxLength: 30, nullable: true),
                    street1 = table.Column<string>(maxLength: 50, nullable: false),
                    street2 = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branch", x => x.branchId);
                });

            migrationBuilder.CreateTable(
                name: "ChargeHead",
                columns: table => new
                {
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: false),
                    category = table.Column<int>(nullable: false),
                    categoryType = table.Column<int>(nullable: false),
                    chargeHeadName = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    part = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChargeHead", x => x.chargeHeadId);
                });

            migrationBuilder.CreateTable(
                name: "Container",
                columns: table => new
                {
                    containerId = table.Column<string>(maxLength: 38, nullable: false),
                    containerName = table.Column<string>(maxLength: 38, nullable: true),
                    containerType = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Container", x => x.containerId);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    currencyId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyCode = table.Column<string>(maxLength: 38, nullable: true),
                    currencyName = table.Column<string>(maxLength: 38, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.currencyId);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    customerId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    cinUan = table.Column<string>(nullable: true),
                    city = table.Column<string>(maxLength: 30, nullable: true),
                    contact = table.Column<string>(maxLength: 12, nullable: true),
                    country = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    creditAmount = table.Column<string>(nullable: true),
                    creditDays = table.Column<string>(nullable: true),
                    customerName = table.Column<string>(maxLength: 50, nullable: false),
                    description = table.Column<string>(maxLength: 50, nullable: true),
                    gstVat = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    panLicence = table.Column<string>(nullable: true),
                    province = table.Column<string>(maxLength: 30, nullable: true),
                    registrationDate = table.Column<DateTime>(nullable: false),
                    size = table.Column<int>(nullable: false),
                    street1 = table.Column<string>(maxLength: 256, nullable: false),
                    street2 = table.Column<string>(maxLength: 10, nullable: true),
                    tan = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.customerId);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    locationId = table.Column<string>(maxLength: 38, nullable: false),
                    LocationType = table.Column<int>(nullable: false),
                    countries = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    locationCode = table.Column<string>(maxLength: 38, nullable: true),
                    locationName = table.Column<string>(maxLength: 38, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.locationId);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    productId = table.Column<string>(maxLength: 38, nullable: false),
                    ProductCategoryName = table.Column<string>(nullable: true),
                    barcode = table.Column<string>(maxLength: 50, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(maxLength: 50, nullable: true),
                    minimumStock = table.Column<float>(nullable: false),
                    priceInUSD = table.Column<decimal>(nullable: false),
                    productCategory = table.Column<int>(nullable: false),
                    productCode = table.Column<string>(maxLength: 50, nullable: false),
                    productName = table.Column<string>(maxLength: 50, nullable: false),
                    productType = table.Column<int>(nullable: false),
                    serialNumber = table.Column<string>(maxLength: 50, nullable: true),
                    uom = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.productId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ApplicationUserRole = table.Column<bool>(nullable: false),
                    BillOfLoadingLineRole = table.Column<bool>(nullable: false),
                    BillOfLoadingRole = table.Column<bool>(nullable: false),
                    BookingLineRole = table.Column<bool>(nullable: false),
                    BookingRole = table.Column<bool>(nullable: false),
                    BookingSlotLineRole = table.Column<bool>(nullable: false),
                    BookingSlotRole = table.Column<bool>(nullable: false),
                    BranchRole = table.Column<bool>(nullable: false),
                    ChargeHeadRole = table.Column<bool>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    ContainerDetailsRole = table.Column<bool>(nullable: false),
                    ContainerRole = table.Column<bool>(nullable: false),
                    CurrencyRole = table.Column<bool>(nullable: false),
                    CustomerLineRole = table.Column<bool>(nullable: false),
                    CustomerRole = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    ExportOperationLineRole = table.Column<bool>(nullable: false),
                    ExportOperationRole = table.Column<bool>(nullable: false),
                    HomeRole = table.Column<bool>(nullable: false),
                    ImportOperationLineRole = table.Column<bool>(nullable: false),
                    ImportOperationRole = table.Column<bool>(nullable: false),
                    InlandPriceLineRole = table.Column<bool>(nullable: false),
                    InlandPriceRole = table.Column<bool>(nullable: false),
                    InvoicesLineRole = table.Column<bool>(nullable: false),
                    InvoicesRole = table.Column<bool>(nullable: false),
                    LocalPriceLineRole = table.Column<bool>(nullable: false),
                    LocalPriceRole = table.Column<bool>(nullable: false),
                    LocationRole = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    MultipalRateQuoteLineRole = table.Column<bool>(nullable: false),
                    MultipalRateQuoteRole = table.Column<bool>(nullable: false),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    OceanPriceLineRole = table.Column<bool>(nullable: false),
                    OceanPriceRole = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    PortPriceLineRole = table.Column<bool>(nullable: false),
                    PortPriceRole = table.Column<bool>(nullable: false),
                    PortStoragePriceLineRole = table.Column<bool>(nullable: false),
                    SalesInquiryLineRole = table.Column<bool>(nullable: false),
                    SalesInquiryRole = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    VendorLineRole = table.Column<bool>(nullable: false),
                    VendorRole = table.Column<bool>(nullable: false),
                    branchId = table.Column<string>(nullable: true),
                    isSuperAdmin = table.Column<bool>(nullable: false),
                    profilePictureUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CustomerLine",
                columns: table => new
                {
                    customerLineId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(maxLength: 38, nullable: true),
                    fax = table.Column<string>(maxLength: 20, nullable: true),
                    firstName = table.Column<string>(maxLength: 20, nullable: false),
                    gender = table.Column<int>(nullable: false),
                    jobTitle = table.Column<string>(maxLength: 20, nullable: false),
                    lastName = table.Column<string>(maxLength: 20, nullable: false),
                    middleName = table.Column<string>(maxLength: 20, nullable: true),
                    mobilePhone = table.Column<string>(maxLength: 20, nullable: true),
                    nickName = table.Column<string>(maxLength: 20, nullable: true),
                    officePhone = table.Column<string>(maxLength: 20, nullable: true),
                    personalEmail = table.Column<string>(maxLength: 50, nullable: true),
                    salutation = table.Column<int>(nullable: false),
                    workEmail = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerLine", x => x.customerLineId);
                    table.ForeignKey(
                        name: "FK_CustomerLine_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "MultipalRateQuote",
                columns: table => new
                {
                    multipalRateQuoteId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(maxLength: 38, nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    multipalRateQuoteDate = table.Column<DateTime>(nullable: false),
                    multipalRateQuoteNumber = table.Column<string>(maxLength: 20, nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    status = table.Column<int>(nullable: false),
                    toDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultipalRateQuote", x => x.multipalRateQuoteId);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuote_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuote_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ContainerDetails",
                columns: table => new
                {
                    containerDetailsId = table.Column<string>(maxLength: 38, nullable: false),
                    availableOn = table.Column<DateTime>(nullable: false),
                    barcode = table.Column<string>(maxLength: 50, nullable: true),
                    branchId = table.Column<string>(nullable: true),
                    containerCode = table.Column<string>(maxLength: 50, nullable: false),
                    containerId = table.Column<string>(nullable: true),
                    containerManufactureDate = table.Column<DateTime>(nullable: false),
                    containerStatus = table.Column<int>(nullable: false),
                    containerleasingDate = table.Column<DateTime>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    dateofPurchase = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(maxLength: 50, nullable: true),
                    emptyyardLocationId = table.Column<string>(nullable: true),
                    locationId = table.Column<string>(nullable: true),
                    noOfIdleDays = table.Column<int>(nullable: false),
                    noofLeasedays = table.Column<int>(nullable: false),
                    ownership = table.Column<int>(nullable: false),
                    payloadcapacity = table.Column<decimal>(nullable: false),
                    purchasedLeasingLocation = table.Column<string>(nullable: true),
                    purchasedLeasingParty = table.Column<string>(nullable: true),
                    serialNumber = table.Column<string>(maxLength: 50, nullable: true),
                    tareweight = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContainerDetails", x => x.containerDetailsId);
                    table.ForeignKey(
                        name: "FK_ContainerDetails_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ContainerDetails_Container_containerId",
                        column: x => x.containerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ContainerDetails_Location_emptyyardLocationId",
                        column: x => x.emptyyardLocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ContainerDetails_Location_locationId",
                        column: x => x.locationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "SalesInquiry",
                columns: table => new
                {
                    salesInquiryId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(nullable: false),
                    freightTerm = table.Column<int>(nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: false),
                    mode = table.Column<int>(nullable: false),
                    onCarriage = table.Column<int>(nullable: false),
                    placeofDeliveryId = table.Column<string>(nullable: true),
                    placeofReceiptId = table.Column<string>(nullable: true),
                    podCountry = table.Column<string>(maxLength: 30, nullable: true),
                    podTerm = table.Column<int>(nullable: false),
                    polCountry = table.Column<string>(maxLength: 30, nullable: true),
                    polTerm = table.Column<int>(nullable: false),
                    precarriage = table.Column<int>(nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    salePerson = table.Column<string>(nullable: true),
                    salesInquiryDate = table.Column<DateTime>(nullable: false),
                    salesInquiryNumber = table.Column<string>(maxLength: 20, nullable: false),
                    shipper = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    toDate = table.Column<DateTime>(nullable: false),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: false),
                    trpPortId = table.Column<string>(maxLength: 38, nullable: true),
                    via = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesInquiry", x => x.salesInquiryId);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Location_placeofDeliveryId",
                        column: x => x.placeofDeliveryId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Location_placeofReceiptId",
                        column: x => x.placeofReceiptId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiry_Location_trpPortId",
                        column: x => x.trpPortId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Vendor",
                columns: table => new
                {
                    vendorId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    cinUan = table.Column<string>(nullable: true),
                    city = table.Column<string>(maxLength: 30, nullable: true),
                    contact = table.Column<string>(maxLength: 20, nullable: true),
                    countries = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    creditAmount = table.Column<string>(nullable: true),
                    creditDays = table.Column<string>(nullable: true),
                    description = table.Column<string>(maxLength: 50, nullable: true),
                    gstVat = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    locationId = table.Column<string>(maxLength: 38, nullable: false),
                    panLicence = table.Column<string>(nullable: true),
                    province = table.Column<string>(maxLength: 30, nullable: true),
                    registrationDate = table.Column<DateTime>(nullable: false),
                    size = table.Column<int>(nullable: false),
                    street1 = table.Column<string>(maxLength: 256, nullable: false),
                    street2 = table.Column<string>(maxLength: 100, nullable: true),
                    tan = table.Column<string>(nullable: true),
                    vendorName = table.Column<string>(maxLength: 50, nullable: false),
                    vendorType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendor", x => x.vendorId);
                    table.ForeignKey(
                        name: "FK_Vendor_Location_locationId",
                        column: x => x.locationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "MultipalRateQuoteLine",
                columns: table => new
                {
                    multipalRateQuoteLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: false),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    mode = table.Column<int>(nullable: false),
                    multipalRateQuoteId = table.Column<string>(maxLength: 38, nullable: true),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: false),
                    trpPortId = table.Column<string>(maxLength: 38, nullable: true),
                    via = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultipalRateQuoteLine", x => x.multipalRateQuoteLineId);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_MultipalRateQuote_multipalRateQuoteId",
                        column: x => x.multipalRateQuoteId,
                        principalTable: "MultipalRateQuote",
                        principalColumn: "multipalRateQuoteId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_MultipalRateQuoteLine_Location_trpPortId",
                        column: x => x.trpPortId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "SalesInquiryLine",
                columns: table => new
                {
                    salesInquiryLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    count = table.Column<decimal>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    part = table.Column<int>(nullable: false),
                    saleCategory = table.Column<int>(nullable: false),
                    salesInquiryId = table.Column<string>(maxLength: 38, nullable: true),
                    totalPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesInquiryLine", x => x.salesInquiryLineId);
                    table.ForeignKey(
                        name: "FK_SalesInquiryLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiryLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiryLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_SalesInquiryLine_SalesInquiry_salesInquiryId",
                        column: x => x.salesInquiryId,
                        principalTable: "SalesInquiry",
                        principalColumn: "salesInquiryId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BookingSlot",
                columns: table => new
                {
                    bookingSlotId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    bookingSlotDate = table.Column<DateTime>(nullable: false),
                    bookingSlotNumber = table.Column<string>(maxLength: 20, nullable: false),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    destinationFreeDays = table.Column<decimal>(nullable: false),
                    destinationPortStorageDays = table.Column<decimal>(nullable: false),
                    documentCuttofftDate = table.Column<DateTime>(nullable: false),
                    documentCuttofftDateIcd = table.Column<DateTime>(nullable: false),
                    etaDate = table.Column<DateTime>(nullable: false),
                    etdDate = table.Column<DateTime>(nullable: false),
                    formCuttofftDate = table.Column<DateTime>(nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: false),
                    gateCloseCuttofftDate = table.Column<DateTime>(nullable: false),
                    gateOpenCuttofftDate = table.Column<DateTime>(nullable: false),
                    hazoggCuttofftDate = table.Column<DateTime>(nullable: false),
                    originFreeDays = table.Column<decimal>(nullable: false),
                    originPortStorageDays = table.Column<decimal>(nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    service = table.Column<string>(maxLength: 128, nullable: true),
                    siCuttofftDate = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    toDate = table.Column<DateTime>(nullable: false),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: false),
                    vendorId = table.Column<string>(maxLength: 38, nullable: false),
                    vesselName = table.Column<string>(maxLength: 128, nullable: true),
                    vgmCuttofftDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingSlot", x => x.bookingSlotId);
                    table.ForeignKey(
                        name: "FK_BookingSlot_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BookingSlot_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BookingSlot_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BookingSlot_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "InlandPrice",
                columns: table => new
                {
                    inlandPriceId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    dropCountries = table.Column<int>(nullable: false),
                    dropLocation = table.Column<string>(maxLength: 64, nullable: true),
                    dropPincode = table.Column<string>(maxLength: 64, nullable: true),
                    dropcity = table.Column<string>(maxLength: 64, nullable: true),
                    fromDate = table.Column<DateTime>(nullable: false),
                    inlandPriceDate = table.Column<DateTime>(nullable: false),
                    inlandPriceNumber = table.Column<string>(maxLength: 20, nullable: false),
                    pickupCountries = table.Column<int>(nullable: false),
                    pickupLocation = table.Column<string>(maxLength: 64, nullable: true),
                    pickupPincode = table.Column<string>(maxLength: 64, nullable: true),
                    pickupcity = table.Column<string>(maxLength: 64, nullable: true),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    toDate = table.Column<DateTime>(nullable: false),
                    vendorId = table.Column<string>(maxLength: 38, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InlandPrice", x => x.inlandPriceId);
                    table.ForeignKey(
                        name: "FK_InlandPrice_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InlandPrice_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "LocalPrice",
                columns: table => new
                {
                    localPriceId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    eximType = table.Column<int>(nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    localPriceDate = table.Column<DateTime>(nullable: false),
                    localPriceNumber = table.Column<string>(maxLength: 20, nullable: false),
                    locationId = table.Column<string>(maxLength: 38, nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    status = table.Column<int>(nullable: false),
                    toDate = table.Column<DateTime>(nullable: false),
                    vendorId = table.Column<string>(maxLength: 38, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalPrice", x => x.localPriceId);
                    table.ForeignKey(
                        name: "FK_LocalPrice_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPrice_Location_locationId",
                        column: x => x.locationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPrice_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "OceanPrice",
                columns: table => new
                {
                    oceanPriceId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    freightTerm = table.Column<int>(nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: false),
                    mode = table.Column<int>(nullable: false),
                    oceanPriceDate = table.Column<DateTime>(nullable: false),
                    oceanPriceNumber = table.Column<string>(maxLength: 20, nullable: false),
                    onCarriage = table.Column<int>(nullable: false),
                    placeofDeliveryId = table.Column<string>(nullable: true),
                    placeofReceiptId = table.Column<string>(nullable: true),
                    podCountry = table.Column<int>(nullable: false),
                    podTerm = table.Column<int>(nullable: false),
                    polCountry = table.Column<int>(nullable: false),
                    polTerm = table.Column<int>(nullable: false),
                    precarriage = table.Column<int>(nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    status = table.Column<int>(nullable: false),
                    toDate = table.Column<DateTime>(nullable: false),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: false),
                    trpPortId = table.Column<string>(maxLength: 38, nullable: true),
                    vendorId = table.Column<string>(maxLength: 38, nullable: false),
                    via = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OceanPrice", x => x.oceanPriceId);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Location_placeofDeliveryId",
                        column: x => x.placeofDeliveryId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Location_placeofReceiptId",
                        column: x => x.placeofReceiptId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Location_trpPortId",
                        column: x => x.trpPortId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPrice_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "PortPrice",
                columns: table => new
                {
                    portPriceId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    fromDate = table.Column<DateTime>(nullable: false),
                    locationId = table.Column<string>(maxLength: 38, nullable: false),
                    portPriceDate = table.Column<DateTime>(nullable: false),
                    portPriceNumber = table.Column<string>(maxLength: 20, nullable: false),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    toDate = table.Column<DateTime>(nullable: false),
                    vendorId = table.Column<string>(maxLength: 38, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PortPrice", x => x.portPriceId);
                    table.ForeignKey(
                        name: "FK_PortPrice_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPrice_Location_locationId",
                        column: x => x.locationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPrice_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "VendorLine",
                columns: table => new
                {
                    vendorLineId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    fax = table.Column<string>(maxLength: 20, nullable: true),
                    firstName = table.Column<string>(maxLength: 20, nullable: false),
                    gender = table.Column<int>(nullable: false),
                    jobTitle = table.Column<string>(maxLength: 20, nullable: false),
                    lastName = table.Column<string>(maxLength: 20, nullable: false),
                    middleName = table.Column<string>(maxLength: 20, nullable: true),
                    mobilePhone = table.Column<string>(maxLength: 20, nullable: true),
                    nickName = table.Column<string>(maxLength: 20, nullable: true),
                    officePhone = table.Column<string>(maxLength: 20, nullable: true),
                    personalEmail = table.Column<string>(maxLength: 50, nullable: true),
                    salutation = table.Column<int>(nullable: false),
                    vendorId = table.Column<string>(maxLength: 38, nullable: true),
                    workEmail = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorLine", x => x.vendorLineId);
                    table.ForeignKey(
                        name: "FK_VendorLine_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    bookingId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    bookingNumber = table.Column<string>(maxLength: 35, nullable: true),
                    bookingSlotId = table.Column<string>(nullable: true),
                    bookingValidity = table.Column<DateTime>(nullable: false),
                    branchId = table.Column<string>(maxLength: 38, nullable: true),
                    city = table.Column<string>(maxLength: 64, nullable: true),
                    commodity = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    destinationFreeDays = table.Column<decimal>(nullable: false),
                    destinationPortStorageDays = table.Column<decimal>(nullable: false),
                    docCutOff = table.Column<DateTime>(nullable: false),
                    earliestdepatureDate = table.Column<DateTime>(nullable: false),
                    emailId = table.Column<string>(maxLength: 64, nullable: true),
                    emptyPickupLocation = table.Column<string>(nullable: true),
                    eta = table.Column<DateTime>(nullable: false),
                    etd = table.Column<DateTime>(nullable: false),
                    finalDestination = table.Column<string>(nullable: true),
                    freightTerms = table.Column<string>(nullable: true),
                    fromServiceMode = table.Column<int>(nullable: false),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: true),
                    hsCode = table.Column<string>(nullable: true),
                    isEmptyContainer = table.Column<bool>(nullable: false),
                    noOfContainers = table.Column<string>(nullable: true),
                    originFreeDays = table.Column<decimal>(nullable: false),
                    originPortStorageDays = table.Column<decimal>(nullable: false),
                    placeofDeliveryId = table.Column<string>(nullable: true),
                    placeofReceiptId = table.Column<string>(nullable: true),
                    preparedBy = table.Column<string>(nullable: true),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    salesInquiryId = table.Column<string>(nullable: true),
                    salesPersonName = table.Column<string>(maxLength: 64, nullable: true),
                    status = table.Column<int>(nullable: false),
                    stuffingPoint = table.Column<string>(nullable: true),
                    stuffingType = table.Column<string>(nullable: true),
                    surveyorDetails = table.Column<string>(nullable: true),
                    toServiceMode = table.Column<int>(nullable: false),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: true),
                    vesselGateInTerminal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.bookingId);
                    table.ForeignKey(
                        name: "FK_Booking_BookingSlot_bookingSlotId",
                        column: x => x.bookingSlotId,
                        principalTable: "BookingSlot",
                        principalColumn: "bookingSlotId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Location_placeofDeliveryId",
                        column: x => x.placeofDeliveryId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Location_placeofReceiptId",
                        column: x => x.placeofReceiptId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_SalesInquiry_salesInquiryId",
                        column: x => x.salesInquiryId,
                        principalTable: "SalesInquiry",
                        principalColumn: "salesInquiryId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Booking_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BookingSlotLine",
                columns: table => new
                {
                    bookingSlotLineId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingSlotId = table.Column<string>(maxLength: 38, nullable: true),
                    bookingSlotLineNumber = table.Column<string>(maxLength: 20, nullable: false),
                    containerCategory = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    qty = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingSlotLine", x => x.bookingSlotLineId);
                    table.ForeignKey(
                        name: "FK_BookingSlotLine_BookingSlot_bookingSlotId",
                        column: x => x.bookingSlotId,
                        principalTable: "BookingSlot",
                        principalColumn: "bookingSlotId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BookingSlotLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "InlandPriceLine",
                columns: table => new
                {
                    inlandPriceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    category = table.Column<int>(nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    empty = table.Column<int>(nullable: false),
                    emptyContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    emptyPrice = table.Column<decimal>(nullable: false),
                    inlandPriceId = table.Column<string>(maxLength: 38, nullable: true),
                    laden = table.Column<int>(nullable: false),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    part = table.Column<int>(nullable: false),
                    wtFrom = table.Column<decimal>(nullable: false),
                    wtTo = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InlandPriceLine", x => x.inlandPriceLineId);
                    table.ForeignKey(
                        name: "FK_InlandPriceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InlandPriceLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InlandPriceLine_Container_emptyContainerId",
                        column: x => x.emptyContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InlandPriceLine_InlandPrice_inlandPriceId",
                        column: x => x.inlandPriceId,
                        principalTable: "InlandPrice",
                        principalColumn: "inlandPriceId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InlandPriceLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "LocalPriceLine",
                columns: table => new
                {
                    localPriceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    containerCategory = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    empty = table.Column<int>(nullable: false),
                    emptyContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    emptyPrice = table.Column<decimal>(nullable: false),
                    laden = table.Column<int>(nullable: false),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    localPriceId = table.Column<string>(maxLength: 38, nullable: true),
                    part = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalPriceLine", x => x.localPriceLineId);
                    table.ForeignKey(
                        name: "FK_LocalPriceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPriceLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPriceLine_Container_emptyContainerId",
                        column: x => x.emptyContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPriceLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_LocalPriceLine_LocalPrice_localPriceId",
                        column: x => x.localPriceId,
                        principalTable: "LocalPrice",
                        principalColumn: "localPriceId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "OceanPriceLine",
                columns: table => new
                {
                    oceanPriceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    emptyContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    emptyPrice = table.Column<decimal>(nullable: true),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    oceanPriceId = table.Column<string>(maxLength: 38, nullable: true),
                    part = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OceanPriceLine", x => x.oceanPriceLineId);
                    table.ForeignKey(
                        name: "FK_OceanPriceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPriceLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPriceLine_Container_emptyContainerId",
                        column: x => x.emptyContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPriceLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_OceanPriceLine_OceanPrice_oceanPriceId",
                        column: x => x.oceanPriceId,
                        principalTable: "OceanPrice",
                        principalColumn: "oceanPriceId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "PortPriceLine",
                columns: table => new
                {
                    portPriceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    containerCategory = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    empty = table.Column<int>(nullable: false),
                    emptyContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    emptyPrice = table.Column<decimal>(nullable: false),
                    laden = table.Column<int>(nullable: false),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    ladenPrice = table.Column<decimal>(nullable: false),
                    part = table.Column<int>(nullable: false),
                    portPriceId = table.Column<string>(maxLength: 38, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PortPriceLine", x => x.portPriceLineId);
                    table.ForeignKey(
                        name: "FK_PortPriceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPriceLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPriceLine_Container_emptyContainerId",
                        column: x => x.emptyContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPriceLine_Container_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "Container",
                        principalColumn: "containerId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortPriceLine_PortPrice_portPriceId",
                        column: x => x.portPriceId,
                        principalTable: "PortPrice",
                        principalColumn: "portPriceId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "PortStoragePriceLine",
                columns: table => new
                {
                    portStoragePriceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    fortyPrice = table.Column<decimal>(nullable: false),
                    fromDay = table.Column<decimal>(nullable: false),
                    portPriceId = table.Column<string>(maxLength: 38, nullable: true),
                    toDay = table.Column<decimal>(nullable: false),
                    twentyPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PortStoragePriceLine", x => x.portStoragePriceLineId);
                    table.ForeignKey(
                        name: "FK_PortStoragePriceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PortStoragePriceLine_PortPrice_portPriceId",
                        column: x => x.portPriceId,
                        principalTable: "PortPrice",
                        principalColumn: "portPriceId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BillOfLoading",
                columns: table => new
                {
                    billOfLoadingId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    bLType = table.Column<int>(nullable: false),
                    billOfLoadingNumber = table.Column<string>(maxLength: 20, nullable: true),
                    bookingId = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: true),
                    brokerName = table.Column<string>(nullable: true),
                    consignee = table.Column<string>(nullable: true),
                    consigneeTaxId = table.Column<string>(nullable: true),
                    coppies = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    destinationAgentReference = table.Column<string>(nullable: true),
                    destinationHaulage = table.Column<int>(nullable: false),
                    destinationPort = table.Column<int>(nullable: false),
                    documentType = table.Column<int>(nullable: false),
                    freightPayableAt = table.Column<int>(nullable: false),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: true),
                    invoiceReference = table.Column<string>(nullable: true),
                    movementType = table.Column<int>(nullable: false),
                    notifyTaxId = table.Column<string>(nullable: true),
                    originHaulage = table.Column<int>(nullable: false),
                    originPort = table.Column<int>(nullable: false),
                    originalBl = table.Column<string>(nullable: true),
                    placeOfDelivery = table.Column<string>(nullable: true),
                    placeOfReceipt = table.Column<string>(nullable: true),
                    remark = table.Column<string>(nullable: true),
                    seaFreaightAdditionals = table.Column<int>(nullable: false),
                    setCharges = table.Column<int>(nullable: false),
                    shipper = table.Column<string>(nullable: true),
                    shipperTaxId = table.Column<string>(nullable: true),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: true),
                    totalNumberCargoes = table.Column<string>(nullable: true),
                    totalNumberContainers = table.Column<string>(nullable: true),
                    vesselName = table.Column<string>(nullable: true),
                    voyageNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillOfLoading", x => x.billOfLoadingId);
                    table.ForeignKey(
                        name: "FK_BillOfLoading_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BillOfLoading_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BillOfLoading_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BillOfLoading_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BookingLine",
                columns: table => new
                {
                    bookingLineId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingId = table.Column<string>(maxLength: 38, nullable: true),
                    containerCategory = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    height = table.Column<decimal>(nullable: true),
                    ladenContainerId = table.Column<string>(maxLength: 38, nullable: true),
                    length = table.Column<decimal>(nullable: true),
                    temperature = table.Column<decimal>(nullable: true),
                    weight = table.Column<decimal>(nullable: true),
                    width = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingLine", x => x.bookingLineId);
                    table.ForeignKey(
                        name: "FK_BookingLine_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BookingLine_ContainerDetails_ladenContainerId",
                        column: x => x.ladenContainerId,
                        principalTable: "ContainerDetails",
                        principalColumn: "containerDetailsId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ExportOperation",
                columns: table => new
                {
                    exportOperationId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    bookingDate = table.Column<DateTime>(nullable: false),
                    bookingId = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(nullable: true),
                    classes = table.Column<string>(nullable: true),
                    commodity = table.Column<string>(nullable: true),
                    containerCategory = table.Column<int>(nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(nullable: true),
                    destinationFreeDays = table.Column<string>(nullable: true),
                    destinationPortStorage = table.Column<string>(nullable: true),
                    docHandoverdate = table.Column<DateTime>(nullable: false),
                    egmDate = table.Column<DateTime>(nullable: false),
                    egmNumber = table.Column<string>(nullable: true),
                    etaDate = table.Column<DateTime>(nullable: false),
                    etdDate = table.Column<DateTime>(nullable: false),
                    exportOperationNumber = table.Column<string>(maxLength: 20, nullable: true),
                    exportOperationeDate = table.Column<DateTime>(nullable: false),
                    freight = table.Column<string>(nullable: true),
                    hsCode = table.Column<string>(nullable: true),
                    leoDate = table.Column<DateTime>(nullable: false),
                    lineCode = table.Column<string>(nullable: true),
                    noOfContainer = table.Column<string>(nullable: true),
                    originFreeDays = table.Column<string>(nullable: true),
                    originPortStorage = table.Column<string>(nullable: true),
                    pod = table.Column<string>(nullable: true),
                    pol = table.Column<string>(nullable: true),
                    preparedby = table.Column<string>(nullable: true),
                    remark = table.Column<string>(nullable: true),
                    sbNumber = table.Column<string>(nullable: true),
                    shipper = table.Column<string>(nullable: true),
                    shippingBillNo = table.Column<string>(nullable: true),
                    slotOperator = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    stuffingType = table.Column<string>(nullable: true),
                    surveyor = table.Column<string>(nullable: true),
                    terminal = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    uan = table.Column<string>(nullable: true),
                    vessel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExportOperation", x => x.exportOperationId);
                    table.ForeignKey(
                        name: "FK_ExportOperation_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ExportOperation_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ExportOperation_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    invoicesId = table.Column<string>(maxLength: 38, nullable: false),
                    bookingId = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    creditDays = table.Column<string>(nullable: true),
                    dueDate = table.Column<DateTime>(nullable: false),
                    invDate = table.Column<DateTime>(nullable: false),
                    invNumber = table.Column<string>(nullable: true),
                    nblNumber = table.Column<string>(nullable: true),
                    netBalance = table.Column<string>(nullable: true),
                    preparedBy = table.Column<string>(nullable: true),
                    receiptAmount = table.Column<string>(nullable: true),
                    receiptDate = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    tds = table.Column<string>(nullable: true),
                    totalInvoiceAmount = table.Column<string>(nullable: true),
                    utrNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.invoicesId);
                    table.ForeignKey(
                        name: "FK_Invoices_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Invoices_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BillOfLoadingLine",
                columns: table => new
                {
                    billOfLoadingLineId = table.Column<string>(maxLength: 38, nullable: false),
                    billOfLoadingId = table.Column<string>(maxLength: 38, nullable: true),
                    cargoItemNo = table.Column<string>(nullable: true),
                    containerNumberId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    grossCargoWeight = table.Column<string>(nullable: true),
                    hsCode = table.Column<string>(nullable: true),
                    kindOfPackages = table.Column<string>(nullable: true),
                    marksAndNo = table.Column<string>(nullable: true),
                    measurement = table.Column<string>(nullable: true),
                    netCargoWeight = table.Column<string>(nullable: true),
                    noOfPackages = table.Column<string>(nullable: true),
                    sealno1 = table.Column<string>(nullable: true),
                    sealno2 = table.Column<string>(nullable: true),
                    sealno3 = table.Column<string>(nullable: true),
                    shippingBillDate = table.Column<DateTime>(nullable: false),
                    unit = table.Column<int>(nullable: false),
                    woodPack = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillOfLoadingLine", x => x.billOfLoadingLineId);
                    table.ForeignKey(
                        name: "FK_BillOfLoadingLine_BillOfLoading_billOfLoadingId",
                        column: x => x.billOfLoadingId,
                        principalTable: "BillOfLoading",
                        principalColumn: "billOfLoadingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_BillOfLoadingLine_ContainerDetails_containerNumberId",
                        column: x => x.containerNumberId,
                        principalTable: "ContainerDetails",
                        principalColumn: "containerDetailsId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ImportOperation",
                columns: table => new
                {
                    importOperationId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    billOfLadingDate = table.Column<DateTime>(nullable: false),
                    billOfLadingId = table.Column<string>(nullable: true),
                    bookingDate = table.Column<DateTime>(nullable: false),
                    bookingId = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(nullable: true),
                    category = table.Column<string>(nullable: true),
                    classes = table.Column<string>(nullable: true),
                    commodity = table.Column<string>(nullable: true),
                    consignee = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    customerId = table.Column<string>(nullable: true),
                    deStuff = table.Column<string>(nullable: true),
                    destinationFreeDays = table.Column<string>(nullable: true),
                    destinationPortStorage = table.Column<string>(nullable: true),
                    eta = table.Column<DateTime>(nullable: false),
                    etd = table.Column<DateTime>(nullable: false),
                    freight = table.Column<string>(nullable: true),
                    hsCode = table.Column<string>(nullable: true),
                    igmDate = table.Column<DateTime>(nullable: false),
                    igmNumber = table.Column<string>(nullable: true),
                    importOperationDate = table.Column<DateTime>(nullable: false),
                    importOperationNumber = table.Column<string>(maxLength: 20, nullable: true),
                    incoterm = table.Column<string>(nullable: true),
                    leoDate = table.Column<DateTime>(nullable: false),
                    lineCode = table.Column<string>(nullable: true),
                    netWeight = table.Column<string>(nullable: true),
                    noOfContainer = table.Column<string>(nullable: true),
                    originFreeDays = table.Column<string>(nullable: true),
                    originPortStorage = table.Column<string>(nullable: true),
                    pod = table.Column<string>(nullable: true),
                    pol = table.Column<string>(nullable: true),
                    remark = table.Column<string>(nullable: true),
                    shipper = table.Column<string>(nullable: true),
                    slotOperator = table.Column<string>(nullable: true),
                    smtpDate = table.Column<DateTime>(nullable: false),
                    smtpNo = table.Column<string>(nullable: true),
                    stuffingType = table.Column<string>(nullable: true),
                    surveyor = table.Column<string>(nullable: true),
                    terminal = table.Column<string>(nullable: true),
                    totalGrossWeight = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    uan = table.Column<string>(nullable: true),
                    vessel = table.Column<string>(nullable: true),
                    voyage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportOperation", x => x.importOperationId);
                    table.ForeignKey(
                        name: "FK_ImportOperation_BillOfLoading_billOfLadingId",
                        column: x => x.billOfLadingId,
                        principalTable: "BillOfLoading",
                        principalColumn: "billOfLoadingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ImportOperation_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ImportOperation_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ImportOperation_Customer_customerId",
                        column: x => x.customerId,
                        principalTable: "Customer",
                        principalColumn: "customerId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ExportOperationLine",
                columns: table => new
                {
                    exportOperationLineId = table.Column<string>(maxLength: 38, nullable: false),
                    actualGrossWeight = table.Column<string>(nullable: true),
                    containerNumberId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    emptyPickupDate = table.Column<DateTime>(nullable: false),
                    emptyPickupYard = table.Column<string>(nullable: true),
                    etaDestination = table.Column<DateTime>(nullable: false),
                    exportOperationId = table.Column<string>(maxLength: 38, nullable: true),
                    form1113 = table.Column<int>(nullable: false),
                    form1113Date = table.Column<DateTime>(nullable: false),
                    gateInTerminal = table.Column<string>(nullable: true),
                    onBoardDate = table.Column<DateTime>(nullable: false),
                    payloadWeight = table.Column<string>(nullable: true),
                    requestDate = table.Column<DateTime>(nullable: false),
                    shiftingConformation = table.Column<int>(nullable: false),
                    ssr = table.Column<string>(nullable: true),
                    ssrRequestDate = table.Column<DateTime>(nullable: false),
                    ssrUpdationDate = table.Column<DateTime>(nullable: false),
                    tareWeight = table.Column<string>(nullable: true),
                    terminalGateInDate = table.Column<DateTime>(nullable: false),
                    updationDate = table.Column<DateTime>(nullable: false),
                    vessel = table.Column<string>(nullable: true),
                    vesselTerminal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExportOperationLine", x => x.exportOperationLineId);
                    table.ForeignKey(
                        name: "FK_ExportOperationLine_ContainerDetails_containerNumberId",
                        column: x => x.containerNumberId,
                        principalTable: "ContainerDetails",
                        principalColumn: "containerDetailsId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ExportOperationLine_ExportOperation_exportOperationId",
                        column: x => x.exportOperationId,
                        principalTable: "ExportOperation",
                        principalColumn: "exportOperationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "InvoicesLine",
                columns: table => new
                {
                    invoicesLineId = table.Column<string>(maxLength: 38, nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    cgst = table.Column<decimal>(nullable: true),
                    costId = table.Column<string>(nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    currencyId = table.Column<string>(nullable: true),
                    igst = table.Column<decimal>(nullable: true),
                    incamount = table.Column<decimal>(nullable: false),
                    incomeId = table.Column<string>(nullable: true),
                    invoicesId = table.Column<string>(maxLength: 38, nullable: true),
                    margin = table.Column<decimal>(nullable: false),
                    rate = table.Column<decimal>(nullable: false),
                    sacCode = table.Column<string>(nullable: true),
                    sgst = table.Column<decimal>(nullable: true),
                    type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoicesLine", x => x.invoicesLineId);
                    table.ForeignKey(
                        name: "FK_InvoicesLine_ChargeHead_costId",
                        column: x => x.costId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InvoicesLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InvoicesLine_ChargeHead_incomeId",
                        column: x => x.incomeId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_InvoicesLine_Invoices_invoicesId",
                        column: x => x.invoicesId,
                        principalTable: "Invoices",
                        principalColumn: "invoicesId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ImportOperationLine",
                columns: table => new
                {
                    importOperationLineId = table.Column<string>(maxLength: 38, nullable: false),
                    contWeight = table.Column<string>(nullable: true),
                    containerNumberId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    doValidityDate = table.Column<DateTime>(nullable: false),
                    emptyReturnDate = table.Column<DateTime>(nullable: false),
                    emptyValidity = table.Column<string>(nullable: true),
                    emptyYardId = table.Column<string>(maxLength: 38, nullable: true),
                    freeDayExpiry = table.Column<string>(nullable: true),
                    ial = table.Column<string>(nullable: true),
                    ialDate = table.Column<DateTime>(nullable: false),
                    icdCode = table.Column<string>(nullable: true),
                    icdEmptyIn = table.Column<string>(nullable: true),
                    icdEmptyOut = table.Column<string>(nullable: true),
                    icdLoadIn = table.Column<string>(nullable: true),
                    icdLoadOut = table.Column<string>(nullable: true),
                    importOperationId = table.Column<string>(maxLength: 38, nullable: true),
                    lineNomination = table.Column<string>(nullable: true),
                    npOfDays = table.Column<string>(nullable: true),
                    partyNomination = table.Column<string>(nullable: true),
                    portOutDate = table.Column<DateTime>(nullable: false),
                    sealNo = table.Column<string>(nullable: true),
                    trpMode = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportOperationLine", x => x.importOperationLineId);
                    table.ForeignKey(
                        name: "FK_ImportOperationLine_ContainerDetails_containerNumberId",
                        column: x => x.containerNumberId,
                        principalTable: "ContainerDetails",
                        principalColumn: "containerDetailsId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ImportOperationLine_Location_emptyYardId",
                        column: x => x.emptyYardId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ImportOperationLine_ImportOperation_importOperationId",
                        column: x => x.importOperationId,
                        principalTable: "ImportOperation",
                        principalColumn: "importOperationId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_branchId",
                table: "AspNetUsers",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoading_bookingId",
                table: "BillOfLoading",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoading_branchId",
                table: "BillOfLoading",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoading_fromlocationId",
                table: "BillOfLoading",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoading_tolocationId",
                table: "BillOfLoading",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoadingLine_billOfLoadingId",
                table: "BillOfLoadingLine",
                column: "billOfLoadingId");

            migrationBuilder.CreateIndex(
                name: "IX_BillOfLoadingLine_containerNumberId",
                table: "BillOfLoadingLine",
                column: "containerNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_bookingSlotId",
                table: "Booking",
                column: "bookingSlotId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_branchId",
                table: "Booking",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_fromlocationId",
                table: "Booking",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_placeofDeliveryId",
                table: "Booking",
                column: "placeofDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_placeofReceiptId",
                table: "Booking",
                column: "placeofReceiptId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_salesInquiryId",
                table: "Booking",
                column: "salesInquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_tolocationId",
                table: "Booking",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingLine_bookingId",
                table: "BookingLine",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingLine_ladenContainerId",
                table: "BookingLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlot_branchId",
                table: "BookingSlot",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlot_fromlocationId",
                table: "BookingSlot",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlot_tolocationId",
                table: "BookingSlot",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlot_vendorId",
                table: "BookingSlot",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlotLine_bookingSlotId",
                table: "BookingSlotLine",
                column: "bookingSlotId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingSlotLine_ladenContainerId",
                table: "BookingSlotLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_ContainerDetails_branchId",
                table: "ContainerDetails",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_ContainerDetails_containerId",
                table: "ContainerDetails",
                column: "containerId");

            migrationBuilder.CreateIndex(
                name: "IX_ContainerDetails_emptyyardLocationId",
                table: "ContainerDetails",
                column: "emptyyardLocationId");

            migrationBuilder.CreateIndex(
                name: "IX_ContainerDetails_locationId",
                table: "ContainerDetails",
                column: "locationId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerLine_customerId",
                table: "CustomerLine",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_ExportOperation_bookingId",
                table: "ExportOperation",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_ExportOperation_branchId",
                table: "ExportOperation",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_ExportOperation_customerId",
                table: "ExportOperation",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_ExportOperationLine_containerNumberId",
                table: "ExportOperationLine",
                column: "containerNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_ExportOperationLine_exportOperationId",
                table: "ExportOperationLine",
                column: "exportOperationId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperation_billOfLadingId",
                table: "ImportOperation",
                column: "billOfLadingId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperation_bookingId",
                table: "ImportOperation",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperation_branchId",
                table: "ImportOperation",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperation_customerId",
                table: "ImportOperation",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperationLine_containerNumberId",
                table: "ImportOperationLine",
                column: "containerNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperationLine_emptyYardId",
                table: "ImportOperationLine",
                column: "emptyYardId");

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperationLine_importOperationId",
                table: "ImportOperationLine",
                column: "importOperationId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPrice_branchId",
                table: "InlandPrice",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPrice_vendorId",
                table: "InlandPrice",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPriceLine_chargeHeadId",
                table: "InlandPriceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPriceLine_currencyId",
                table: "InlandPriceLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPriceLine_emptyContainerId",
                table: "InlandPriceLine",
                column: "emptyContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPriceLine_inlandPriceId",
                table: "InlandPriceLine",
                column: "inlandPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_InlandPriceLine_ladenContainerId",
                table: "InlandPriceLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_bookingId",
                table: "Invoices",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_branchId",
                table: "Invoices",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicesLine_costId",
                table: "InvoicesLine",
                column: "costId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicesLine_currencyId",
                table: "InvoicesLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicesLine_incomeId",
                table: "InvoicesLine",
                column: "incomeId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoicesLine_invoicesId",
                table: "InvoicesLine",
                column: "invoicesId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPrice_branchId",
                table: "LocalPrice",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPrice_locationId",
                table: "LocalPrice",
                column: "locationId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPrice_vendorId",
                table: "LocalPrice",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPriceLine_chargeHeadId",
                table: "LocalPriceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPriceLine_currencyId",
                table: "LocalPriceLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPriceLine_emptyContainerId",
                table: "LocalPriceLine",
                column: "emptyContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPriceLine_ladenContainerId",
                table: "LocalPriceLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalPriceLine_localPriceId",
                table: "LocalPriceLine",
                column: "localPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuote_branchId",
                table: "MultipalRateQuote",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuote_customerId",
                table: "MultipalRateQuote",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_chargeHeadId",
                table: "MultipalRateQuoteLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_currencyId",
                table: "MultipalRateQuoteLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_fromlocationId",
                table: "MultipalRateQuoteLine",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_ladenContainerId",
                table: "MultipalRateQuoteLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_multipalRateQuoteId",
                table: "MultipalRateQuoteLine",
                column: "multipalRateQuoteId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_tolocationId",
                table: "MultipalRateQuoteLine",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipalRateQuoteLine_trpPortId",
                table: "MultipalRateQuoteLine",
                column: "trpPortId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_branchId",
                table: "OceanPrice",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_fromlocationId",
                table: "OceanPrice",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_placeofDeliveryId",
                table: "OceanPrice",
                column: "placeofDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_placeofReceiptId",
                table: "OceanPrice",
                column: "placeofReceiptId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_tolocationId",
                table: "OceanPrice",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_trpPortId",
                table: "OceanPrice",
                column: "trpPortId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPrice_vendorId",
                table: "OceanPrice",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPriceLine_chargeHeadId",
                table: "OceanPriceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPriceLine_currencyId",
                table: "OceanPriceLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPriceLine_emptyContainerId",
                table: "OceanPriceLine",
                column: "emptyContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPriceLine_ladenContainerId",
                table: "OceanPriceLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_OceanPriceLine_oceanPriceId",
                table: "OceanPriceLine",
                column: "oceanPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPrice_branchId",
                table: "PortPrice",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPrice_locationId",
                table: "PortPrice",
                column: "locationId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPrice_vendorId",
                table: "PortPrice",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPriceLine_chargeHeadId",
                table: "PortPriceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPriceLine_currencyId",
                table: "PortPriceLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPriceLine_emptyContainerId",
                table: "PortPriceLine",
                column: "emptyContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPriceLine_ladenContainerId",
                table: "PortPriceLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_PortPriceLine_portPriceId",
                table: "PortPriceLine",
                column: "portPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_PortStoragePriceLine_chargeHeadId",
                table: "PortStoragePriceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_PortStoragePriceLine_portPriceId",
                table: "PortStoragePriceLine",
                column: "portPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_branchId",
                table: "SalesInquiry",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_customerId",
                table: "SalesInquiry",
                column: "customerId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_fromlocationId",
                table: "SalesInquiry",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_placeofDeliveryId",
                table: "SalesInquiry",
                column: "placeofDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_placeofReceiptId",
                table: "SalesInquiry",
                column: "placeofReceiptId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_tolocationId",
                table: "SalesInquiry",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiry_trpPortId",
                table: "SalesInquiry",
                column: "trpPortId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiryLine_chargeHeadId",
                table: "SalesInquiryLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiryLine_currencyId",
                table: "SalesInquiryLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiryLine_ladenContainerId",
                table: "SalesInquiryLine",
                column: "ladenContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesInquiryLine_salesInquiryId",
                table: "SalesInquiryLine",
                column: "salesInquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendor_locationId",
                table: "Vendor",
                column: "locationId");

            migrationBuilder.CreateIndex(
                name: "IX_VendorLine_vendorId",
                table: "VendorLine",
                column: "vendorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BillOfLoadingLine");

            migrationBuilder.DropTable(
                name: "BookingLine");

            migrationBuilder.DropTable(
                name: "BookingSlotLine");

            migrationBuilder.DropTable(
                name: "CustomerLine");

            migrationBuilder.DropTable(
                name: "ExportOperationLine");

            migrationBuilder.DropTable(
                name: "ImportOperationLine");

            migrationBuilder.DropTable(
                name: "InlandPriceLine");

            migrationBuilder.DropTable(
                name: "InvoicesLine");

            migrationBuilder.DropTable(
                name: "LocalPriceLine");

            migrationBuilder.DropTable(
                name: "MultipalRateQuoteLine");

            migrationBuilder.DropTable(
                name: "OceanPriceLine");

            migrationBuilder.DropTable(
                name: "PortPriceLine");

            migrationBuilder.DropTable(
                name: "PortStoragePriceLine");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "SalesInquiryLine");

            migrationBuilder.DropTable(
                name: "VendorLine");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ExportOperation");

            migrationBuilder.DropTable(
                name: "ContainerDetails");

            migrationBuilder.DropTable(
                name: "ImportOperation");

            migrationBuilder.DropTable(
                name: "InlandPrice");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "LocalPrice");

            migrationBuilder.DropTable(
                name: "MultipalRateQuote");

            migrationBuilder.DropTable(
                name: "OceanPrice");

            migrationBuilder.DropTable(
                name: "PortPrice");

            migrationBuilder.DropTable(
                name: "ChargeHead");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "Container");

            migrationBuilder.DropTable(
                name: "BillOfLoading");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropTable(
                name: "BookingSlot");

            migrationBuilder.DropTable(
                name: "SalesInquiry");

            migrationBuilder.DropTable(
                name: "Vendor");

            migrationBuilder.DropTable(
                name: "Branch");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Location");
        }
    }
}
