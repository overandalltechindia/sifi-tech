﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "purchaseInvoiceNumber",
                table: "PurchaseInvoice",
                newName: "purchaseSequenceNumber");

            migrationBuilder.AddColumn<string>(
                name: "blNumber",
                table: "PurchaseInvoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "vendorInvoiceNumber",
                table: "PurchaseInvoice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "blNumber",
                table: "PurchaseInvoice");

            migrationBuilder.DropColumn(
                name: "vendorInvoiceNumber",
                table: "PurchaseInvoice");

            migrationBuilder.RenameColumn(
                name: "purchaseSequenceNumber",
                table: "PurchaseInvoice",
                newName: "purchaseInvoiceNumber");
        }
    }
}
