﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v18 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "inlandPriceId",
                table: "Booking",
                maxLength: 38,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "localPriceId",
                table: "Booking",
                maxLength: 38,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "oceanPriceId",
                table: "Booking",
                maxLength: 38,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "portPriceId",
                table: "Booking",
                maxLength: 38,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Booking_inlandPriceId",
                table: "Booking",
                column: "inlandPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_localPriceId",
                table: "Booking",
                column: "localPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_oceanPriceId",
                table: "Booking",
                column: "oceanPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Booking_portPriceId",
                table: "Booking",
                column: "portPriceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_InlandPrice_inlandPriceId",
                table: "Booking",
                column: "inlandPriceId",
                principalTable: "InlandPrice",
                principalColumn: "inlandPriceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_LocalPrice_localPriceId",
                table: "Booking",
                column: "localPriceId",
                principalTable: "LocalPrice",
                principalColumn: "localPriceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_OceanPrice_oceanPriceId",
                table: "Booking",
                column: "oceanPriceId",
                principalTable: "OceanPrice",
                principalColumn: "oceanPriceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Booking_PortPrice_portPriceId",
                table: "Booking",
                column: "portPriceId",
                principalTable: "PortPrice",
                principalColumn: "portPriceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Booking_InlandPrice_inlandPriceId",
                table: "Booking");

            migrationBuilder.DropForeignKey(
                name: "FK_Booking_LocalPrice_localPriceId",
                table: "Booking");

            migrationBuilder.DropForeignKey(
                name: "FK_Booking_OceanPrice_oceanPriceId",
                table: "Booking");

            migrationBuilder.DropForeignKey(
                name: "FK_Booking_PortPrice_portPriceId",
                table: "Booking");

            migrationBuilder.DropIndex(
                name: "IX_Booking_inlandPriceId",
                table: "Booking");

            migrationBuilder.DropIndex(
                name: "IX_Booking_localPriceId",
                table: "Booking");

            migrationBuilder.DropIndex(
                name: "IX_Booking_oceanPriceId",
                table: "Booking");

            migrationBuilder.DropIndex(
                name: "IX_Booking_portPriceId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "inlandPriceId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "localPriceId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "oceanPriceId",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "portPriceId",
                table: "Booking");
        }
    }
}
