﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "cgstPercent",
                table: "PurchaseInvoiceLine",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "igstPercent",
                table: "PurchaseInvoiceLine",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "sgstPercent",
                table: "PurchaseInvoiceLine",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cgstPercent",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropColumn(
                name: "igstPercent",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropColumn(
                name: "sgstPercent",
                table: "PurchaseInvoiceLine");
        }
    }
}
