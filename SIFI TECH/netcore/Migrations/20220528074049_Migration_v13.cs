﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PurchaseInvoiceLineRole",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "PurchaseInvoiceRole",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PurchaseInvoice",
                columns: table => new
                {
                    purchaseInvoiceId = table.Column<string>(maxLength: 38, nullable: false),
                    HasChild = table.Column<string>(nullable: true),
                    billOfLadingId = table.Column<string>(nullable: true),
                    bookingDate = table.Column<DateTime>(nullable: false),
                    bookingId = table.Column<string>(nullable: true),
                    branchId = table.Column<string>(maxLength: 38, nullable: false),
                    createdAt = table.Column<DateTime>(nullable: false),
                    createdUserEmail = table.Column<string>(nullable: true),
                    fromlocationId = table.Column<string>(maxLength: 38, nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    modifiedUserEmail = table.Column<string>(nullable: true),
                    netBalanceAmount = table.Column<decimal>(nullable: true),
                    noOfContainers = table.Column<string>(nullable: true),
                    paymentAmount = table.Column<decimal>(nullable: true),
                    paymentDate = table.Column<DateTime>(nullable: true),
                    preparedBy = table.Column<string>(nullable: true),
                    purchaseInvoiceDate = table.Column<DateTime>(nullable: false),
                    purchaseInvoiceNumber = table.Column<string>(maxLength: 20, nullable: false),
                    remark = table.Column<string>(maxLength: 128, nullable: true),
                    serviceDate = table.Column<DateTime>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    tdsAmount = table.Column<decimal>(nullable: true),
                    tolocationId = table.Column<string>(maxLength: 38, nullable: false),
                    totalAmount = table.Column<decimal>(nullable: true),
                    totalGSTAmount = table.Column<decimal>(nullable: true),
                    totalInvoiceAmount = table.Column<decimal>(nullable: true),
                    utrNumber = table.Column<string>(nullable: true),
                    vendorId = table.Column<string>(nullable: false),
                    vendorServiceNumber = table.Column<string>(maxLength: 50, nullable: false),
                    vendorType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseInvoice", x => x.purchaseInvoiceId);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_BillOfLoading_billOfLadingId",
                        column: x => x.billOfLadingId,
                        principalTable: "BillOfLoading",
                        principalColumn: "billOfLoadingId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_Booking_bookingId",
                        column: x => x.bookingId,
                        principalTable: "Booking",
                        principalColumn: "bookingId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_Branch_branchId",
                        column: x => x.branchId,
                        principalTable: "Branch",
                        principalColumn: "branchId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_Location_fromlocationId",
                        column: x => x.fromlocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_Location_tolocationId",
                        column: x => x.tolocationId,
                        principalTable: "Location",
                        principalColumn: "locationId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoice_Vendor_vendorId",
                        column: x => x.vendorId,
                        principalTable: "Vendor",
                        principalColumn: "vendorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseInvoiceLine",
                columns: table => new
                {
                    purchaseInvoiceLineId = table.Column<string>(maxLength: 38, nullable: false),
                    cgst = table.Column<decimal>(nullable: false),
                    chargeHeadId = table.Column<string>(maxLength: 38, nullable: true),
                    createdAt = table.Column<DateTime>(nullable: false),
                    createdUserEmail = table.Column<string>(nullable: true),
                    currencyId = table.Column<string>(maxLength: 38, nullable: true),
                    igst = table.Column<decimal>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false),
                    modifiedUserEmail = table.Column<string>(nullable: true),
                    purchaseInvoiceId = table.Column<string>(maxLength: 38, nullable: true),
                    quantity = table.Column<int>(nullable: false),
                    sac = table.Column<string>(nullable: true),
                    sgst = table.Column<decimal>(nullable: false),
                    totalAmountWithGST = table.Column<decimal>(nullable: false),
                    totalAmountWithoutGST = table.Column<decimal>(nullable: false),
                    totalGSTAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseInvoiceLine", x => x.purchaseInvoiceLineId);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoiceLine_ChargeHead_chargeHeadId",
                        column: x => x.chargeHeadId,
                        principalTable: "ChargeHead",
                        principalColumn: "chargeHeadId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoiceLine_Currency_currencyId",
                        column: x => x.currencyId,
                        principalTable: "Currency",
                        principalColumn: "currencyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseInvoiceLine_PurchaseInvoice_purchaseInvoiceId",
                        column: x => x.purchaseInvoiceId,
                        principalTable: "PurchaseInvoice",
                        principalColumn: "purchaseInvoiceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_billOfLadingId",
                table: "PurchaseInvoice",
                column: "billOfLadingId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_bookingId",
                table: "PurchaseInvoice",
                column: "bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_branchId",
                table: "PurchaseInvoice",
                column: "branchId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_fromlocationId",
                table: "PurchaseInvoice",
                column: "fromlocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_tolocationId",
                table: "PurchaseInvoice",
                column: "tolocationId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoice_vendorId",
                table: "PurchaseInvoice",
                column: "vendorId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoiceLine_chargeHeadId",
                table: "PurchaseInvoiceLine",
                column: "chargeHeadId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoiceLine_currencyId",
                table: "PurchaseInvoiceLine",
                column: "currencyId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoiceLine_purchaseInvoiceId",
                table: "PurchaseInvoiceLine",
                column: "purchaseInvoiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PurchaseInvoiceLine");

            migrationBuilder.DropTable(
                name: "PurchaseInvoice");

            migrationBuilder.DropColumn(
                name: "PurchaseInvoiceLineRole",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PurchaseInvoiceRole",
                table: "AspNetUsers");
        }
    }
}
