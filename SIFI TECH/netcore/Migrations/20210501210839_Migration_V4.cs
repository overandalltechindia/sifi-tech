﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_V4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "thirdPartyBLN",
                table: "ImportOperation",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isActive",
                table: "ContainerDetails",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "thirdPartyBLN",
                table: "ImportOperation");

            migrationBuilder.DropColumn(
                name: "isActive",
                table: "ContainerDetails");
        }
    }
}
