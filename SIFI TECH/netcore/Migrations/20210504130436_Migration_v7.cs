﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "VendorLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "VendorLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Vendor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Vendor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "SalesInquiryLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "SalesInquiryLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "SalesInquiry",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "SalesInquiry",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "PortStoragePriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "PortStoragePriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "PortPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "PortPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "PortPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "PortPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "OceanPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "OceanPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "OceanPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "OceanPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "MultipalRateQuoteLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "MultipalRateQuoteLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "MultipalRateQuote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "MultipalRateQuote",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "LocalPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "LocalPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "LocalPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "LocalPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "InvoicesLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "InvoicesLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Invoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Invoices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "InlandPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "InlandPriceLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "InlandPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "InlandPrice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ImportOperationLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ImportOperationLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ImportOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ImportOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ExportOperationLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ExportOperationLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ExportOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ExportOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "CustomerLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "CustomerLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Currency",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ContainerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ContainerDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Container",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Container",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "ChargeHead",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "ChargeHead",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "BookingSlotLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "BookingSlotLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "BookingSlot",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "BookingSlot",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "BookingLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "BookingLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "BillOfLoadingLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "BillOfLoadingLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserEmail",
                table: "BillOfLoading",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "modifiedUserEmail",
                table: "BillOfLoading",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "VendorLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "VendorLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Vendor");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Vendor");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "SalesInquiryLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "SalesInquiryLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "SalesInquiry");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "SalesInquiry");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "PortStoragePriceLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "PortStoragePriceLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "PortPriceLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "PortPriceLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "PortPrice");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "PortPrice");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "OceanPriceLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "OceanPriceLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "OceanPrice");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "OceanPrice");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "MultipalRateQuoteLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "MultipalRateQuoteLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "MultipalRateQuote");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "MultipalRateQuote");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "LocalPriceLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "LocalPriceLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "LocalPrice");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "LocalPrice");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "InvoicesLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "InvoicesLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "InlandPriceLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "InlandPriceLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "InlandPrice");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "InlandPrice");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ImportOperationLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ImportOperationLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ImportOperation");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ImportOperation");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ExportOperationLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ExportOperationLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ExportOperation");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ExportOperation");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "CustomerLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "CustomerLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ContainerDetails");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ContainerDetails");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Container");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Container");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "ChargeHead");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "ChargeHead");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "BookingSlotLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "BookingSlotLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "BookingSlot");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "BookingSlot");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "BookingLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "BookingLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "BillOfLoadingLine");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "BillOfLoadingLine");

            migrationBuilder.DropColumn(
                name: "createdUserEmail",
                table: "BillOfLoading");

            migrationBuilder.DropColumn(
                name: "modifiedUserEmail",
                table: "BillOfLoading");
        }
    }
}
