﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "vendorTerminalId",
                table: "ImportOperation",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ImportOperation_vendorTerminalId",
                table: "ImportOperation",
                column: "vendorTerminalId");

            migrationBuilder.AddForeignKey(
                name: "FK_ImportOperation_Vendor_vendorTerminalId",
                table: "ImportOperation",
                column: "vendorTerminalId",
                principalTable: "Vendor",
                principalColumn: "vendorId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImportOperation_Vendor_vendorTerminalId",
                table: "ImportOperation");

            migrationBuilder.DropIndex(
                name: "IX_ImportOperation_vendorTerminalId",
                table: "ImportOperation");

            migrationBuilder.DropColumn(
                name: "vendorTerminalId",
                table: "ImportOperation");
        }
    }
}
