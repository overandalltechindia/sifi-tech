﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "VendorLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Vendor",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "SalesInquiryLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "SalesInquiry",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Product",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "PortStoragePriceLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "PortPriceLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "PortPrice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "OceanPriceLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "OceanPrice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "MultipalRateQuoteLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "MultipalRateQuote",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Location",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "LocalPriceLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "LocalPrice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "InvoicesLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Invoices",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "InlandPriceLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "InlandPrice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ImportOperationLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ImportOperation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ExportOperationLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ExportOperation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "CustomerLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Customer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Currency",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ContainerDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Container",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "ChargeHead",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Branch",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "BookingSlotLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "BookingSlot",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "BookingLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Booking",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "BillOfLoadingLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "BillOfLoading",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "VendorLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Vendor");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "SalesInquiryLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "SalesInquiry");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "PortStoragePriceLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "PortPriceLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "PortPrice");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "OceanPriceLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "OceanPrice");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "MultipalRateQuoteLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "MultipalRateQuote");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "LocalPriceLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "LocalPrice");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "InvoicesLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Invoices");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "InlandPriceLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "InlandPrice");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ImportOperationLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ImportOperation");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ExportOperationLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ExportOperation");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "CustomerLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ContainerDetails");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Container");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "ChargeHead");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "BookingSlotLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "BookingSlot");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "BookingLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "BillOfLoadingLine");

            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "BillOfLoading");
        }
    }
}
