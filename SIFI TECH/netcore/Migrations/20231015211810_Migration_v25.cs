﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "principal",
                table: "ExportOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "principal",
                table: "Booking",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "principal",
                table: "ExportOperation");

            migrationBuilder.DropColumn(
                name: "principal",
                table: "Booking");
        }
    }
}
