﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "containerEmptyLaden",
                table: "PurchaseInvoiceLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "emptyContainerId",
                table: "PurchaseInvoiceLine",
                maxLength: 38,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ladenContainerId",
                table: "PurchaseInvoiceLine",
                maxLength: 38,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoiceLine_emptyContainerId",
                table: "PurchaseInvoiceLine",
                column: "emptyContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseInvoiceLine_ladenContainerId",
                table: "PurchaseInvoiceLine",
                column: "ladenContainerId");

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseInvoiceLine_Container_emptyContainerId",
                table: "PurchaseInvoiceLine",
                column: "emptyContainerId",
                principalTable: "Container",
                principalColumn: "containerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PurchaseInvoiceLine_Container_ladenContainerId",
                table: "PurchaseInvoiceLine",
                column: "ladenContainerId",
                principalTable: "Container",
                principalColumn: "containerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseInvoiceLine_Container_emptyContainerId",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropForeignKey(
                name: "FK_PurchaseInvoiceLine_Container_ladenContainerId",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseInvoiceLine_emptyContainerId",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropIndex(
                name: "IX_PurchaseInvoiceLine_ladenContainerId",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropColumn(
                name: "containerEmptyLaden",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropColumn(
                name: "emptyContainerId",
                table: "PurchaseInvoiceLine");

            migrationBuilder.DropColumn(
                name: "ladenContainerId",
                table: "PurchaseInvoiceLine");
        }
    }
}
