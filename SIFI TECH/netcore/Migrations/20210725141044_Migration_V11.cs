﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_V11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address2",
                table: "Branch",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Contact",
                table: "Branch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Branch",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address2",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "Contact",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Branch");
        }
    }
}
