﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "bankRemark",
                table: "PurchaseInvoice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                table: "Invoices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "bankRemark",
                table: "PurchaseInvoice");

            migrationBuilder.DropColumn(
                name: "remark",
                table: "Invoices");
        }
    }
}
