﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_v21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "linePricingType",
                table: "PurchaseInvoiceLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "emptyPrice",
                table: "LocalPriceLine",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "emptyPrice",
                table: "InlandPriceLine",
                nullable: true,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "linePricingType",
                table: "PurchaseInvoiceLine");

            migrationBuilder.AlterColumn<decimal>(
                name: "emptyPrice",
                table: "LocalPriceLine",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "emptyPrice",
                table: "InlandPriceLine",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
