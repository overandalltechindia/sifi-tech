﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_V3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "cgstPercent",
                table: "InvoicesLine",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "igstPercent",
                table: "InvoicesLine",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "sgstPercent",
                table: "InvoicesLine",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "noOfIdleDays",
                table: "ContainerDetails",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cgstPercent",
                table: "InvoicesLine");

            migrationBuilder.DropColumn(
                name: "igstPercent",
                table: "InvoicesLine");

            migrationBuilder.DropColumn(
                name: "sgstPercent",
                table: "InvoicesLine");

            migrationBuilder.AlterColumn<int>(
                name: "noOfIdleDays",
                table: "ContainerDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
