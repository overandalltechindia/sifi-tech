﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace netcore.Migrations
{
    public partial class Migration_V2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "emptyPickupLocation",
                table: "Booking");

            migrationBuilder.AddColumn<string>(
                name: "emptyYardLocationId",
                table: "BookingLine",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BookingLine_emptyYardLocationId",
                table: "BookingLine",
                column: "emptyYardLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_BookingLine_Location_emptyYardLocationId",
                table: "BookingLine",
                column: "emptyYardLocationId",
                principalTable: "Location",
                principalColumn: "locationId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BookingLine_Location_emptyYardLocationId",
                table: "BookingLine");

            migrationBuilder.DropIndex(
                name: "IX_BookingLine_emptyYardLocationId",
                table: "BookingLine");

            migrationBuilder.DropColumn(
                name: "emptyYardLocationId",
                table: "BookingLine");

            migrationBuilder.AddColumn<string>(
                name: "emptyPickupLocation",
                table: "Booking",
                nullable: true);
        }
    }
}
