﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using netcore.Models;
using netcore.Models.Invent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Services
{
    public interface INetcoreService
    {
        Task SendEmailBySendGridAsync(string apiKey, string fromEmail, string fromFullName, string subject, string message, string email);

        Task<bool> IsAccountActivatedAsync(string email, UserManager<ApplicationUser> userManager);

        Task SendEmailByGmailAsync(string fromEmail,
            string fromFullName,
            string subject,
            string messageBody,
            string toEmail,
            string toFullName,
            string smtpUser,
            string smtpPassword,
            string smtpHost,
            int smtpPort,
            bool smtpSSL);

        Task<string> UploadFile(List<IFormFile> files, IHostingEnvironment env);

        Task UpdateRoles(ApplicationUser appUser, ApplicationUser currentUserLogin);

        IEnumerable<SelectListItem> GetUserBranches(string userName);

        string GetUserBranchId(string userName);

        string DecimalToWords(decimal number);

        //Task InitCRM();

        Task CreateDefaultSuperAdmin();
        Task CreateDefaultApplicationAdmin();
        Task InitBranches();



        //VMStock GetStockByProductAndWarehouse(string productId, string warehouseId);

        //List<VMStock> GetStockPerWarehouse();


        //VMStock GetVendorDetailsPerProduct(string productId, string vendorId, string purchaseOrderId);
        //VMStock GetCustomerDetailsPerProduct(string productId, string vendorId, string purchaseOrderId);

        //List<VMStock> GetVendorsPerProduct();
        //List<VMStock> GetCustomerPerProduct();


    }
}
