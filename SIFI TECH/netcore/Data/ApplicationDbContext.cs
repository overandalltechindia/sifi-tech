﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using netcore.Models;
using netcore.Models.Invent;

namespace netcore.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

        }

        public DbSet<netcore.Models.ApplicationUser> ApplicationUser { get; set; }

        public DbSet<netcore.Models.Invent.Branch> Branch { get; set; }

        public DbSet<netcore.Models.Invent.Product> Product { get; set; }
        public DbSet<netcore.Models.Invent.ChargeHead> ChargeHead { get; set; }
        public DbSet<netcore.Models.Invent.Container> Container { get; set; }
        public DbSet<netcore.Models.Invent.Currency> Currency { get; set; }
        public DbSet<netcore.Models.Invent.Location> Location { get; set; }

        public DbSet<netcore.Models.Invent.Vendor> Vendor { get; set; }

        public DbSet<netcore.Models.Invent.VendorLine> VendorLine { get; set; }

        public DbSet<netcore.Models.Invent.OceanPrice> OceanPrice { get; set; }

        public DbSet<netcore.Models.Invent.OceanPriceLine> OceanPriceLine { get; set; }

        public DbSet<netcore.Models.Invent.InlandPrice> InlandPrice { get; set; }

        public DbSet<netcore.Models.Invent.InlandPriceLine> InlandPriceLine { get; set; }
        public DbSet<netcore.Models.Invent.PortPrice> PortPrice { get; set; }
        public DbSet<netcore.Models.Invent.PortPriceLine> PortPriceLine { get; set; }
        public DbSet<netcore.Models.Invent.PortStoragePriceLine> PortStoragePriceLine { get; set; }

        public DbSet<netcore.Models.Invent.LocalPrice> LocalPrice { get; set; }

        public DbSet<netcore.Models.Invent.LocalPriceLine> LocalPriceLine { get; set; }

        public DbSet<netcore.Models.Invent.SalesInquiry> SalesInquiry { get; set; }

        public DbSet<netcore.Models.Invent.SalesInquiryLine> SalesInquiryLine { get; set; }
        public DbSet<netcore.Models.Invent.MultipalRateQuote> MultipalRateQuote { get; set; }
        public DbSet<netcore.Models.Invent.MultipalRateQuoteLine> MultipalRateQuoteLine { get; set; }
        public DbSet<netcore.Models.Invent.BookingSlot> BookingSlot { get; set; }
        public DbSet<netcore.Models.Invent.BookingSlotLine> BookingSlotLine { get; set; }
        public DbSet<netcore.Models.Invent.Booking> Booking { get; set; }
        public DbSet<netcore.Models.Invent.BookingLine> BookingLine { get; set; }
        public DbSet<netcore.Models.Invent.BillOfLoading> BillOfLoading { get; set; }
        public DbSet<netcore.Models.Invent.BillOfLoadingLine> BillOfLoadingLine { get; set; }
        public DbSet<netcore.Models.Invent.ExportOperation> ExportOperation { get; set; }
        public DbSet<netcore.Models.Invent.ExportOperationLine> ExportOperationLine { get; set; }

        public DbSet<netcore.Models.Invent.ImportOperation> ImportOperation { get; set; }
        public DbSet<netcore.Models.Invent.ImportOperationLine> ImportOperationLine { get; set; }

        public DbSet<netcore.Models.Invent.Invoices> Invoices { get; set; }
        public DbSet<netcore.Models.Invent.InvoicesLine> InvoicesLine { get; set; }
        public DbSet<netcore.Models.Invent.ContainerDetails> ContainerDetails { get; set; }

        public DbSet<netcore.Models.Invent.Customer> Customer { get; set; }

        public DbSet<netcore.Models.Invent.CustomerLine> CustomerLine { get; set; }


        public DbSet<netcore.Models.Invent.PurchaseInvoice> PurchaseInvoice { get; set; }
        public DbSet<netcore.Models.Invent.PurchaseInvoiceLine> PurchaseInvoiceLine { get; set; }


        //public DbSet<netcore.Models.Invent.SalesOrder> SalesOrder { get; set; }

        //public DbSet<netcore.Models.Invent.SalesOrderLine> SalesOrderLine { get; set; }


        //public DbSet<netcore.Models.Invent.Warehouse> Warehouse { get; set; }

        //public DbSet<netcore.Models.Invent.PurchaseOrder> PurchaseOrder { get; set; }

        //public DbSet<netcore.Models.Invent.PurchaseOrderLine> PurchaseOrderLine { get; set; }

        //public DbSet<netcore.Models.Invent.Shipment> Shipment { get; set; }

        //public DbSet<netcore.Models.Invent.ShipmentLine> ShipmentLine { get; set; }

        //public DbSet<netcore.Models.Invent.Receiving> Receiving { get; set; }

        //public DbSet<netcore.Models.Invent.ReceivingLine> ReceivingLine { get; set; }

        //public DbSet<netcore.Models.Invent.TransferOrder> TransferOrder { get; set; }

        //public DbSet<netcore.Models.Invent.TransferOrderLine> TransferOrderLine { get; set; }

        //public DbSet<netcore.Models.Invent.TransferOut> TransferOut { get; set; }

        //public DbSet<netcore.Models.Invent.TransferOutLine> TransferOutLine { get; set; }

        //public DbSet<netcore.Models.Invent.TransferIn> TransferIn { get; set; }

        //public DbSet<netcore.Models.Invent.TransferInLine> TransferInLine { get; set; }

        //public DbSet<netcore.Models.Crm.Rating> Rating { get; set; }

        //public DbSet<netcore.Models.Crm.Activity> Activity { get; set; }

        //public DbSet<netcore.Models.Crm.Channel> Channel { get; set; }

        //public DbSet<netcore.Models.Crm.Stage> Stage { get; set; }

        //public DbSet<netcore.Models.Crm.AccountExecutive> AccountExecutive { get; set; }

        //public DbSet<netcore.Models.Crm.Lead> Lead { get; set; }

        //public DbSet<netcore.Models.Crm.LeadLine> LeadLine { get; set; }

        //public DbSet<netcore.Models.Crm.Opportunity> Opportunity { get; set; }

        //public DbSet<netcore.Models.Crm.OpportunityLine> OpportunityLine { get; set; }

    }
}
