﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Purchase Invoice")]
    public class PurchaseInvoice : INetcoreMasterChild
    {
        public PurchaseInvoice()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            //this.purchaseInvoiceNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-PIN";
            this.purchaseInvoiceDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.bookingDate = DateTime.Now.Date;
            this.paymentDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Purchase Invoice Id")]
        public string purchaseInvoiceId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Sequence Number")]
        public string purchaseSequenceNumber { get; set; }

        [Display(Name = "Vendor Invoice Number")]
        public string vendorInvoiceNumber { get; set; }

        [Display(Name = "Purchase Invoice Date")]
        [Required]
        public DateTime purchaseInvoiceDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [Required]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor Type")]
        public VendorType vendorType { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }

        //[StringLength(50)]
        //[Required]
        //[Display(Name = "Vendor Service Number")]
        //public string vendorServiceNumber { get; set; }


        [StringLength(38)]
        [Required]
        [Display(Name = "Port of Loading Id")]
        public string fromlocationId { get; set; }

        [Display(Name = "Port of Loading")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port of Destination Id")]
        public string tolocationId { get; set; }

        [Display(Name = "Port of Destination")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }

        [Display(Name = "No. of Containers")]
        public string noOfContainers { get; set; }

        [Display(Name = "Booking Date")]
        [Required]
        public DateTime bookingDate { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking No.")]
        public Booking booking { get; set; }

        [Display(Name = "Bill of Lading Id")]
        public string billOfLadingId { get; set; }

        [Display(Name = "BL No.")]
        public BillOfLoading billOfLading { get; set; }

        [Display(Name = "BL Number")]
        public string blNumber { get; set; }



        //Invoice Details

        [Display(Name = "UTR Numbers")]
        public string utrNumber { get; set; }

        [Display(Name = "Sub Total Amount")]
        public decimal? totalAmount { get; set; }

        [Display(Name = "Total GST Amount")]
        public decimal? totalGSTAmount { get; set; }

        [Display(Name = "Total Invoice Amount")]
        public decimal? totalInvoiceAmount { get; set; }

        [Display(Name = "Net Balance Amount")]
        public decimal? netBalanceAmount { get; set; }

        [Display(Name = "TDS Amount")]
        public decimal? tdsAmount { get; set; }

        [Display(Name = "Payment Amount")]
        public decimal? paymentAmount { get; set; }

        [Display(Name = "Payment Date")]
        public DateTime? paymentDate { get; set; }




        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }

        [Display(Name = "Status")]
        public InvoiceStatus status { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Bank & Payment Remarks")]
        public string bankRemark { get; set; }


        [Display(Name = "Purchase Invoice Lines")]
        public List<PurchaseInvoiceLine> purchaseInvoiceLine { get; set; } = new List<PurchaseInvoiceLine>();
    }
}
