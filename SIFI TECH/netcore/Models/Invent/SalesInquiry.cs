﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Sales Inquiry")]
    public class SalesInquiry : INetcoreMasterChild
    {
        public SalesInquiry()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.salesInquiryNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-SIN";
            this.salesInquiryDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Sales Inquiry Id")]
        public string salesInquiryId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Sales Inquiry Number")]
        public string salesInquiryNumber { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime salesInquiryDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [Required]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }


        [Display(Name = "Shipper")]
        public string shipper { get; set; }

   

        [Display(Name = "Sale Person")]
        public string salePerson { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port of Loading Id")]
        public string fromlocationId { get; set; }

        [Display(Name = "Port of Loading")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port of Destination Id")]
        public string tolocationId { get; set; }

        [Display(Name = "Port of Destination")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }



        [Display(Name = "Place of Receipt")]
        public string placeofReceiptId { get; set; }

        [Display(Name = "Place of Receipt")]
        [ForeignKey("placeofReceiptId")]
        public Location placeofReceipt { get; set; }



        [Display(Name = "Place of Delivery")]
        public string placeofDeliveryId { get; set; }

        [Display(Name = "Place of Delivery")]
        [ForeignKey("placeofDeliveryId")]
        public Location placeofDelivery { get; set; }




        [StringLength(30)]
        [Display(Name = "POL Country")]
        public string polCountry { get; set; }

        [StringLength(30)]
        [Display(Name = "POD Country")]
        public string podCountry { get; set; }

        [Display(Name = "Precarriage")]
        public Precarriage precarriage { get; set; }

        [Display(Name = "ON carriage")]
        public Precarriage onCarriage { get; set; }

        [Display(Name = "VIA")]
        public VIA via { get; set; }

        [StringLength(38)]
        [Display(Name = "TRP Port")]
        public string trpPortId { get; set; }

        [Display(Name = "TRP Port")]
        [ForeignKey("trpPortId")]
        public Location trpPort { get; set; }


        [Display(Name = "MODE")]
        public MODE mode { get; set; }

        [Display(Name = "POL Term")]
        public POLPOD polTerm { get; set; }

        [Display(Name = "POD Term")]
        public POLPOD podTerm { get; set; }

        [Display(Name = "Freight Term")]
        public FreightTerm freightTerm { get; set; }

        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }


        [Display(Name = "Status")]
        public Status status { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }


        [Display(Name = "Sales Inquiry Lines")]
        public List<SalesInquiryLine> salesInquiryLine { get; set; } = new List<SalesInquiryLine>();
    }
}
