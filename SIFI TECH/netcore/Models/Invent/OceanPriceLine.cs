﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class OceanPriceLine : INetcoreBasic
    {
        public OceanPriceLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Ocean Price Line Id")]
        [Key]
        public string oceanPriceLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Ocean Price Id")]
        public string oceanPriceId { get; set; }

        [Display(Name = "Ocean Price")]
        public OceanPrice oceanPrice { get; set; }

        [StringLength(38)]
        [Display(Name = "Charge Head")]
        public string chargeHeadId { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        //[Display(Name = "Laden")]
        //[JsonConverter(typeof(StringEnumConverter))]
        //public Laden laden { get; set; }

        [StringLength(38)]
        [Display(Name = "Laden Container")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Laden")]
        [ForeignKey("ladenContainerId")]
        public Container containerLaden { get; set; }

        [Display(Name = "Price")]
        public decimal ladenPrice { get; set; }

        //[Display(Name = "Empty")]
        //[JsonConverter(typeof(StringEnumConverter))]
        //public Empty empty { get; set; }

        [StringLength(38)]
        [Display(Name = "Empty Container")]
        public string emptyContainerId { get; set; }

        [Display(Name = "Container Empty")]
        [ForeignKey("emptyContainerId")]
        public Container containerEmpty { get; set; }

        [Display(Name = "Price")]
        public decimal? emptyPrice { get; set; }

        [Display(Name = "Part")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Part part { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency")]
        public string currencyId { get; set; }

        [Display(Name = "Currency")]
        public Currency currency { get; set; }

    }
}
