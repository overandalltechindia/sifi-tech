﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Import Operation")]
    public class ImportOperation : INetcoreMasterChild
    {
        public ImportOperation()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.importOperationNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-IMP";
            this.importOperationDate = DateTime.Now.Date;
            this.bookingDate = DateTime.Now.Date;
            this.igmDate = DateTime.Now.Date;
            this.leoDate = DateTime.Now.Date;
            this.etd = DateTime.Now.Date;
            this.eta = DateTime.Now.Date;
            this.leoDate = DateTime.Now.Date;
            this.billOfLadingDate = DateTime.Now.Date;
            this.smtpDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Import Operation Id")]
        public string importOperationId { get; set; }

        [StringLength(20)]
        [Display(Name = "Import Operation Number")]
        public string importOperationNumber { get; set; }

        [Display(Name = "Import Operation Date")]
        public DateTime importOperationDate { get; set; }

        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        //[Display(Name = "Vendor Id")]
        //public string vendorId { get; set; }

        //[Display(Name = "Vendor")]
        //public Vendor vendor { get; set; }

        [Display(Name = "Shipper")]
        public string shipper { get; set; }

        [Display(Name = "Clearing Party Details")]
        public string clearingpartydetails { get; set; }

        [Display(Name = "Consignee")]
        public string consignee { get; set; }

        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime bookingDate { get; set; }

        [Display(Name = "Bill Of Lading Id")]
        public string billOfLadingId { get; set; }

        [Display(Name = "Bill Of Loading")]
        public BillOfLoading billOfLading { get; set; }

        [Display(Name = "Third Party BLN")]
        public string thirdPartyBLN { get; set; }

        [Display(Name = "Bill Of Lading Date")]
        public DateTime billOfLadingDate { get; set; }

        [Display(Name = "Origin Free Days")]
        public string originFreeDays { get; set; }

        [Display(Name = "Origin Port Storage")]
        public string originPortStorage { get; set; }

        [Display(Name = "Destination Free Days")]
        public string destinationFreeDays { get; set; }

        [Display(Name = "Destination Port Storage")]
        public string destinationPortStorage { get; set; }

        [Display(Name = "Commodity")]
        public string commodity { get; set; }

        [Display(Name = "Category")]
        public string category { get; set; }

        [Display(Name = "UAN")]
        public string uan { get; set; }

        [Display(Name = "Class")]
        public string classes { get; set; }

        [Display(Name = "DE Stuff")]
        public string deStuff { get; set; }

        [Display(Name = "Stuffing Type")]
        public string stuffingType { get; set; }

        [Display(Name = "IGM Number")]
        public string igmNumber { get; set; }

        [Display(Name = "IGM Date")]
        public DateTime igmDate { get; set; }

        [Display(Name = "Freight")]
        public string freight { get; set; }

        [Display(Name = "POL")]
        public string pol { get; set; }

        [Display(Name = "POD")]
        public string pod { get; set; }

        [Display(Name = "Vessel")]
        public string vessel { get; set; }


        [Display(Name = "Voyage")]
        public string voyage { get; set; }

        [Display(Name = "Terminal")]
        public string terminal { get; set; }

        [Display(Name = "CFS/TERMINAL/ICD")]
        public string vendorTerminalId { get; set; }

        [Display(Name = "CFS/TERMINAL/ICD")]
        public Vendor vendorTerminal { get; set; }

        [Display(Name = "Line Code")]
        public string lineCode { get; set; }

        [Display(Name = "Slot Operator")]
        public string slotOperator { get; set; }

        [Display(Name = "No of Container")]
        public string noOfContainer { get; set; }

        [Display(Name = "Type")]
        public string type { get; set; }


        [Display(Name = "Surveyor ")]
        public string surveyor { get; set; }

        [Display(Name = "LEO Date")]
        public DateTime leoDate { get; set; }

        [Display(Name = "ETD")]
        public DateTime etd { get; set; }

        [Display(Name = "ETA")]
        public DateTime eta { get; set; }

        [Display(Name = "Total Gross Weigt")]
        public string totalGrossWeight { get; set; }

        [Display(Name = "Net Weight")]
        public string netWeight { get; set; }

        [Display(Name = "HS Code")]
        public string hsCode { get; set; }


        [Display(Name = "SMTP No")]
        public string smtpNo { get; set; }

        [Display(Name = "SMTP Date")]
        public DateTime smtpDate { get; set; }

        [Display(Name = "Incoterm")]
        public string incoterm { get; set; }

        [Display(Name = "Remark")]
        public string remark { get; set; }


        [Display(Name = "Import Operation Lines")]
        public List<ImportOperationLine> importOperationLine { get; set; } = new List<ImportOperationLine>();
    }
}
