﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ContainerType
    {
        [Display(Name = "20")]
        Twenty = 1,
        [Display(Name = "40")]
        Forty = 2,
    }
}
