﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Bill Of Loading")]
    public class BillOfLoading : INetcoreMasterChild
    {
        public BillOfLoading()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.billOfLoadingNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-BLN";

        }

        [StringLength(38)]
        [Display(Name = "Bill Of Loading Id")]
        public string billOfLoadingId { get; set; }

        [StringLength(20)]
        [Display(Name = "BL Number")]
        public string billOfLoadingNumber { get; set; }

        [StringLength(38)]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }


        [Display(Name = "Broker/Forwarder Name")]
        public string brokerName { get; set; }

        [Display(Name = "Document Type")]
        public DocumentType documentType { get; set; }

        [Display(Name = "Shipper (Add Semi Colon to Break Line)")]
        public string shipper { get; set; }

        [Display(Name = "Booking Number")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [Display(Name = "Consignee (Add Semi Colon to Break Line)")]
        public string consignee { get; set; }

        [Display(Name = "Destination Agent Reference (Add Semi Colon to Break Line)")]
        public string destinationAgentReference { get; set; }

        [Display(Name = "Place Of Receipt")]
        public string placeOfReceipt { get; set; }

        [Display(Name = "Place Of Delivery")]
        public string placeOfDelivery { get; set; }

        [Display(Name = "Vessel Name")]
        public string vesselName { get; set; }

        [Display(Name = "Voyage Number")]
        public string voyageNumber { get; set; }


        [StringLength(38)]
        [Display(Name = "Port Of Loading")]
        public string fromlocationId { get; set; }

        [Display(Name = "Port Of Loading")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Display(Name = "Port of Destination")]
        public string tolocationId { get; set; }

        [Display(Name = "Port of Destination")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }

        [Display(Name = "Movement Type")]
        public MovementType movementType { get; set; }

        [Display(Name = "Total Number Of Containers")]
        public string totalNumberContainers { get; set; }

        [Display(Name = "Total Number Of Cargoes")]
        public string totalNumberCargoes { get; set; }

        [Display(Name = "Shipper TAX Id")]
        public string shipperTaxId { get; set; }

        [Display(Name = "Consignee TAX Id")]
        public string consigneeTaxId { get; set; }

        [Display(Name = "Notify TAX Id")]
        public string notifyTaxId { get; set; }


        [Display(Name = "Freight Payable At")]
        public FreightPayableAt freightPayableAt { get; set; }

        [Display(Name = "Invoice Reference")]
        public string invoiceReference { get; set; }


        [Display(Name = "Set Charges")]
        public SetCharges setCharges { get; set; }


        [Display(Name = "Origin Haulage Charges")]
        public Charges originHaulage { get; set; }

        [Display(Name = "Origin Port Charges")]
        public Charges originPort { get; set; }

        [Display(Name = "Sea Freight + Additionals")]
        public Charges seaFreaightAdditionals { get; set; }

        [Display(Name = "Destination Haulage Charges")]
        public Charges destinationHaulage { get; set; }

        [Display(Name = "Destination Port Charges")]
        public Charges destinationPort { get; set; }

        [Display(Name = "BL Type")]
        public BLType bLType { get; set; }

        [Display(Name = "Original BS/L")]
        public string originalBl { get; set; }

        [Display(Name = "No. of Copies")]
        public string coppies { get; set; }

        [Display(Name = "Remark")]
        public string remark { get; set; }


        [Display(Name = "Notify To Party")]
        public string notifyToparty { get; set; }


        [Display(Name = "Bill Of Lading Line")]
        public List<BillOfLoadingLine> billOfLoadingLine { get; set; } = new List<BillOfLoadingLine>();
    }
}
