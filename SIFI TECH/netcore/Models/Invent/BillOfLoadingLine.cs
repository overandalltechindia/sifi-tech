﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BillOfLoadingLine : INetcoreBasic
    {
        public BillOfLoadingLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.shippingBillDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Bill Of Loading Line Id")]
        [Key]
        public string billOfLoadingLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Bill Of Loading Id")]
        public string billOfLoadingId { get; set; }

        [Display(Name = "Bill Of Loading")]
        public BillOfLoading billOfLoading { get; set; }

        [StringLength(38)]
        [Display(Name = "Container Number")]
        public string containerNumberId { get; set; }

        [Display(Name = "Container Number")]
        [ForeignKey("containerNumberId")]
        public ContainerDetails containerNumber { get; set; }

        [Display(Name = "Cargo Item No")]
        public string cargoItemNo { get; set; }

        [Display(Name = "Marks And Nos (Add Semi Colon to Break Line)")]
        public string marksAndNo { get; set; }

        [Display(Name = "Wood Pack")]
        public string woodPack { get; set; }

        [Display(Name = "Line Seal No 1")]
        public string sealno1 { get; set; }

        [Display(Name = "Custom Seal No 2")]
        public string sealno2 { get; set; }

        [Display(Name = "Seal No 3")]
        public string sealno3 { get; set; }

        [Display(Name = "No Of Packages")]
        public string noOfPackages { get; set; }

        [Display(Name = "Kind Of Packages / UN Packing Code")]
        public string kindOfPackages { get; set; }

        [Display(Name = "Description Of Goods (Add Semi Colon to Break Line)")]
        public string description { get; set; }

        [Display(Name = "Gross Cargo Weight")]
        public string grossCargoWeight { get; set; }

        [Display(Name = "Net Cargo Weight")]
        public string netCargoWeight { get; set; }

        [Display(Name = "Unit")]
        public Unit unit { get; set; }

        [Display(Name = "Measurement (CBM)")]
        public string measurement { get; set; }

        [Display(Name = "HS Code")]
        public string hsCode { get; set; }

        [Display(Name = "Shipping Bill Date")]
        public DateTime shippingBillDate { get; set; }




    }
}
