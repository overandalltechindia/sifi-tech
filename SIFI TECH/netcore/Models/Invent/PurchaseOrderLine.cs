﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class PurchaseOrderLine : INetcoreBasic
    {
        public PurchaseOrderLine()
        {
            this.createdAt = DateTime.Now.Date;
            this.discountAmount = 0m;
            this.totalAmount = 0m;
        }

        [StringLength(38)]
        [Display(Name = "Inward Order Line Id")]
        public string purchaseOrderLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Inward Order Id")]
        public string purchaseOrderId { get; set; }

        [Display(Name = "Inward Order")]
        public PurchaseOrder purchaseOrder { get; set; }

        [StringLength(38)]
        [Display(Name = "Product Model No.")]
        public string productId { get; set; }

        [Display(Name = "Product")]
        public Product product { get; set; }

        [Display(Name = "Width")]
        public decimal width { get; set; }

        [Display(Name = "Heigth")]
        public decimal height { get; set; }

        [Display(Name = "Thickness")]
        public decimal thickness { get; set; }

        [Display(Name = "Quantity (Count)")]
        public float qty { get; set; }

        [Display(Name = "Weight Per Piece")]
        public float weight { get; set; }

        [Display(Name = "Batch Number")]
        public int batchNumber { get; set; }

        //[Display(Name = "Total Weight")]
        //public int palletBundle { get; set; }

        [Display(Name = "Total Weight")]
        public int totalWeight { get; set; }



        [Display(Name = "Price in INR")]
        public decimal price { get; set; }

        [Display(Name = "Discount Amount")]
        public decimal discountAmount { get; set; }

        [Display(Name = "Total Amount in INR")]
        public decimal totalAmount { get; set; }
    }
}
