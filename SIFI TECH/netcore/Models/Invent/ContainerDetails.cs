﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ContainerDetails : INetcoreBasic
    {
        public ContainerDetails()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.containerManufactureDate = DateTime.Now.Date;
            this.dateofPurchase = DateTime.Now.Date;
            this.containerleasingDate = DateTime.Now.Date;
            this.availableOn = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Container Details Id")]
        public string containerDetailsId { get; set; }

        [StringLength(50)]
        [Display(Name = "Container Code")]
        [Required]
        public string containerCode { get; set; }

        [Display(Name = "Container Id")]
        public string containerId { get; set; }

        [Display(Name = "Container")]
        public Container container { get; set; }

        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [Display(Name = "Current Location Id")]
        public string locationId { get; set; }

        [Display(Name = "Current Location")]
        public Location location { get; set; }

        [StringLength(50)]
        [Display(Name = "Description")]
        public string description { get; set; }

        [StringLength(50)]
        [Display(Name = "Barcode")]
        public string barcode { get; set; }

        [StringLength(50)]
        [Display(Name = "Serial Number")]
        public string serialNumber { get; set; }

        [Display(Name = "Ownership")]
        public Ownership ownership { get; set; }

        [Display(Name = "Container Status")]
        public ContainerStatus containerStatus { get; set; }



        [Display(Name = "Container Manufacture Date")]
        public DateTime containerManufactureDate { get; set; }

        [Display(Name = "Date of Purchase")]
        public DateTime dateofPurchase { get; set; }

        [Display(Name = "Purchased / Leasing Party")]
        public string purchasedLeasingParty { get; set; }

        [Display(Name = "Purchased / Leasing location")]
        public string purchasedLeasingLocation { get; set; }

        [Display(Name = "Container leasing Date")]
        public DateTime containerleasingDate { get; set; }

        [Display(Name = "No of Lease days")]
        public int noofLeasedays { get; set; }

        [Display(Name = "Tare weight")]
        public decimal tareweight { get; set; }

        [Display(Name = "PayloadCapacity")]
        public decimal payloadcapacity { get; set; }

        [Display(Name = "Empty yard Location Id")]
        public string emptyyardLocationId { get; set; }

        [Display(Name = "Empty yard Location")]
        public Location emptyyardLocation { get; set; }

        [Display(Name = "Available On")]
        public DateTime availableOn { get; set; }

        [Display(Name = "Idle days")]
        public int? noOfIdleDays { get; set; }


        [Display(Name = "Is Active")]
        public bool isActive { get; set; }

        [NotMapped]
        [Display(Name = "Booking Number")]
        public string bookingNumber { get; set; }

    }
}
