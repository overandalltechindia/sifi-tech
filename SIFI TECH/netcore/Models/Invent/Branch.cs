﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Branch: INetcoreBasic, IBaseAddress
    {
        public Branch()
        {
            this.createdAt = DateTime.Now.Date;
            this.isDefaultBranch = false;
        }

        [StringLength(38)]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [StringLength(50)]
        [Display(Name = "Branch Name")]
        [Required]
        public string branchName { get; set; }

        [StringLength(50)]
        [Display(Name = "Branch Description")]
        public string description { get; set; }

        [Display(Name = "Is Default Branch ?")]
        public bool isDefaultBranch { get; set; } = false;

        //IBaseAddress
        [Display(Name = "Address 1")]
        [Required]
        [StringLength(50)]
        public string street1 { get; set; }

        [Display(Name = "Address 2")]
        [Required]
        public string Address2 { get; set; }

        [Display(Name = "PIN Code")]
        [StringLength(50)]
        public string street2 { get; set; } // use as Pin code

        [Display(Name = "City")]
        [StringLength(30)]
        public string city { get; set; }

        [Display(Name = "State")]
        [StringLength(30)]
        public string province { get; set; }

        [Display(Name = "Country")]
        [StringLength(30)]
        public string country { get; set; }
        //IBaseAddress



        [Display(Name = "Bank Name")]
        [StringLength(50)]
        public string bankName { get; set; }

        [Display(Name = "Benificiary")]
        [StringLength(100)]
        public string benificiary { get; set; }

        [Display(Name = "Account Number")]
        [StringLength(50)]
        public string accountNumber { get; set; }

        [Display(Name = "IFSC")]
        [StringLength(50)]
        public string ifsc { get; set; }

        [Display(Name = "IBAN")]
        [StringLength(50)]
        public string iban { get; set; }

        [Display(Name = "Currency")]
        [StringLength(50)]
        public string currency { get; set; }

        [Display(Name = "GST")]
        [StringLength(50)]
        public string gst { get; set; }

        [Display(Name = "PAN")]
        [StringLength(50)]
        public string pan { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Contact")]
        public string Contact { get; set; }


        [Display(Name = "Logo URL")]
        public string branchLogoUrl { get; set; }

    }
}
