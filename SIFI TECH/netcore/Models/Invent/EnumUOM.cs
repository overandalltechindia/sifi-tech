﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum UOM
    {
        KG = 1,
        LBS = 2,
        PCS = 3, 
    }
}
