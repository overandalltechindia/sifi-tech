﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class SalesInquiryLine : INetcoreBasic
    {
        public SalesInquiryLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Sales Inquiry Line Id")]
        [Key]
        public string salesInquiryLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Sales Inquiry Id")]
        public string salesInquiryId { get; set; }

        [Display(Name = "Sales Inquiry")]
        public SalesInquiry salesInquiry { get; set; }

        [StringLength(38)]
        [Display(Name = "Charge Head Id")]
        public string chargeHeadId { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        [Display(Name = "SaleCategory")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SaleCategory saleCategory { get; set; }

        [StringLength(38)]
        [Display(Name = "Laden Container Id")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Laden")]
        [ForeignKey("ladenContainerId")]
        public Container containerLaden { get; set; }

        [Display(Name = "Price")]
        public decimal ladenPrice { get; set; }

        [Display(Name = "Count")]
        public decimal count { get; set; }

        [Display(Name = "Total Price")]
        public decimal totalPrice { get; set; }

        [Display(Name = "Part")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Part part { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency Id")]
        public string currencyId { get; set; }

        [Display(Name = "Currency")]
        public Currency currency { get; set; }

    }
}
