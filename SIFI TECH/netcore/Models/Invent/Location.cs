﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Location : INetcoreBasic
    {
        public Location()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "LocationId")]
        public string locationId { get; set; }

        [Display(Name = "Countries")]
        public Countries countries { get; set; }

        [StringLength(38)]
        [Display(Name = "Location Name")]
        public string locationName { get; set; }

        [StringLength(38)]
        [Display(Name = "Location Code")]
        public string locationCode { get; set; }

        [Display(Name = "Location Type")]
        public LocationType LocationType  { get; set; }

    }
}
