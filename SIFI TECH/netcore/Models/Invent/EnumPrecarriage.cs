﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum Precarriage
    {
        [Display(Name = "MERCHANT")]
        MERC = 1,
        [Display(Name = "CARRIER")]
        CARR = 2,
    }

    public enum VIA
    {
        [Display(Name = "DIRECT")]
        DIRECT = 1,
        [Display(Name = "TRANSHIPMENT")]
        TRANSHIPMENT = 2,
    }


    public enum MODE
    {
        [Display(Name = "CY/CY")]
        CYCY = 1,
        [Display(Name = "SD/CY")]
        SDCY = 2,
        [Display(Name = "CY/SD")]
        CYSD = 3,
        [Display(Name = "SD/SD")]
        SDSD = 4,
    }

    public enum POLPOD
    {
        [Display(Name = "FIO")]
        FIO = 1,
        [Display(Name = "CY")]
        CY = 2,
    }


    public enum FreightTerm
    {
        [Display(Name = "COLLECT")]
        COLLECT = 1,
        [Display(Name = "PREPAID")]
        PREPAID = 2,
    }


    public enum Laden
    {
        [Display(Name = "LADEN")]
        LADEN = 1,
    }

    public enum Empty
    {
        [Display(Name = "EMPTY")]
        EMPTY = 1,
    }

    public enum ContainerEmptyLaden
    {
        [Display(Name = "LADEN")]
        LADEN = 1,

        [Display(Name = "EMPTY")]
        EMPTY = 2,
    }

    public enum Part
    {
        [Display(Name = "FREIGHT")]
        FREIGHT = 1,
        [Display(Name = "DESTINATION")]
        DESTINATION = 2,
        [Display(Name = "ORIGIN")]
        ORIGIN = 3,
        [Display(Name = "MISCELLANEOUS")]
        MISCELLANEOUS = 4,
    }



    public enum Category
    {
        [Display(Name = "Road")]
        Road = 1,
        [Display(Name = "Rail")]
        Rail = 2,
        [Display(Name = "Storage")]
        STORAGE = 3,
    }

    public enum ChargeHeadCategory
    {
        [Display(Name = "Ocean")]
        Ocean = 1,
        [Display(Name = "Inland")]
        Inland = 2,
        [Display(Name = "Storage")]
        Storage = 3,
    }

    public enum ContainerCategory
    {
        [Display(Name = "Normal")]
        Normal = 1,
        [Display(Name = "Hazard")]
        Hazard = 2,
        [Display(Name = "Refeer")]
        Refeer = 3,
        [Display(Name = "ODC")]
        ODC = 4,
    }

    public enum SaleCategory
    {
        [Display(Name = "BL")]
        BL = 1,
        [Display(Name = "CONT")]
        CONT = 2,
    }

    public enum ServiceMode
    {
        [Display(Name = "STORE DOOR")]
        SD = 1,
        [Display(Name = "CONTAINER YARD")]
        CY = 2,
    }

    public enum Status
    {
        [Display(Name = "DRAFT")]
        DRAFT = 1,
        [Display(Name = "BOOKED")]
        BOOKED = 2,
    }


    public enum InvoiceStatus
    {
        [Display(Name = "DRAFT")]
        DRAFT = 1,
        [Display(Name = "COMPLETED")]
        COMPLETED = 2,
    }



    public enum DocumentType
    {
        [Display(Name = "Seaway BL")]
        Seaway = 1,
        [Display(Name = "Original BL")]
        original = 2,
    }


    public enum MovementType
    {
        [Display(Name = "FCL/FCL")]
        FCL = 1,
        [Display(Name = "LCL/LCL")]
        lcl = 2,
    }



    public enum SetCharges
    {
        [Display(Name = "All Prepaid")]
        AllPrepaid = 1,
        [Display(Name = "All Collect")]
        AllCollect = 2,
        [Display(Name = "Individually")]
        Individually = 3,
    }


    public enum Charges
    {
        [Display(Name = "Prepaid")]
        Prepaid = 1,
        [Display(Name = "Collect")]
        Collect = 2,

    }


    public enum FreightPayableAt
    {
        [Display(Name = "Origin")]
        Origin = 1,
        [Display(Name = "Destination")]
        Destination = 2,
     
    }


    public enum BLType
    {
        [Display(Name = "Freighted BL")]
        Freighted = 1,
        [Display(Name = "Unfreighter BL")]
        Unfreighter = 2,

    }


    public enum Unit
    {
        [Display(Name = "KG")]
        KG = 1,
        [Display(Name = "MT")]
        MT = 2,

    }

    public enum Forms
    {
        [Display(Name = "11 - Rail")]
        Rail = 1,
        [Display(Name = "13 -  Road")]
        Road = 2,

    }

    public enum Conformation
    {
        [Display(Name = "No")]
        No = 1,
        [Display(Name = "Yes")]
        Yes = 2,

    }


    public enum TRPMode
    {
        [Display(Name = "Rail")]
        Rail = 1,
        [Display(Name = "Road")]
        Road = 2,

    }
    public enum Ownership
    {
        [Display(Name = "Owned")]
        owned = 1,
        [Display(Name = "Lease")]
        lease = 2,
        [Display(Name = "One Way")]
        oneway = 3,

    }

    public enum ContainerStatus
    {
        [Display(Name = "Empty")]
        empty = 1,
        [Display(Name = "Loaded")]
        loaded = 2,
        [Display(Name = "On water")]
        onwater = 3,

    }

    public enum VendorType
    {
        [Display(Name = "Empty Yard")]
        EmptyYard = 1,
        [Display(Name = "Slot Operator ")]
        SlotOperator = 2,
        [Display(Name = "Freight Forwarder")]
        FreightForwarder = 3,
        [Display(Name = "CHA")]
        CHA = 4,
        [Display(Name = "NVOCC / Liner")]
        NVOCCLiner = 5,
    }

    public enum EximType
    {
        [Display(Name = "Import")]
        Import = 1,
        [Display(Name = "Export")]
        Export = 2,
    }

    public enum LocationType
    {
        [Display(Name = "Port")]
        Port = 1,
        [Display(Name = "Empty Yard")]
        EmptyYard = 2,
        [Display(Name = "Port & Yard")]
        PortYard = 3,
    }


    public enum PricingType
    {
        [Display(Name = "Ocean")]
        Ocean = 1,
        [Display(Name = "In Land")]
        Inland = 2,
        [Display(Name = "Port")]
        Port = 3,
        [Display(Name = "Local")]
        Local = 4,
        [Display(Name = "Multiple")]
        Multiple = 5
    }

    public enum LinePricingType
    {
        [Display(Name = "Ocean")]
        Ocean = 1,
        [Display(Name = "In Land")]
        Inland = 2,
        [Display(Name = "Port")]
        Port = 3,
        [Display(Name = "Local")]
        Local = 4
    }
}
