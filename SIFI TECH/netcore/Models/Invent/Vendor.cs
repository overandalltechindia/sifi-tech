﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Vendor : INetcoreMasterChild
    {
        public Vendor()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.registrationDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [StringLength(50)]
        [Display(Name = "Vendor Name")]
        [Required]
        public string vendorName { get; set; }

        [StringLength(50)]
        [Display(Name = "Description")]
        public string description { get; set; }
        
        [Display(Name = "Type Of Company")]
        public BusinessSize size { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Location")]
        public string locationId { get; set; }

        [Display(Name = "Location")]
        public Location location { get; set; }

        //IBaseAddress
        [Display(Name = "Address")]
        [Required]
        [StringLength(256)]
        public string street1 { get; set; }

        [Display(Name = "PIN Code")]
        [StringLength(100)]
        public string street2 { get; set; } // use as Pin code

        [Display(Name = "City")]
        [StringLength(30)]
        public string city { get; set; }

        [Display(Name = "State")]
        [StringLength(30)]
        public string province { get; set; }

        [Display(Name = "Countries")]
        public Countries countries { get; set; }


        [Display(Name = "Vendor Type")]
        public VendorType vendorType { get; set; }

        [Display(Name = "Primary Contact")]
        [StringLength(20)]
        public string contact { get; set; }

        [Display(Name = "GST/VAT No")]
        public string gstVat { get; set; }

        [Display(Name = "PAN/License No")]
        public string panLicence { get; set; }

        [Display(Name = "TAN No")]
        public string tan { get; set; }

        [Display(Name = "CIN No./UAN")]
        public string cinUan { get; set; }

        [Display(Name = "Credit Days")]
        public string creditDays { get; set; }

        [Display(Name = "Credit Amount")]
        public string creditAmount  { get; set; }

        [Display(Name = "Registration Date")]
        public DateTime registrationDate { get; set; }

        [Display(Name = "Is Active")]
        public bool isActive { get; set; }



        [Display(Name = "Vendor Contacts")]
        public List<VendorLine> vendorLine { get; set; } = new List<VendorLine>();
    }
}
