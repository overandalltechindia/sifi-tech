﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class JobWiseProfit : INetcoreBasic
    {
        public JobWiseProfit()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.invDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.dueDate = DateTime.Now.Date;
            this.receiptDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            //this.invNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-INV";


        }

        [StringLength(38)]
        [Display(Name = "Job Wise Profit Id")]
        public string jobWiseProfitId { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }


        [Display(Name = "MBL Number")]
        public string nblNumber { get; set; }

        [Display(Name = "Invoice Number")]
        public string invNumber { get; set; }

        [Display(Name = "Invoice Date")]
        public DateTime invDate { get; set; }

        [Display(Name = "Due Date")]
        public DateTime dueDate { get; set; }

        [Display(Name = "UTR Numbers")]
        public string utrNumber { get; set; }

        [Display(Name = "Total Invoice Amount")]
        public string totalInvoiceAmount { get; set; }

        [Display(Name = "Supply To")]
        public string supplyTo { get; set; }

        [Display(Name = "Receipt Amount")]
        public string receiptAmount { get; set; }

        [Display(Name = "TDS Amount")]
        public string tds { get; set; }

        [Display(Name = "Receipt Date")]
        public DateTime receiptDate { get; set; }

        [Display(Name = "Net Balance")]
        public string netBalance { get; set; }

        [Display(Name = "Credit Days")]
        public string creditDays { get; set; }

        [Display(Name = "Status")]
        public InvoiceStatus status { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Container/Booking Remarks")]
        public string remark { get; set; }

        [Display(Name = "Invoices Line")]
        public List<InvoicesLine> invoicesLine { get; set; } = new List<InvoicesLine>();

    }
}
