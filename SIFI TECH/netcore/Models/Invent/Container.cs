﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Container : INetcoreBasic
    {
        public Container()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "ContainerId")]
        public string containerId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Container Type")]
        public ContainerType containerType { get; set; }

        [StringLength(38)]
        [Display(Name = "ContainerName")]
        public string containerName { get; set; }


    }
}
