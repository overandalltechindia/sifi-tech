﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ExportOperationLine : INetcoreBasic
    {
        public ExportOperationLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.emptyPickupDate = DateTime.Now.Date;
            this.form1113Date = DateTime.Now.Date;
            this.terminalGateInDate = DateTime.Now.Date;
            this.onBoardDate = DateTime.Now.Date;
            this.etaDestination = DateTime.Now.Date;
            this.requestDate = DateTime.Now.Date;
            this.updationDate = DateTime.Now.Date;
            this.ssrRequestDate = DateTime.Now.Date;
            this.ssrUpdationDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Export Operation Line Id")]
        [Key]
        public string exportOperationLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Export Operation Id")]
        public string exportOperationId { get; set; }

        [Display(Name = "Export Operation")]
        public ExportOperation exportOperation { get; set; }

        [Display(Name = "Empty Pickup Date")]
        public DateTime emptyPickupDate { get; set; }

        [StringLength(38)]
        [Display(Name = "Container Number")]
        public string containerNumberId { get; set; }

        [Display(Name = "Container Number")]
        [ForeignKey("containerNumberId")]
        public ContainerDetails containerNumber { get; set; }

        [Display(Name = "Empty Pickup Yard")]
        public string emptyPickupYard { get; set; }

        [Display(Name = "Tare Weight")]
        public string tareWeight { get; set; }

        [Display(Name = "Payload Weight")]
        public string payloadWeight { get; set; }

        [Display(Name = "Actual Gross Weight")]
        public string actualGrossWeight { get; set; }

        [Display(Name = "Form 11 / 13")]
        public Forms form1113 { get; set; }

        [Display(Name = "Form 11 / 13 Date")]
        public DateTime form1113Date { get; set; }

        [Display(Name = "Terminal Gate In Date")]
        public DateTime terminalGateInDate { get; set; }

        [Display(Name = "Gate In Terminal")]
        public string gateInTerminal { get; set; }


        [Display(Name = "On Board Date")]
        public DateTime onBoardDate { get; set; }

        [Display(Name = "ETA Destination")]
        public DateTime etaDestination { get; set; }


        [Display(Name = "Request Date")]
        public DateTime requestDate { get; set; }

        [Display(Name = "Updation Date")]
        public DateTime updationDate { get; set; }



        [Display(Name = "SSR")]
        public string ssr { get; set; }

        [Display(Name = "SSR Request Date")]
        public DateTime ssrRequestDate { get; set; }

        [Display(Name = "SSR Updation Date")]
        public DateTime ssrUpdationDate { get; set; }

        [Display(Name = "Vessel")]
        public string vessel { get; set; }


        [Display(Name = "Vessel Terminal")]
        public string vesselTerminal { get; set; }



        [Display(Name = "Shifting Conformation")]
        public Conformation shiftingConformation { get; set; }


    }
}
