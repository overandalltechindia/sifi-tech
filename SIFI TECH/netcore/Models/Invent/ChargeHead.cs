﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ChargeHead : INetcoreBasic
    {
        public ChargeHead()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Charge Head Id")]
        public string chargeHeadId { get; set; }
        
        [Display(Name = "Category Type")]
        public CategoryType categoryType { get; set; }

        [Display(Name = "Part")]
        public Part part { get; set; }

        [Display(Name = "Category")]
        public ChargeHeadCategory category { get; set; }

        [StringLength(38)]
        [Display(Name = "Charge Head")]
        public string chargeHeadName { get; set; }


    }
}
