﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Export Operation")]
    public class ExportOperation : INetcoreMasterChild
    {
        public ExportOperation()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.exportOperationNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-EXP";
            this.exportOperationeDate = DateTime.Now.Date;
            this.bookingDate = DateTime.Now.Date;
            this.egmDate = DateTime.Now.Date;
            this.docHandoverdate = DateTime.Now.Date;
            this.leoDate = DateTime.Now.Date;
            this.etaDate = DateTime.Now.Date;
            this.etdDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Export Operation Id")]
        public string exportOperationId { get; set; }

        [StringLength(20)]
        [Display(Name = "Export Operation Number")]
        public string exportOperationNumber { get; set; }

        [Display(Name = "Export Operation Date")]
        public DateTime exportOperationeDate { get; set; }

        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        //[Display(Name = "Vendor Id")]
        //public string vendorId { get; set; }

        //[Display(Name = "Vendor")]
        //public Vendor vendor { get; set; }

        [Display(Name = "Shipper")]
        public string shipper { get; set; }

        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        //[Display(Name = "Country")]
        //public Countries countries { get; set; }

        //[StringLength(64)]
        //[Display(Name = "City")]
        //public string city { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        //[Display(Name = "Bill Of Lading Id")]
        //public string billOfLadingId { get; set; }

        //[Display(Name = "Bill Of Lading")]
        //public BillOfLoading billOfLading { get; set; }

        [Display(Name = "Booking Date")]
        public DateTime bookingDate { get; set; }

        [Display(Name = "Origin Free Days")]
        public string originFreeDays { get; set; }

        [Display(Name = "Origin Port Storage")]
        public string originPortStorage { get; set; }

        [Display(Name = "Destination Free Days")]
        public string destinationFreeDays { get; set; }

        [Display(Name = "Destination Port Storage")]
        public string destinationPortStorage { get; set; }

        [Display(Name = "Commodity")]
        public string commodity { get; set; }


        [Display(Name = "UAN")]
        public string uan { get; set; }

        [Display(Name = "Class")]
        public string classes { get; set; }

        [Display(Name = "Stuffing Type")]
        public string stuffingType { get; set; }

        [Display(Name = "EGM Number")]
        public string egmNumber { get; set; }

        [Display(Name = "EGM Date")]
        public DateTime egmDate { get; set; }

        [Display(Name = "Freight")]
        public string freight { get; set; }

        [Display(Name = "POL")]
        public string pol { get; set; }

        [Display(Name = "POD")]
        public string pod { get; set; }

        [Display(Name = "Vessel")]
        public string vessel { get; set; }

        [Display(Name = "Terminal")]
        public string terminal { get; set; }

        [Display(Name = "Line Code")]
        public string lineCode { get; set; }

        [Display(Name = "Slot Operator")]
        public string slotOperator { get; set; }

        [Display(Name = "No of Container")]
        public string noOfContainer { get; set; }

        [Display(Name = "Type")]
        public string type { get; set; }

        [Display(Name = "Doc Handover Date")]
        public DateTime docHandoverdate { get; set; }

        [Display(Name = "SB Number")]
        public string sbNumber { get; set; }

        [Display(Name = "Shipping Bill Number")]
        public string shippingBillNo { get; set; }

        [Display(Name = "Surveyor ")]
        public string surveyor { get; set; }

        [Display(Name = "LEO Date")]
        public DateTime leoDate { get; set; }

        [Display(Name = "Remark")]
        public string remark { get; set; }



        [Display(Name = "HS Code")]
        public string hsCode { get; set; }

        [Display(Name = "Container Category")]
        public ContainerCategory containerCategory { get; set; }

        [Display(Name = "Status")]
        public Status status { get; set; }

        [Display(Name = "ETA Date")]
        public DateTime etaDate { get; set; }

        [Display(Name = "ETD Date")]
        public DateTime etdDate { get; set; }

        [Display(Name = "Prepared by")]
        public string preparedby { get; set; }

        [Display(Name = "Export Operation Lines")]
        public List<ExportOperationLine> exportOperationLine { get; set; } = new List<ExportOperationLine>();

        [Display(Name = "Principal")]
        public string principal { get; set; }
    }
}
