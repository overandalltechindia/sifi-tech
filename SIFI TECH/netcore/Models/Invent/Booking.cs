﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Booking")]
    public class Booking : INetcoreMasterChild
    {
        public Booking()
        {
            this.createdAt = DateTime.Now + DateTime.Now.TimeOfDay;
            //this.bookingNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#BK";
            this.earliestdepatureDate = DateTime.Now.Date;
            this.eta = DateTime.Now.Date;
            this.etd = DateTime.Now.Date;
            this.bookingValidity = DateTime.Now.Date;
            this.docCutOff = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Booking")]
        public string bookingId { get; set; }

        [StringLength(35)]
        [Display(Name = "Booking Number")]
        public string bookingNumber { get; set; }

        [StringLength(38)]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [Display(Name = "Pricing Type")]
        public PricingType pricingType { get; set; }

        [StringLength(38)]
        [Display(Name = "OceanPrice Id")]
        public string oceanPriceId { get; set; }

        [Display(Name = "Ocean Price")]
        [ForeignKey("oceanPriceId")]
        public OceanPrice oceanPrice { get; set; }


        [StringLength(38)]
        [Display(Name = "InlandPrice Id")]
        public string inlandPriceId { get; set; }

        [Display(Name = "Inland Price")]
        [ForeignKey("inlandPriceId")]
        public InlandPrice inlandPrice { get; set; }


        [StringLength(38)]
        [Display(Name = "PortPrice Id")]
        public string portPriceId { get; set; }

        [Display(Name = "Port Price")]
        [ForeignKey("portPriceId")]
        public PortPrice portPrice { get; set; }



        [StringLength(38)]
        [Display(Name = "LocalPrice Id")]
        public string localPriceId { get; set; }

        [Display(Name = "Local Price")]
        [ForeignKey("localPriceId")]
        public LocalPrice localPrice { get; set; }




        [StringLength(64)]
        [Display(Name = "Email Id")]
        public string emailId { get; set; }

        [StringLength(64)]
        [Display(Name = "Sales Person")]
        public string salesPersonName { get; set; }

        [Display(Name = "Status")]
        public Status status { get; set; }

        [StringLength(38)]
        [Display(Name = " From Location Id")]
        public string fromlocationId { get; set; }

        [Display(Name = "Origin")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [Display(Name = "From Service Mode")]
        public ServiceMode fromServiceMode { get; set; }

        [StringLength(38)]
        [Display(Name = "To Location Id")]
        public string tolocationId { get; set; }

        [Display(Name = "Destination")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }

        [Display(Name = "To Service Mode")]
        public ServiceMode toServiceMode { get; set; }

        [Display(Name = "I want to pickup the empty containers in another city")]
        public bool isEmptyContainer { get; set; }

        [StringLength(64)]
        [Display(Name = "City")]
        public string city { get; set; }

        //[StringLength(128)]
        //[Display(Name = "Sales Enquiry Number")]
        //public string salesEnquiryNumber { get; set; }

        [Display(Name = "Booking Release. Date")]
        public DateTime earliestdepatureDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }



        [Display(Name = "Place of Receipt")]
        public string placeofReceiptId { get; set; }

        [Display(Name = "Place of Receipt")]
        [ForeignKey("placeofReceiptId")]
        public Location placeofReceipt { get; set; }

        [Display(Name = "Place of Delivery")]
        public string placeofDeliveryId { get; set; }

        [Display(Name = "Place of Delivery")]
        [ForeignKey("placeofDeliveryId")]
        public Location placeofDelivery { get; set; }

        [Display(Name = "Stuffing Type")]
        public string stuffingType { get; set; }

        [Display(Name = "Commodity")]
        public string commodity { get; set; }

        [Display(Name = "HS Code")]
        public string hsCode { get; set; }

        [Display(Name = "Origin Free Days")]
        public decimal originFreeDays { get; set; }

        [Display(Name = "Origin Port Storage Days")]
        public decimal originPortStorageDays { get; set; }

        [Display(Name = "Destination Free Days")]
        public decimal destinationFreeDays { get; set; }

        [Display(Name = "Destination Port Storage Days")]
        public decimal destinationPortStorageDays { get; set; }

        [Display(Name = "Sales Inquiry Id")]
        public string salesInquiryId { get; set; }

        [Display(Name = "Sales Inquiry")]
        public SalesInquiry salesInquiry { get; set; }

        [Display(Name = "Booking Slot Id")]
        public string bookingSlotId { get; set; }

        [Display(Name = "Booking Slot")]
        public BookingSlot bookingSlot { get; set; }


        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "ETA")]
        public DateTime eta { get; set; }

        [Display(Name = "ETD")]
        public DateTime etd { get; set; }




        [Display(Name = "Surveyor Details")]
        public string surveyorDetails { get; set; }

        [Display(Name = "No Of Containers")]
        public string noOfContainers { get; set; }

        [Display(Name = "Booking Validity")]
        public DateTime bookingValidity { get; set; }

        [Display(Name = "Freight Terms")]
        public string freightTerms { get; set; }

        [Display(Name = "Final Destination")]
        public string finalDestination { get; set; }

        [Display(Name = "Document Cut Off")]
        public DateTime docCutOff { get; set; }

        [Display(Name = "Stuffing Point")]
        public string stuffingPoint { get; set; }

        //[Display(Name = "Container Empty Pickup Location")]
        //public string emptyPickupLocation { get; set; }

        [Display(Name = "Vessel Gate In Terminal")]
        public string vesselGateInTerminal { get; set; }

        [Display(Name = "Booking Lines")]
        public List<BookingLine> bookingLine { get; set; } = new List<BookingLine>();


        [Display(Name = "Principal")]
        public string principal { get; set; }
    }
}
