﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Inward")]
    public class PurchaseOrder : INetcoreMasterChild
    {
        public PurchaseOrder()
        {
            this.createdAt = DateTime.Now.Date;
            this.purchaseOrderNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#PO";
            this.poDate = DateTime.Now.Date;
            this.deliveryDate = this.poDate.AddDays(5);
            this.purchaseOrderStatus = PurchaseOrderStatus.Draft;
            this.totalDiscountAmount = 0m;
            this.totalOrderAmount = 0m;
        }

        [StringLength(38)]
        [Display(Name = "Inward Order Id")]
        public string purchaseOrderId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Inward Number")]
        public string purchaseOrderNumber { get; set; }

        [StringLength(38)]
        [Display(Name = "Delivery Inward Number")]
        public string deliveryInwardNumber { get; set; }

        [Display(Name = "Terms of Payment (TOP)")]
        public TOP top { get; set; }


        [Display(Name = "Inward Date")]
        [Required]
        public DateTime poDate { get; set; }

        
        [Display(Name = "Delivery Date")]
        public DateTime deliveryDate { get; set; }

        [StringLength(256)]
        [Display(Name = "Delivery Address")]
        public string deliveryAddress { get; set; }
        
        
        [StringLength(100)]
        [Display(Name = "Service Description")]
        public string description { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor Name")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }

        //[StringLength(38)]
        //[Required]
        //[Display(Name = "Customer Name")]
        //public string customerId { get; set; }

        //public Customer customer { get; set; }
        [StringLength(30)]
        [Display(Name = "Employee Name")]
        public string employeeName { get; set; }

        [Display(Name = "USD Rate")]
        public decimal? usdRate { get; set; }


        [StringLength(30)]
        [Display(Name = "PIC Vendor")]
        public string picVendor { get; set; }

        [Display(Name = "PO Status")]
        public PurchaseOrderStatus purchaseOrderStatus { get; set; }

        [Display(Name = "Total Discount")]
        public decimal totalDiscountAmount { get; set; }

        [Display(Name = "Total Order")]
        public decimal totalOrderAmount { get; set; }

        [Display(Name = "Purchase Receive Number")]
        public string purchaseReceiveNumber { get; set; }

        [Display(Name = "Purchase Order Lines")]
        public List<PurchaseOrderLine> purchaseOrderLine { get; set; } = new List<PurchaseOrderLine>();
    }
}
