﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class JobWiseProfitLine : INetcoreBasic
    {
        public JobWiseProfitLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Invoices Line Id")]
        [Key]
        public string invoicesLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Invoices Id")]
        public string invoicesId { get; set; }

        [Display(Name = "Invoices")]
        public Invoices invoices { get; set; }

        [Display(Name = "Cost Id")]
        public string costId { get; set; }

        [Display(Name = "Cost")]
        public ChargeHead cost { get; set; }

        [Display(Name = "Currency")]
        public string currencyId { get; set; }

        [Display(Name = "Currency")]
        public Currency currency { get; set; }

        [Display(Name = "Rate")]
        public decimal rate { get; set; }

        [Display(Name = "Cost Amount")]
        public decimal amount { get; set; }

        [Display(Name = "Income Id")]
        public string incomeId { get; set; }

        [Display(Name = "Income")]
        public ChargeHead income { get; set; }

        [Display(Name = "Type")]
        public ContainerCategory type { get; set; }

        [Display(Name = "CGST")]
        public decimal? cgst { get; set; }

        [Display(Name = "CGST %")]
        public decimal? cgstPercent { get; set; }

        [Display(Name = "SGST")]
        public decimal? sgst { get; set; }

        [Display(Name = "SGST %")]
        public decimal? sgstPercent { get; set; }

        [Display(Name = "IGST")]
        public decimal? igst { get; set; }

        [Display(Name = "IGST %")]
        public decimal? igstPercent { get; set; }

        [Display(Name = "Total Amount")]
        public decimal incamount { get; set; }

        [Display(Name = "Taxable INR")]
        public decimal? taxableINR { get; set; }

        [Display(Name = "Margin")]
        public decimal margin { get; set; }

        [Display(Name = "SAC Code")]
        public string sacCode { get; set; }

    }
}
