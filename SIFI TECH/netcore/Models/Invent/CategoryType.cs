﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum CategoryType
    {   
        [Display(Name = "Import")]
        IMP  = 1,
        [Display(Name = "Export")]
        EXP = 2,
        [Display(Name = "Freight")]
        FREi = 3,
        [Display(Name = "MISCELLANEOUS")]
        MISCELLANEOUS = 4,
    }
}
