﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Multipal Rate Quote")]
    public class MultipalRateQuote : INetcoreMasterChild
    {
        public MultipalRateQuote()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.multipalRateQuoteNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-MRQ";
            this.multipalRateQuoteDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Multipal Rate Quote")]
        public string multipalRateQuoteId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Multipal Rate Quote")]
        public string multipalRateQuoteNumber { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        //[Display(Name = "Countries")]
        //public Countries countries { get; set; }

        //[StringLength(64)]
        //[Display(Name = "City")]
        //public string city { get; set; }

        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime multipalRateQuoteDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }


        [Display(Name = "Status")]
        public Status status { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }


        [Display(Name = "Multipal Rate Quote Lines")]
        public List<MultipalRateQuoteLine> multipalRateQuoteLine { get; set; } = new List<MultipalRateQuoteLine>();
    }
}
