﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Inland Price")]
    public class InlandPrice : INetcoreMasterChild
    {
        public InlandPrice()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.inlandPriceNumber = DateTime.Now.Date.ToString("yyyyMMdd")  +"-"+ Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-IP";
            this.inlandPriceDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Inland Price")]
        public string inlandPriceId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Inland Price Number")]
        public string inlandPriceNumber { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime inlandPriceDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }

        //[Display(Name = "Vendor Country")]
        //public Countries countries { get; set; }

        //[StringLength(64)]
        //[Display(Name = "Vendor City")]
        //public string city { get; set; }

        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [Display(Name = "Pickup Countries")]
        public Countries pickupCountries { get; set; }

        [StringLength(64)]
        [Display(Name = "Pick up Location")]
        public string pickupLocation { get; set; }

        [StringLength(64)]
        [Display(Name = "Pick up City")]
        public string pickupcity { get; set; }

        [StringLength(64)]
        [Display(Name = "Pick up PinCode")]
        public string pickupPincode { get; set; }

        [Display(Name = "Drop Countries")]
        public Countries dropCountries { get; set; }

        [StringLength(64)]
        [Display(Name = "Drop Location")]
        public string dropLocation { get; set; }

        [StringLength(64)]
        [Display(Name = "Drop City")]
        public string dropcity { get; set; }

        [StringLength(64)]
        [Display(Name = "Drop PinCode")]
        public string dropPincode { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }
        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Inland Price Lines")]
        public List<InlandPriceLine> inlandPriceLine { get; set; } = new List<InlandPriceLine>();
    }
}
