﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Port Price")]
    public class PortPrice : INetcoreMasterChild
    {
        public PortPrice()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.portPriceDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
            this.portPriceNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-PP";

        }

        [StringLength(38)]
        [Display(Name = "Port Price")]
        public string portPriceId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Port Price Number")]
        public string portPriceNumber { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        //[Display(Name = "Countries")]
        //public Countries countries { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port Location")]
        public string locationId { get; set; }

        [Display(Name = "Port Location")]
        public Location location { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor (Terminal)")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor (Terminal)")]
        public Vendor vendor { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime portPriceDate { get; set; }

        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }
        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Port Price Line")]
        public List<PortPriceLine> portPriceLine { get; set; } = new List<PortPriceLine>();

        [Display(Name = "Port Storag Price Line")]
        public List<PortStoragePriceLine> portStoragePriceLine { get; set; } = new List<PortStoragePriceLine>();
    }
}
