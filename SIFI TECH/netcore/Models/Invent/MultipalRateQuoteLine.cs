﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class MultipalRateQuoteLine : INetcoreBasic
    {
        public MultipalRateQuoteLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Multipal Rate Quote")]
        [Key]
        public string multipalRateQuoteLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Multipal Rate Quote")]
        public string multipalRateQuoteId { get; set; }

        [Display(Name = "Multipal Rate Quote")]
        public MultipalRateQuote multipalRateQuote { get; set; }

        [StringLength(38)]
        [Display(Name = "Charge Head")]
        public string chargeHeadId { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = " From Location Id")]
        public string fromlocationId { get; set; }

        [Display(Name = "From Location")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "To Location Id")]
        public string tolocationId { get; set; }

        [Display(Name = "To Location")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency Id")]
        public string currencyId { get; set; }

        [Display(Name = "Currency")]
        public Currency currency { get; set; }


        [Display(Name = "VIA")]
        public VIA via { get; set; }
        [StringLength(38)]
        [Display(Name = "TRP Port")]
        public string trpPortId { get; set; }

        [Display(Name = "TRP Port")]
        [ForeignKey("trpPortId")]
        public Location trpPort { get; set; }

        [Display(Name = "MODE")]
        public MODE mode { get; set; }

        [StringLength(38)]
        [Display(Name = "Laden Container")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Laden")]
        [ForeignKey("ladenContainerId")]
        public Container containerLaden { get; set; }

        [Display(Name = "Price")]
        public decimal ladenPrice { get; set; }

    }
}
