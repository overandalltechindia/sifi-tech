﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class PurchaseInvoiceLine : INetcoreBasic
    {
        public PurchaseInvoiceLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Purchase Invoice Line Id")]
        [Key]
        public string purchaseInvoiceLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Purchase Id")]
        public string purchaseInvoiceId { get; set; }

        [Display(Name = "Purchase Invoice")]
        public PurchaseInvoice purchaseInvoice { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Display(Name = "Pricing Type")]
        public LinePricingType linePricingType { get; set; }


        [StringLength(38)]
        [Display(Name = "Charge Head Id")]
        public string chargeHeadId { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency Id")]
        public string currencyId { get; set; }

        [Display(Name = "Currency")]
        public Currency currency { get; set; }

        [Display(Name = "Quantity")]
        public int quantity { get; set; }

        [Display(Name = "Rate")]
        public decimal rate { get; set; }

        [Display(Name = "Container")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContainerEmptyLaden containerEmptyLaden { get; set; }

        [StringLength(38)]
        [Display(Name = "Laden Container")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Laden")]
        [ForeignKey("ladenContainerId")]
        public Container containerLaden { get; set; }

        [StringLength(38)]
        [Display(Name = "Empty Container")]
        public string emptyContainerId { get; set; }

        [Display(Name = "Container Empty")]
        [ForeignKey("emptyContainerId")]
        public Container containerEmpty { get; set; }

        [Display(Name = "Amount")]
        public decimal amount { get; set; }

        [Display(Name = "Total Amount without GST")]
        public decimal totalAmountWithoutGST { get; set; }

        [Display(Name = "SAC Code")]
        public string sac { get; set; }

        [Display(Name = "CGST %")]
        public decimal? cgstPercent { get; set; }

        [Display(Name = "CGST")]
        public decimal cgst { get; set; }

        [Display(Name = "SGST %")]
        public decimal? sgstPercent { get; set; }

        [Display(Name = "SGST")]
        public decimal sgst { get; set; }

        [Display(Name = "IGST %")]
        public decimal? igstPercent { get; set; }

        [Display(Name = "IGST")]
        public decimal igst { get; set; }

        [Display(Name = "Total GST")]
        public decimal totalGSTAmount { get; set; }

        [Display(Name = "Total Amount with GST")]
        public decimal totalAmountWithGST { get; set; }

    }
}
