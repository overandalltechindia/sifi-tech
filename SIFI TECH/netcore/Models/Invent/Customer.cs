﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Customer : INetcoreMasterChild
    {
        public Customer()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.registrationDate = DateTime.Now;


        }

        [StringLength(38)]
        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [StringLength(50)]
        [Display(Name = "Customer Name")]
        [Required]
        public string customerName { get; set; }


        [Display(Name = "Registration Date")]
        public DateTime registrationDate { get; set; }

        [StringLength(50)]
        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "Type Of Company")]
        public BusinessSize size { get; set; }

        //IBaseAddress
        [Display(Name = "Address")]
        [Required]
        [StringLength(256)]
        public string street1 { get; set; }

        [Display(Name = "PIN Code")]
        [StringLength(10)]
        public string street2 { get; set; } // use as Pin code

        [Display(Name = "City")]
        [StringLength(30)]
        public string city { get; set; }

        [Display(Name = "State")]
        [StringLength(30)]
        public string province { get; set; }

        [Display(Name = "Country")]
        public Countries country { get; set; }

        [Display(Name = "Primary Contact")]
        [StringLength(12)]
        public string contact { get; set; }

        [Display(Name = "GST/VAT No")]
        public string gstVat { get; set; }

        [Display(Name = "PAN/License No")]
        public string panLicence { get; set; }

        [Display(Name = "TAN No")]
        public string tan { get; set; }

        [Display(Name = "CIN No./UAN")]
        public string cinUan { get; set; }

        [Display(Name = "Credit Days")]
        public string creditDays { get; set; }

        [Display(Name = "Credit Amount")]
        public string creditAmount { get; set; }

        [Display(Name = "Is Active")]
        public bool isActive { get; set; }

        //IBaseAddress

        [Display(Name = "Customer Contacts")]
        public List<CustomerLine> customerLine { get; set; } = new List<CustomerLine>();
    }
}
