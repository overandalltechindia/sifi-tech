﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BookingLine : INetcoreBasic
    {
        public BookingLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "Booking Line")]
        [Key]
        public string bookingLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [StringLength(38)]
        [Display(Name = "Container Number")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Number")]
        [ForeignKey("ladenContainerId")]
        public ContainerDetails containerLaden { get; set; }

        [Display(Name = "Category")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContainerCategory containerCategory { get; set; }

        [Display(Name = "Temperature")]
        public decimal? temperature { get; set; }

        [Display(Name = "Length (cm)")]
        public decimal? length { get; set; }

        [Display(Name = "Width (cm)")]
        public decimal? width { get; set; }

        [Display(Name = "Height (cm)")]
        public decimal? height { get; set; }

        [Display(Name = "Weight / Cont")]
        public decimal? weight { get; set; }

        [Display(Name = "Empty Yard Location")]
        public string emptyYardLocationId { get; set; }

        [Display(Name = "Empty Yard Location")]
        [ForeignKey("emptyYardLocationId")]
        public Location emptyYardLocation { get; set; }

    }
}
