﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class PortStoragePriceLine : INetcoreBasic
    {
        public PortStoragePriceLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }
        [Key]
        [StringLength(38)]
        [Display(Name = "Port Storage Price Line")]
        public string portStoragePriceLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Port Price")]
        public string portPriceId { get; set; }
        
        [Display(Name = "Port Price")]
        public PortPrice portPrice { get; set; }

        [StringLength(38)]
        [Display(Name = "Charge Head Id")]
        public string chargeHeadId { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        [Display(Name = "20 Feet Price")]
        public decimal twentyPrice { get; set; }

        [Display(Name = "40 Feet Price")]
        public decimal fortyPrice { get; set; }

        [Display(Name = "From Day")]
        public decimal fromDay { get; set; }

        [Display(Name = "To Day")]
        public decimal toDay { get; set; }

    }
}
