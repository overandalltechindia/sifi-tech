﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum ProductType
    {   
        [Display(Name = "GYM Equipment")]
        GE  = 1,
        [Display(Name = "GYM Spare Part")]
        GSP = 2,
        [Display(Name = "Miscellaneous")]
        MSC = 3
        
    }
}
