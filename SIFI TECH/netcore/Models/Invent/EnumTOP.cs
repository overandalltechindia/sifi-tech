﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum TOP
    {
        Cash = 1,
        Cheque = 2,
        NEFT = 3,
        IMPS = 4,
        RTGS = 5
    }
}
