﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class Currency : INetcoreBasic
    {
        public Currency()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
        }

        [StringLength(38)]
        [Display(Name = "CurrencyId")]
        public string currencyId { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency Name")]
        public string currencyName { get; set; }

        [StringLength(38)]
        [Display(Name = "Currency Code")]
        public string currencyCode { get; set; }

    }
}
