﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class BookingSlotLine : INetcoreBasic
    {
        public BookingSlotLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.bookingSlotLineNumber = DateTime.Now.Date.ToString("yyyyMMdd") + Guid.NewGuid().ToString().Substring(0, 5).ToUpper() + "#LP";
        }

        [StringLength(38)]
        [Display(Name = "Booking Slot Line")]
        [Key]
        public string bookingSlotLineId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Booking Slot Line Number")]
        public string bookingSlotLineNumber { get; set; }

        [StringLength(38)]
        [Display(Name = "Booking Slot")]
        public string bookingSlotId { get; set; }

        [Display(Name = "Booking Slot")]
        public BookingSlot bookingSlot { get; set; }


        [Display(Name = "No Of Container")]
        public decimal qty { get; set; }

        [StringLength(38)]
        [Display(Name = "Laden Container")]
        public string ladenContainerId { get; set; }

        [Display(Name = "Container Laden")]
        [ForeignKey("ladenContainerId")]
        public Container containerLaden { get; set; }

        [Display(Name = "Price")]
        public decimal ladenPrice { get; set; }

        [Display(Name = "Category")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContainerCategory containerCategory { get; set; }



    }
}
