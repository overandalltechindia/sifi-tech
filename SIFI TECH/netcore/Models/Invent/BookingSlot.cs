﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Booking Slot")]
    public class BookingSlot : INetcoreMasterChild
    {
        public BookingSlot()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.bookingSlotNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-BSN";
            this.bookingSlotDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
            this.etaDate = DateTime.Now;
            this.etdDate = DateTime.Now;
            this.documentCuttofftDate = DateTime.Now;
            this.siCuttofftDate = DateTime.Now;
            this.documentCuttofftDateIcd = DateTime.Now;
            this.hazoggCuttofftDate = DateTime.Now;
            this.gateOpenCuttofftDate = DateTime.Now;
            this.gateCloseCuttofftDate = DateTime.Now;
            this.vgmCuttofftDate = DateTime.Now;
            this.formCuttofftDate = DateTime.Now;
        }

        [StringLength(38)]
        [Display(Name = "Booking Slot")]
        public string bookingSlotId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Booking Slot Number")]
        public string bookingSlotNumber { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }

        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime bookingSlotDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }



        [StringLength(38)]
        [Required]
        [Display(Name = " From Location Id")]
        public string fromlocationId { get; set; }

        [Display(Name = "From Location")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "To Location Id")]
        public string tolocationId { get; set; }

        [Display(Name = "To Location")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }

        [Display(Name = "ETA Date")]
        public DateTime etaDate { get; set; }

        [Display(Name = "ETD Date")]
        public DateTime etdDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Vessel Name")]
        public string vesselName { get; set; }

        [StringLength(128)]
        [Display(Name = "Service")]
        public string service { get; set; }

        [Display(Name = "Document Cutt Off")]
        [Required]
        public DateTime documentCuttofftDate { get; set; }

        [Display(Name = "SI Cutt Off")]
        [Required]
        public DateTime siCuttofftDate { get; set; }

        [Display(Name = "Document Cutt Off (ICD)")]
        [Required]
        public DateTime documentCuttofftDateIcd { get; set; }

        [Display(Name = "HAZ & OGG Cutt Off")]
        [Required]
        public DateTime hazoggCuttofftDate { get; set; }

        [Display(Name = "Gate Open Cutt Off")]
        [Required]
        public DateTime gateOpenCuttofftDate { get; set; }

        [Display(Name = "Gate CLose Cutt Off")]
        [Required]
        public DateTime gateCloseCuttofftDate { get; set; }

        [Display(Name = "VGM Cutt Off")]
        [Required]
        public DateTime vgmCuttofftDate { get; set; }

        [Display(Name = "Form 11/13 Cutt Off")]
        [Required]
        public DateTime formCuttofftDate { get; set; }

        [Display(Name = "Origin Free Days")]
        public decimal originFreeDays { get; set; }

        [Display(Name = "Origin Port Storage Days")]
        public decimal originPortStorageDays { get; set; }

        [Display(Name = "Destination Free Days")]
        public decimal destinationFreeDays { get; set; }

        [Display(Name = "Destination Port Storage Days")]
        public decimal destinationPortStorageDays { get; set; }

        [Display(Name = "Status")]
        public Status status { get; set; }


        [Display(Name = "Booking Slot Lines")]
        public List<BookingSlotLine> bookingSlotLine { get; set; } = new List<BookingSlotLine>();
    }
}
