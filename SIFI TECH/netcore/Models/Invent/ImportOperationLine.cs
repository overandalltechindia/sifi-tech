﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public class ImportOperationLine : INetcoreBasic
    {
        public ImportOperationLine()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.doValidityDate = DateTime.Now.Date;
            this.ialDate = DateTime.Now.Date;
            this.emptyReturnDate = DateTime.Now.Date;
            this.portOutDate = DateTime.Now.Date;

        }

        [StringLength(38)]
        [Display(Name = "Import Operation Line Id")]
        [Key]
        public string importOperationLineId { get; set; }

        [StringLength(38)]
        [Display(Name = "Import Operation Id")]
        public string importOperationId { get; set; }

        [Display(Name = "Import Operation")]
        public ImportOperation importOperation { get; set; }

        [Display(Name = "ICD Code")]
        public string icdCode { get; set; }

        [Display(Name = "3rd Party Nomination")]
        public string partyNomination { get; set; }

        [Display(Name = "Empty Validity")]
        public string emptyValidity { get; set; }

        [Display(Name = "DO Validity Date")]
        public DateTime doValidityDate { get; set; }

        [Display(Name = "Line Nomination CFS")]
        public string lineNomination { get; set; }

        [StringLength(38)]
        [Display(Name = "Container Number")]
        public string containerNumberId { get; set; }

        [Display(Name = "Container Number")]
        [ForeignKey("containerNumberId")]
        public ContainerDetails containerNumber { get; set; }

        [Display(Name = "IAL")]
        public string ial { get; set; }

        [Display(Name = "IAL Date")]
        public DateTime ialDate { get; set; }

        [Display(Name = "TRP Mode")]
        public TRPMode trpMode { get; set; }

        [Display(Name = "Free Day Expiry")]
        public string freeDayExpiry { get; set; }

        [Display(Name = "No Of Days Dctention")]
        public string npOfDays { get; set; }

        [Display(Name = "Empty Return Date")]
        public DateTime emptyReturnDate { get; set; }

        [Display(Name = "Cont Port Out  Date")]
        public DateTime portOutDate { get; set; }

        [Display(Name = "Cont Weight")]
        public string contWeight { get; set; }

        [Display(Name = "Seal No")]
        public string sealNo { get; set; }

        [Display(Name = "ICD Load In")]
        public string icdLoadIn { get; set; }

        [Display(Name = "ICD Load Out")]
        public string icdLoadOut { get; set; }

        [Display(Name = "ICD Empty In")]
        public string icdEmptyIn { get; set; }

        [Display(Name = "ICD Empty Out")]
        public string icdEmptyOut { get; set; }

        [StringLength(38)]
        [Display(Name = "Empty Yard Id")]
        public string emptyYardId { get; set; }

        [Display(Name = "Empty Yard")]
        [ForeignKey("emptyYardId")]
        public Location emptyYard { get; set; }

    }
}
