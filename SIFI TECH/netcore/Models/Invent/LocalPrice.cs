﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Local Price")]
    public class LocalPrice : INetcoreMasterChild
    {
        public LocalPrice()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.localPriceNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-LP";
            this.localPriceDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Local Price")]
        public string localPriceId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Local Price Number")]
        public string localPriceNumber { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }


        //[Display(Name = "Countries")]
        //public Countries countries { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Location Id")]
        public string locationId { get; set; }

        [Display(Name = "Location")]
        public Location location { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }


        [Display(Name = "From Date")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime toDate { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime localPriceDate { get; set; }

        //[StringLength(64)]
        //[Display(Name = "City")]
        //public string city { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Exim Type")]
        public EximType eximType { get; set; }

        [Display(Name = "Status")]
        public Status status { get; set; }

        [Display(Name = "Local Price Lines")]
        public List<LocalPriceLine> localPriceLine { get; set; } = new List<LocalPriceLine>();
    }
}
