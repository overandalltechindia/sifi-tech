﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    public enum BusinessSize
    {
        [Display(Name = "Sole Proprietorship")]
        SoleProprietorship = 1,
        [Display(Name = "PVT LTD")]
        PVTLTD = 2,
        [Display(Name = "LLP")]
        LLP = 3,
        [Display(Name = "Partnership Firm")]
        PartnershipFirm = 4,
        [Display(Name = "LTD Company")]
        LTDCompany = 5,
        [Display(Name = "LLC")]
        LLC = 6,
    }
}
