﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.Invent
{
    [DisplayName("Ocean Price")]
    public class OceanPrice : INetcoreMasterChild
    {
        public OceanPrice()
        {
            this.createdAt = DateTime.Now.Date + DateTime.Now.TimeOfDay;
            this.oceanPriceNumber = DateTime.Now.Date.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Substring(0, 2).ToUpper() + "-OP";
            this.oceanPriceDate = DateTime.Now.Date;
            this.fromDate = DateTime.Now.Date;
            this.toDate = DateTime.Now.Date;
        }

        [StringLength(38)]
        [Display(Name = "Ocean Price Id")]
        public string oceanPriceId { get; set; }

        [StringLength(20)]
        [Required]
        [Display(Name = "Ocean Price Number")]
        public string oceanPriceNumber { get; set; }

        [Display(Name = "Ocean Price Date")]
        [Required]
        public DateTime oceanPriceDate { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Branch Id")]
        public string branchId { get; set; }

        [Display(Name = "Branch")]
        public Branch branch { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Vendor Id")]
        public string vendorId { get; set; }

        [Display(Name = "Vendor")]
        public Vendor vendor { get; set; }


        [Display(Name = "Validity From")]
        public DateTime fromDate { get; set; }

        [Display(Name = "Validity To")]
        public DateTime toDate { get; set; }

        //[Display(Name = "Vendor Country")]
        //public Countries countries { get; set; }

        //[StringLength(64)]
        //[Display(Name = "Vendor City")]
        //public string city { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port Of Loading")]
        public string fromlocationId { get; set; }

        [Display(Name = "Port Of Loading")]
        [ForeignKey("fromlocationId")]
        public Location fromLocation { get; set; }

        [StringLength(38)]
        [Required]
        [Display(Name = "Port of Destination")]
        public string tolocationId { get; set; }

        [Display(Name = "Port of Destination")]
        [ForeignKey("tolocationId")]
        public Location toLocation { get; set; }



        [Display(Name = "Place of Receipt")]
        public string placeofReceiptId { get; set; }

        [Display(Name = "Place of Receipt")]
        [ForeignKey("placeofReceiptId")]
        public Location placeofReceipt { get; set; }



        [Display(Name = "Place of Delivery")]
        public string placeofDeliveryId { get; set; }

        [Display(Name = "Place of Delivery")]
        [ForeignKey("placeofDeliveryId")]
        public Location placeofDelivery { get; set; }


        [Display(Name = "POL Country")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Countries polCountry { get; set; }

        [Display(Name = "POD Country")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Countries podCountry { get; set; }

        [Display(Name = "Precarriage")]
        public Precarriage precarriage { get; set; }

        [Display(Name = "ON carriage")]
        public Precarriage onCarriage { get; set; }

        [Display(Name = "VIA")]
        public VIA via { get; set; }

        [StringLength(38)]
        [Display(Name = "TRP Port")]
        public string trpPortId { get; set; }

        [Display(Name = "TRP Port")]
        [ForeignKey("trpPortId")]
        public Location trpPort { get; set; }

        [Display(Name = "MODE")]
        public MODE mode { get; set; }

        [Display(Name = "POL Term")]
        public POLPOD polTerm { get; set; }

        [Display(Name = "POD Term")]
        public POLPOD podTerm { get; set; }

        [Display(Name = "Freight Term")]
        public FreightTerm freightTerm { get; set; }

        [Display(Name = "Status")]
        public Status status { get; set; }

        [StringLength(128)]
        [Display(Name = "Remark")]
        public string remark { get; set; }

        [Display(Name = "Prepared By")]
        public string preparedBy { get; set; }

        [Display(Name = "Ocean Price Lines")]
        public List<OceanPriceLine> oceanPriceLine { get; set; } = new List<OceanPriceLine>();
    }
}
