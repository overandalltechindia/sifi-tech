﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models
{
    public class INetcoreBasic
    {
        [Display(Name = "Created At")]
        public DateTime createdAt { get; set; } = DateTime.UtcNow + DateTime.UtcNow.TimeOfDay;
        public bool isDeleted { get; set; }
        public string createdUserEmail { get; set; }
        public string modifiedUserEmail { get; set; }
    }
}
