﻿using netcore.Models.Invent;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ManageViewModels
{
    public class JobWiseProfitViewModel
    {
        [Display(Name = "JobWiseProfit Id")]
        public string JobWiseProfitId { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Booking")]
        public Booking booking { get; set; }

        [Display(Name = "MBL Number")]
        public string mblNumber { get; set; }

        [Display(Name = "From Location")]
        public string fromlocationName { get; set; }

        //[Display(Name = "Origin")]
        //public Location fromLocation { get; set; }

        [Display(Name = "To Location")]
        public string tolocationName { get; set; }

        //[Display(Name = "Destination")]
        //public Location toLocation { get; set; }

        [Display(Name = "Customer Id")]
        public string customerId { get; set; }

        [Display(Name = "Customer")]
        public Customer customer { get; set; }

        [Display(Name = "ETA Date")]
        public DateTime etaDate { get; set; }

        [Display(Name = "ETD Date")]
        public DateTime etdDate { get; set; }

        [Display(Name = "No. of Containers")]
        public string noOfContainers { get; set; }

        [Display(Name = "Vessel")]
        public string vessel { get; set; }

        [Display(Name = "Branch")]
        public string branch { get; set; }

        [Display(Name = "Buy Exchange Rate")]
        public string buyExchangeRate { get; set; }

        [Display(Name = "Sale Exchange Rate")]
        public string sellExchangeRate { get; set; }

        [Display(Name = "JobWise Profit Income Lines")]
        public List<JobWiseProfitLineViewModel> jobWiseProfitLineViewModel { get; set; } = new List<JobWiseProfitLineViewModel>();

        [Display(Name = "JobWise Profit Cost Lines")]
        public List<JobWiseProfitCostLineViewModel> jobWiseProfitCostLineViewModel { get; set; } = new List<JobWiseProfitCostLineViewModel>();

    }
}
