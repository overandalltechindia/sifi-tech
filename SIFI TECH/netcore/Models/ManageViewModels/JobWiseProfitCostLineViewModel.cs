﻿using netcore.Models.Invent;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netcore.Models.ManageViewModels
{
    public class JobWiseProfitCostLineViewModel
    {
        [StringLength(38)]
        [Display(Name = "Job Wise Profit Line")]
        [Key]
        public string jobWiseProfitLineViewModelId { get; set; }

        [Display(Name = "JobWiseProfit Id")]
        public string jobWiseProfitViewModelId { get; set; }

        [Display(Name = "JobWiseProfit")]
        public JobWiseProfitViewModel jobWiseProfitModel { get; set; }

        [Display(Name = "Booking Id")]
        public string bookingId { get; set; }

        [Display(Name = "Charge Head Name")]
        public string chargeHeadName { get; set; }

        [Display(Name = "Charge Head")]
        public ChargeHead chargeHead { get; set; }

        [Display(Name = "Cost Amount")]
        public decimal costAmount { get; set; }

    }
}
