USE [SIFITECHDB]
GO
INSERT [dbo].[Currency] ([currencyId], [createdAt], [currencyCode], [currencyName]) VALUES (N'119456a7-4763-4930-8bc5-b1bc31dea733', CAST(0x0700000000004F420B AS DateTime2), N'EURO', N'EURO')
INSERT [dbo].[Currency] ([currencyId], [createdAt], [currencyCode], [currencyName]) VALUES (N'48a07a18-a097-4955-9fd4-e637778ed61f', CAST(0x0700000000004F420B AS DateTime2), N'AED', N'AED')
INSERT [dbo].[Currency] ([currencyId], [createdAt], [currencyCode], [currencyName]) VALUES (N'8899a9f5-a23e-4b6d-bcd1-8009c6f0f28f', CAST(0x0700000000004F420B AS DateTime2), N'USD', N'USD')
INSERT [dbo].[Currency] ([currencyId], [createdAt], [currencyCode], [currencyName]) VALUES (N'eb602844-a663-409d-8782-0f7fe170b53e', CAST(0x0700000000004F420B AS DateTime2), N'INR', N'INR')
